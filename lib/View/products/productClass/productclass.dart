// To parse this JSON data, do
//
//     final productClass = productClassFromJson(jsonString);

// ignore_for_file: file_names

import 'dart:convert';

ProductClass productClassFromJson(String str) => ProductClass.fromJson(json.decode(str));

String productClassToJson(ProductClass data) => json.encode(data.toJson());

class ProductClass {
    ProductClass({
        required this.status,
        required this.data,
        required this.message,
    });

    final String status;
    final Data data;
    final String message;

    factory ProductClass.fromJson(Map<String, dynamic> json) => ProductClass(
        status: json["status"],
        data: Data.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
        "message": message,
    };
}

class Data {
    Data({
        required this.currentPage,
        required this.data,
        // @required this.from,
        required this.lastPage,
        // @required this.nextPageUrl,
        // @required this.path,
        // @required this.perPage,
        // @required this.prevPageUrl,
        // @required this.to,
        // @required this.total,
    });

    final int currentPage;
    final List<Datum> data;
    // final int from;
    final int lastPage;
    // final String nextPageUrl;
    // final String path;
    // final int perPage;
    // final dynamic prevPageUrl;
    // final int to;
    // final int total;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        currentPage: json["current_page"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        // from: json["from"],
        lastPage: json["last_page"],
        // nextPageUrl: json["next_page_url"],
        // path: json["path"],
        // perPage: json["per_page"],
        // prevPageUrl: json["prev_page_url"],
        // to: json["to"],
        // total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        // "from": from,
        "last_page": lastPage,
        // "next_page_url": nextPageUrl,
        // "path": path,
        // "per_page": perPage,
        // "prev_page_url": prevPageUrl,
        // "to": to,
        // "total": total,
    };
}

class Datum {
    Datum({
        required this.id,
        required this.categoryId,
        required this.name,
        required this.image,
        required this.sortOrder,
        required this.globalStatus,
        required this.viewAs,
        required this.status,
        required this.stockStatus,
    });

    final int id;
    final int categoryId;
    final String name;
    final String image;
    final int sortOrder;
    final int globalStatus;
    final int viewAs;
     int status;
     int stockStatus;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        categoryId: json["category_id"],
        name: json["name"],
        image: json["image"],
        sortOrder: json["sort_order"],
        globalStatus: json["global_status"],
        viewAs: json["view_as"],
        status: json["status"],
        stockStatus: json["stock_status"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "category_id": categoryId,
        "name": name,
        "image": image,
        "sort_order": sortOrder,
        "global_status": globalStatus,
        "view_as": viewAs,
        "status": status,
        "stock_status": stockStatus,
    };
}
