

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Models/MProvider/categoryprovider.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_dropdwnfield.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textbutton.dart';

class MFilter extends StatefulWidget {
  const MFilter({Key? key}) : super(key: key);

  @override
  _MFilterState createState() => _MFilterState();
}

class _MFilterState extends State<MFilter> {
  final sort = ["Newest First", "Oldest First"];
  final sstatus = ["All", "Active", "Inactive"];
  final actions = ["All", "Enabled", "Disabled"];
  final page = ["All","16", "32", ];
  String? stcontroller;
  String sortq = "";
  String? srtcontroller;
  String statq = "";
  String actq = "";
  String? actcontroller;
  String stockq ="";
  String? stkcontroller;
  final RefreshController refreshController =
      RefreshController(initialRefresh:false);
  // bool _isChecked = false;
  String? dropdownValue;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Consumer<ProviderModel>(builder: (context, provider, child) {
      return GestureDetector(
        onTap: () async {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  customText(text: "Filter By"),

                ],
              ),
              content: Padding(
                padding: const EdgeInsets.all(0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(children: [
                          CustomContainer(
                            borderRadius: BorderRadius.circular(10),
                            height: 48,
                            color: AppColors.white10,
                            width: MediaQuery.of(context).size.width * 0.33,
                            margin: const EdgeInsets.all(5),
                            child: CustomDDF(
                              controller: srtcontroller,
                              labeltext: "Sort",
                              list: sort.map(droplist2).toList(),
                              onchanged: (value) => setState(() async {
                                srtcontroller = value;
                                sortq = srtcontroller.toString();
                                if (srtcontroller.toString() ==
                                    "Newest First") {
                                  sortq = "desc";
                                }
                                if (srtcontroller.toString() ==
                                    "Oldest First") {
                                  sortq = "asc";
                                }
                                
                              }),
                            ),
                          ),
                         
                          const SizedBox(
                            height: 10,
                          ),
                        
                          CustomContainer(
                            height: 48,
                            borderRadius: BorderRadius.circular(10),
                            color: AppColors.white10,
                            width: MediaQuery.of(context).size.width * 0.31,
                            margin: const EdgeInsets.all(5),
                            child: CustomDDF(
                              controller: stcontroller,
                              labeltext: "Status",
                              list: sstatus.map(droplist2).toList(),
                              onchanged: (value) => setState(() async {
                                stcontroller = value;
                                statq = stcontroller.toString();
                                if (stcontroller.toString() == "All") {
                                  statq = "all";
                                }
                                if (stcontroller.toString() == "Active") {
                                  statq = "0";
                                }
                                if (stcontroller.toString() == "Inactive") {
                                  statq = "1";
                                }
                                
                              }),
                            ),
                          ),
                        ]),

                        const SizedBox(
                          height: 10,
                        ),
                        Row(children: [
                          CustomContainer(
                            height: 48,
                            borderRadius: BorderRadius.circular(10),
                            color: AppColors.white10,
                            width: MediaQuery.of(context).size.width * 0.33,
                            margin: const EdgeInsets.all(5),
                            child: CustomDDF(
                              controller: actcontroller,
                              labeltext: "Actions",
                              list: actions.map(droplist2).toList(),
                              onchanged: (value) => setState(() async {
                                actcontroller = value;
                                actq = actcontroller.toString();
                                if (actcontroller.toString() == "All") {
                                  actq = "all";
                                }
                                if (actcontroller.toString() == "Enabled") {
                                  actq = "0";
                                }
                                if (actcontroller.toString() == "Disabled") {
                                  actq = "1";
                                }
                              
                              }),
                            ),
                          ),
                        
                          const SizedBox(
                            height: 10,
                          ),
                       
                          // CustomContainer(
                          //   height: 48,
                          //   color: AppColors.white10,
                          //   borderRadius: BorderRadius.circular(10),
                          //   width: MediaQuery.of(context).size.width * 0.3,
                          //   margin: const EdgeInsets.all(5),
                          //   child: CustomDDF(
                          //     controller: stkcontroller,
                          //     labeltext: "Stock",
                          //     list: stock.map(droplist2).toList(),
                          //     onchanged: (value) => setState(() async {
                          //       stkcontroller = value;
                          //       stockq = stkcontroller;
                          //       if (stkcontroller.toString() == "All") {
                          //         stockq = "all";
                          //       }
                          //       if (stkcontroller == 16) {
                          //         stockq = 16;
                          //       }
                          //       if (stkcontroller == 32) {
                          //         stockq = 32;
                          //       }
                          //       if (stkcontroller == 48) {
                          //         stockq = 48;
                          //       }
                          //     }),
                          //   ),
                          // ),
                     
                        const SizedBox(height: 10,),
                         CustomContainer(
                            height: 48,
                            color: AppColors.white10,
                            borderRadius: BorderRadius.circular(10),
                            width: MediaQuery.of(context).size.width * 0.31,
                            margin: const EdgeInsets.all(5),
                            child: CustomDDF(
                              controller: stkcontroller,
                              labeltext: "Per page",
                              list: page.map(droplist2).toList(),
                              onchanged: (value) => setState((){
                                stkcontroller = value;
                                stockq = stkcontroller.toString();
                                if (stkcontroller.toString() == "All") {
                                  stockq = "all";
                                }
                                if (stkcontroller.toString() == "16") {
                                  stockq = "16";
                                }
                                if (stkcontroller.toString() == "32") {
                                  stockq = "32";
                                }
                              }),
                            ),
                          ),
                             ]),
                      ]),
                ),
              ),
              actions: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                  CustomtextButton(
                      text: "Cancel",
                      onpressed: () async {
                        statq = '';
                        sortq = '';
                        actq = '';
                        stockq = '';
                        final search = await provider.getModels(
                          isRefresh: true,
                          sort: sortq,
                          action: actq,
                          status: statq);
                        if (search) {
                          refreshController.refreshCompleted();
                        } else {
                          refreshController.refreshFailed();
                        }
                        stcontroller?.replaceAll(
                            statq, stcontroller = "All");

                        srtcontroller?.replaceAll(sortq,
                            srtcontroller = "Newest First");
                        actcontroller?.replaceAll(
                            actq, actcontroller = "All");
                        stkcontroller?.replaceAll(
                            stockq, stkcontroller = "All");
                         Navigator.pop(context,true);
                      }),
                  MaterialButton(
                    color: AppColors.white10,
                    onPressed: () async {
                       Navigator.pop(context,true);
                      final search = await provider.getModels(
                          isRefresh: true,
                          perpage: stockq,
                          sort: sortq,
                          action: actq,
                          status: statq,
                          );
                      // if (search){
                      //   refreshController.refreshCompleted();
                      // } else {
                      //   refreshController.refreshFailed();
                      // }
                    },
                    child: const Text("Apply"),
                  ),
                ]),
              ],
            ),
          );
        },
        behavior: HitTestBehavior.translucent,
        child: CustomContainer(
           border: Border.all(
                                        color: AppColors.dark, width: 0.5),
          borderRadius: BorderRadius.circular(10),
          shadow:const [
                  BoxShadow(
                      blurRadius: 5.0, spreadRadius: 0.2, color: AppColors.hint)
                ],
          align: Alignment.center,
          padding: const EdgeInsets.all(10),
          color: AppColors.white,
          height: size.height * 0.06,
          width: size.width * 0.12,
          child:  const Icon(Icons.filter_alt_outlined),
        ),
      );
    });
  }
DropdownMenuItem<String> droplist2(String sstatus) =>
DropdownMenuItem(value: sstatus, child: Text(sstatus));
}
