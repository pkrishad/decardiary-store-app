// To parse this JSON data, do
//
//     final version = versionFromJson(jsonString);

import 'dart:convert';

Version versionFromJson(String str) => Version.fromJson(json.decode(str));

String versionToJson(Version data) => json.encode(data.toJson());

class Version {
    Version({
        required this.status,
        required this.version,
        required this.supportingVersion,
        required this.validity,
        required this.privacy,
        required this.terms,
        required this.supportNumber,
    });

    final String status;
    final String version;
    final String supportingVersion;
    final bool validity;
    final String privacy;
    final String terms;
    final String supportNumber;

    factory Version.fromJson(Map<String, dynamic> json) => Version(
        status: json["status"],
        version: json["version"],
        supportingVersion: json["supportingVersion"],
        validity: json["validity"],
        privacy: json["privacy"],
        terms: json["terms"],
        supportNumber: json["supportNumber"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "version": version,
        "supportingVersion": supportingVersion,
        "validity": validity,
        "privacy": privacy,
        "terms": terms,
        "supportNumber": supportNumber,
    };
}
