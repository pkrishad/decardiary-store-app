import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Profile/profileclass.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class ProviderProfile with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;
  ProviderProfile({
    required this.repo,
    required this.dioClient,
  });
  late Profile user;
  Data? data;

  Future<void> userProfile(BuildContext context) async {
    ApiResponse res = await repo.getdata(StringConst.PROFILE);
    if (kDebugMode) {
      print(res.response!.statusCode);
    }
    if (res.response != null && res.response!.statusCode == 200) {
      user = Profile.fromJson(res.response?.data);
      data = user.data;

      notifyListeners();
    } else if (res.error != "401") {
      if (kDebugMode) {
        print(res.error);
      }
    }
  }
}
