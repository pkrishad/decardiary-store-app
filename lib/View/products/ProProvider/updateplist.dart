


// ignore_for_file: unused_import, avoid_print, unused_local_variable

import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class UpdateProductlist with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;

  UpdateProductlist({required this.repo, required this.dioClient});
// "product_categories"
  Future<ApiResponse?> updateprolist(
      {required BuildContext context,
      required int productid,
      required int status,
      required int stock, 
      }) async {
    // Dialogs.showLoading(context: context);
    var mapData = {
      "products": [
        json.encode({"product_id":productid,"status":status,"stock":stock})
      ].toString()
    };
   
    print(mapData);
    ApiResponse? res = await repo.postData(
        StringConst.UPDATE_PRODUCTLIST,
        data: mapData,);
    final resdata = (res.response?.data);
    if (kDebugMode) {
      print(mapData);
    }
    if (res.response != null && res.response?.statusCode == 200) {
    } else if (res.error != "401") {
      if (kDebugMode) {
        print(res.error);
      }
      // Alerts.showError("Enter a valid Username and Password");
    }
  }
}