import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/UpdateProfile/p_update.dart';
import 'package:storeadmin_app/custom_widgets/custom_button.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textfield.dart';
import 'package:storeadmin_app/dio/api_response.dart';

class Username extends StatefulWidget {
  const Username({Key? key}) : super(key: key);

  @override
  _UsernameState createState() => _UsernameState();
}

class _UsernameState extends State<Username> {
  final updateKey = GlobalKey<FormState>();
  final TextEditingController _newusername = TextEditingController();
  final TextEditingController _currentpassword = TextEditingController();
  @override
  Widget build(BuildContext context) {
    UpdateProfile user = Provider.of<UpdateProfile>(context, listen: false);

    return Form(
        key: updateKey,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              const SizedBox(
        height: 20,
              ),
              const SizedBox(
        width: 10,
              ),
              Row(
        children: [
          const Icon(Icons.person_outline_outlined),
          const SizedBox(
            width: 20,
          ),
          customText(
              text: "Create new username",
              weight: FontWeight.bold,
              textSize: 25.0),
        ],
              ),
              const SizedBox(
        height: 20,
              ),
              customText(
          text:
              "Your new username must be different \n from previous used username",
          color: AppColors.hint),
              const SizedBox(
        height: 20,
              ),
              CustomTextFormField(
          hintColor: AppColors.hint,
          controller: _newusername,
          validator: (value) {
            if (value!.isEmpty) {
              return "Username can't be empty";
            }
          },
          hintText: "Enter new username"),
              const Divider(
        color: AppColors.white,
              ),
              CustomTextFormField(
        hintColor: AppColors.hint,
        controller: _currentpassword,
        hintText: "Enter current password",
        validator: (value) {
          if (value!.isEmpty) {
            return "Password can't be empty";
          } else if (value.length < 5) {
            return "Invalid Password";
          }
          return null;
        },
              ),
              const SizedBox(
        height: 20,
              ),
              CustomElevatedButton(
          text: customText(text: "Update Username"),
          onpress: () {
            if (updateKey.currentState!.validate()) {
              final Future<ApiResponse?> successfulMessage =
                  user.updateuser(
                      context: context,
                      username: _newusername,
                      password: _currentpassword);
              successfulMessage.then((response) {
                _newusername.clear();
                _currentpassword.clear();
              });
            } else {
              if (kDebugMode) {
                print("form is invalid");
              }
            }
          },
          bgColor: AppColors.dark,
          fgColor: AppColors.white)
            ]));
  }
}
