import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

import 'custom_text.dart';

class ProContainer extends StatelessWidget {
  final void Function()? onTap;
  final String text;
  final IconData icon;
  const ProContainer(
      {Key? key, this.onTap, required this.text, required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed:
        onTap,


      style: ButtonStyle(
          overlayColor: MaterialStateProperty.all(AppColors.white10),
          foregroundColor: MaterialStateProperty.all<Color>(AppColors.black),
          backgroundColor: MaterialStateProperty.all<Color>(AppColors.white),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)))),
      child: Padding(
        padding: const EdgeInsets.all(14),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Row(children: [
            Icon(
              icon,
              color: AppColors.blue,
              size: 32,
            ),
            const SizedBox(width: 10),
            customText(text: text, textSize: 22.0)
          ]),
          const Icon(
            Icons.arrow_forward_ios,
            color: AppColors.blue,
          )
        ]),
      ),
    );
  }
}
