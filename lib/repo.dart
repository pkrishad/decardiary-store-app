
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:storeadmin_app/View/products/productClass/categryclass.dart';
import 'dio/api_response.dart';
import 'dio/dio_client.dart';
import 'dio/error_handler.dart';

class Repo {
  final DioClient dioClient;
  final SharedPreferences sharedPreferences;

  Repo({required this.dioClient, required this.sharedPreferences});

  Future<ApiResponse> getdata(String url) async {
    try {
      final response = await dioClient.get(
        url,
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      if (kDebugMode) {
        print("Repo Error");
      }
      return ApiResponse.withError(ApierrorHandler.getMessage(e));
    }
  }
  
  Future<ApiResponse> postData(String url, {data, isMultipart}) async {
    try {
      if (isMultipart == null) {
        dioClient.dio.options.contentType = "application/json";
      } else {
    
        dioClient.dio.options.contentType = "multipart/form-data";
      }
      final response = await dioClient.post(
        url,
        data: data,
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      if (kDebugMode) {
        print("Repo Error");
      }
      return ApiResponse.withError(ApierrorHandler.getMessage(e));
    }
  }
}