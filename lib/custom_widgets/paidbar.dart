import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

import 'custom_container.dart';
import 'custom_text.dart';

class PaidBar extends StatelessWidget {
  const PaidBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return CustomContainer(
      width: size.width * 0.20,
      height: size.height * 0.045,
      color: Colors.white,
      shadow: [
        BoxShadow(
            offset: const Offset(0, 1),
            blurRadius: 2,
            color: AppColors.black.withOpacity(0.2))
      ],
      borderRadius: BorderRadius.circular(4.0),
      child: Center(
        child: customText(text: "Paid"),),
    );
  }
}
