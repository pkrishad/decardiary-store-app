import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_dropdwnfield.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textbutton.dart';

import 'p_users.dart';


class Ufilter extends StatefulWidget {
  Ufilter({Key? key}) : super(key: key);

  @override
  State<Ufilter> createState() => _UfilterState();
}

class _UfilterState extends State<Ufilter> {
  String cs = "";

  final RefreshController refreshController =
  RefreshController(initialRefresh: true);

  final controller = TextEditingController();

  String? ordercontroller;

  final order = ["All", "Newest First", "Oldest First"];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    ProviderUser userdata = Provider.of<ProviderUser>(context, listen: false);
    return GestureDetector(
        onTap: () async {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              customText(text: "Filter By"),

            ],
          ),
          content: Padding(
          padding: const EdgeInsets.all(0),
    child: SingleChildScrollView(
    scrollDirection: Axis.vertical,
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
          Container(
                              decoration: BoxDecoration(
                                  color: AppColors.white,
                                  borderRadius:
                                      BorderRadius.circular(10)),
                              alignment: Alignment.center,
                              width: 160,
                              height: 55,
                              child: CustomDDF(
                                controller: ordercontroller,
                                labeltext: "Sort",
                                list: order.map(droplist).toList(),
                                onchanged: (value) => setState(() {
                                  ordercontroller = value;

                                  cs = ordercontroller.toString();
                                  if (ordercontroller.toString() ==
                                      "All") {
                                    cs = "all";
                                  }
                                  if (ordercontroller.toString() ==
                                      "Newest First") {
                                    cs = "dsc";
                                  }
                                  if (ordercontroller.toString() ==
                                      "Oldest First") {
                                    cs = "asc";
                                  }

                                  }),


                                  ),
          ),]))),
              actions: <Widget>[
          Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CustomtextButton(
                    text: "Cancel",
                    onpressed: () async {
                    cs ='';
                      final search = await userdata.getUser(
                        isRefresh: true,
                      );
                      if (search) {
                        refreshController.refreshCompleted();
                      } else {
                        refreshController.refreshFailed();
                      }

                      Navigator.pop(context,true);
                    }),
                MaterialButton(
                  color: AppColors.white10,
                  onPressed: () async {
                    Navigator.pop(context,true);

                    final search = await userdata.getUser(sort: cs,isRefresh:true );
                    if (search) {
                      refreshController.refreshCompleted();
                    } else {
                      refreshController.refreshFailed();
                    }
                  },
                  child: const Text("Apply"),
                ),
              ]),
        ],
      ),
          );
        },
      behavior: HitTestBehavior.opaque,
      child: CustomContainer(
        shadow:  [ BoxShadow(
            blurRadius: 5.0, spreadRadius: 0.2, color: AppColors.hint)],
        border: Border.all(color: AppColors.dark, width: 0.5),
        borderRadius: BorderRadius.circular(10),
        align: Alignment.center,
        padding: const EdgeInsets.all(10),
        color: AppColors.white,
        height: size.height * 0.06,
        width: size.width * 0.12,
        child: const Icon(Icons.filter_alt_outlined),
        //     customText(text: "Filter", weight: FontWeight.w400)
        //   ],
        // ),
      ),
    );
     }
  DropdownMenuItem<String> droplist(String order) => DropdownMenuItem(
    value: order,
    child: Text(order),
  );
}

