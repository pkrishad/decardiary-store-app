// ignore_for_file: non_constant_identifier_names, prefer_typing_uninitialized_variables, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:storeadmin_app/Values/values.dart';

class Dashcontainer extends StatelessWidget {
  final text1;
  final Widget? text2;
  final Icon? icon;
  final Color? color;
  final BoxBorder? border;
  final txtcolor;
  final size;
  final totalsteps;
  final currentstep;
  const Dashcontainer(
      {Key? key,
      this.text1,
      this.border,
      this.text2,
      this.icon,
      this.color,
      this.txtcolor,
      this.size,
      this.totalsteps,
      this.currentstep})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 40),
        decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(12.0),
            border: border),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  CircleAvatar(
                      backgroundColor: Colors.grey.shade50, child: icon),
                  text2!,
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.02),
              Center(
                child: text1,
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.01),
    
              // SizedBox(
              //   height:MediaQuery.of(context).size.height * 0.14,
              //   child: Center(
              //     child: Column(
              //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //       children: [
              //        CircleAvatar(
              //           backgroundColor: AppColors.white10.withOpacity(0.6),
              //           child: icon),
              //         text1
              //       ],
              //     ),
              //   ),
              // ),
              // Container(
              //   height:MediaQuery.of(context).size.height * 0.06,
              //   decoration: BoxDecoration(
              //    color: color,
    
              //       borderRadius: const BorderRadius.only(
              //           bottomLeft: Radius.circular(8.0),
              //           bottomRight: Radius.circular(8.0),
              //           topRight: Radius.circular(20.0),
              //           topLeft: Radius.circular(20.0)),
              //   ),
              //   child: Center(
              //     child: text2,
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
