import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Users/p_users.dart';
import 'package:storeadmin_app/View/Users/ufilter.dart';
import 'package:storeadmin_app/View/Users/usersclass.dart';
import 'package:storeadmin_app/custom_widgets/activebar.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_appbar.dart';
import 'package:storeadmin_app/custom_widgets/custom_card.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textfield.dart';

class Users extends StatefulWidget {
  const Users({Key? key}) : super(key: key);

  @override
  _UsersState createState() => _UsersState();
}

class _UsersState extends State<Users> {
  Datum? datum;
  bool isLoading = false;
  String cs = "";
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);
  final controller = TextEditingController();
  String? ordercontroller;
  final order = ["All", "Newest First", "Oldest First"];

  // @override
  // void initState() {
  //   WidgetsBinding.instance!.addPostFrameCallback((_) {
  //     Provider.of<ProviderUser>(context, listen: false).callpageApi();
  //   });
  //   super.initState();
  //   _controller.addListener(_scrollListener);
  // }

  // void _scrollListener() {
  //   if (_controller.position.pixels == _controller.position.maxScrollExtent) {
  //     Provider.of<ProviderUser>(context, listen: false).callpageApi();
  //   }
  // }

  @override
  Widget build(BuildContext context) {
       checkConnection(context);
    Size size = MediaQuery.of(context).size;
    ProviderUser userdata = Provider.of<ProviderUser>(context, listen: false);
    return Consumer<ProviderUser>(builder: (context, provider, child) {
      return 
      Scaffold(backgroundColor: AppColors.white,
          appBar: customAppBar(
            fntcolor: AppColors.black,
            textSize: 20.0,
            bottom: Padding(
                padding: const EdgeInsets.all(6),
                child: Container(
                  width: size.width*3,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8)),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 78, vertical: 10),
                  child: Container(
                      decoration: BoxDecoration(
                          color: AppColors.white,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: [
                            BoxShadow(
                                offset: const Offset(0, 1),
                                blurRadius: 5,
                                color: AppColors.black.withOpacity(0.23))
                          ]),
                      child: Center(
                          child: customText(
                              text: "All Users", textSize: 20.0))),
                )),
          ),
          body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
              child: Column(children: [
// SizedBox(height: size.height*0.01,),
                Row(mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomContainer(
                      align: Alignment.center,
                      borderRadius: BorderRadius.circular(10),
                      width: size.width * 0.80,
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      color: AppColors.white,
                      height: size.height * 0.06,
                      child: CustomTextFormField(
                        controller: controller,
                        suffixIcon: InkWell(
                            onTap: () async {
                              // await provider.getUser(
                              //   search: controller.text.toUpperCase(),
                              //   isRefresh: true,
                              // );
                            },
                            child: const Icon(Icons.search_outlined)),
                        hintText: "Search",
                        hintColor: AppColors.hint,
                      ),
                    ),
                    SizedBox(width: size.width * 0.01),
                    Ufilter(),
                    SizedBox(width: size.width * 0.02),

                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Expanded(
                    child: SmartRefresher(
                        controller: refreshController,
                        enablePullUp: true,
                        enablePullDown: true,
                        footer: CustomFooter(
                          builder: (BuildContext context, LoadStatus? mode) {
                            Widget child;
                            if (mode == LoadStatus.idle) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "Swipe up to load more",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.loading) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    CupertinoActivityIndicator(),
                                    SizedBox(width: 5,),
                                    customText(
                                        text: "Loading..",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.failed) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.error_outlined,
                                      color: AppColors.hint2,
                                      
                                    ),
                                    SizedBox(width: 5,),
                                    customText(
                                        text: "No more Data",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.canLoading) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "Swipe up to load more",
                                        color: AppColors.hint2)
                                  ]);
   
                            } else {
                              child =Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "No more Data",
                                        color: AppColors.hint2)
                                  ]); 
                            }
                            return child;
                          },
                        ),
                        onRefresh: () async {
                          final result = await userdata.getUser(
                            isRefresh: true,
                          );
                          if (result) {
                            refreshController.refreshCompleted();
                          } else {
                            refreshController.refreshFailed();
                          }
                        },
                        onLoading: () async {
                          final result = await userdata.getUser();
                          if (result) {
                            refreshController.loadComplete();
                          } else {
                            refreshController.loadFailed();
                          }
                        },
                        child: isLoading
                            ? const Center(child: CircularProgressIndicator())
                            : provider.userdata.isNotEmpty
                                ? ListView.separated(
                                    itemCount: provider.userdata.length,
                                    itemBuilder: (BuildContext context, index) {
                                      final users = provider.userdata[index];
                                      return CustomCard(
                                          child: SingleChildScrollView(
                                        physics: const BouncingScrollPhysics(),
                                        child: Column(
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 20,
                                                      vertical: 5),
                                              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                                  children: [
                                                    CircleAvatar(
                                                      radius: 25,
                                                      backgroundColor:
                                                          AppColors.white,
                                                      child: SvgPicture.asset('assets/svg/user.svg')
                                                    ),
                                              Column(children: [
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                customText(
                                                    text: "Expires in"),
                                                const SizedBox(
                                                  height: 2,
                                                ),
                                                ActiveBar(text: customText(text: "${users.expiresin} days"),bgcolor: AppColors.blue,),
                                                    ]),
                                                    Column(children: [
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      customText(
                                                          text: "Status"),
                                                      const SizedBox(
                                                        height: 2,
                                                      ),
                                                      ActiveBar(
                                                        bgcolor:
                                                            users.membershipstatus ==
                                                                    0
                                                                ? Colors.green
                                                                    .shade100
                                                                : Colors.red
                                                                    .shade700,
                                                        text: customText(
                                                            text: users.membershipstatus ==
                                                                    0
                                                                ? "Active"
                                                                : "In Active"),

                                                      ),
                                                    ]),
                                                  ]),
                                            ),
                                            ExpansionTile(
                                              title: customText(
                                                  text: users.username,
                                                  color: AppColors.hint,
                                                  weight: FontWeight.w400),
                                              children: [
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .center,
                                                  children: [
                                                    Center(
                                                      child: customText(
                                                          text:
                                                              "Membership Details",color: AppColors.hint),
                                                    ),
                                                    
                                                    const SizedBox(
                                                      height: 20,
                                                    ),
                                                    users.expiresin == null
                                                        ? Text("",
                                                            style: TextStyles
                                                                .white_20_700())
                                                        : customText(align: TextAlign.center,
                                                            text:
                                                                "Valid till : ${users.expiresin} days"),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    users.createdAt == null
                                                        ? Text("",
                                                            style: TextStyles
                                                                .white_20_700())
                                                        : Center(
                                                          child: customText(
                                                              text:
                                                                  "Purchased in : ${users.createdAt.toString().substring(0, 10)}"),
                                                        ),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    users.expiryDate == null
                                                        ? Text("",
                                                            style: TextStyles
                                                                .white_20_700())
                                                        : customText(
                                                            text:
                                                                "Expires in: ${users.expiryDate.toString().substring(0, 10)}"),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                  ],
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ));
                                    },
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return SizedBox(
                                        height: size.height * 0.009,
                                      );
                                    },
                                  )
                                : Center(
                                    child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Icon(
                                        Icons.search_off_rounded,
                                        color: AppColors.hint,
                                        size: 50.0,
                                      ),
                                      customText(text: "No Active Users.")
                                    ],
                                  ))))
              ])));
    });
  }

  status() {
    if (datum?.membershipstatus == null) {
      return customText(text: "Active");
    } else {
      return customText(text: "In Active");
    }
  }

  DropdownMenuItem<String> droplist(String order) => DropdownMenuItem(
        value: order,
        child: Text(order),
      );
}
