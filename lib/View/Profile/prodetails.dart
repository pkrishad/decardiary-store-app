// ignore_for_file: unused_field

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Profile/providerprofile.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_listtile.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';

class ProDetails extends StatefulWidget {
  const ProDetails({Key? key}) : super(key: key);

  @override
  _ProDetailsState createState() => _ProDetailsState();
}

class _ProDetailsState extends State<ProDetails> {
  final TextEditingController _shopname = TextEditingController();
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      Provider.of<ProviderProfile>(context, listen: false).userProfile(context);
    });
    checkConnection(context);
    Size size = MediaQuery.of(context).size;
    return Consumer<ProviderProfile>(builder: (context, provider, child) {
      var user = provider.data;
      return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(
                Icons.arrow_back_ios,
                color: AppColors.black,
              )),
        ),
        body: Padding(
          padding: const EdgeInsets.all(30),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                user?.storeimage == null? Text(""):
CircleAvatar(backgroundColor: AppColors.white10,
  backgroundImage: NetworkImage(user!.storeimage),
  radius: 80,
),
                                CustomContainer(borderRadius: BorderRadius.circular(10),
                                  shadow: const [BoxShadow(
                                    color:AppColors.blue,
                                  blurRadius:0.1 ,spreadRadius:0.01 ,

                                offset: Offset(0,2) 
                                  )],
                                  color: AppColors.white,
          child:Padding(padding: const EdgeInsets.all(20),child:
   Column(children: [
user?.storename == null 
 ? Text("#STORE",
                                          style: TextStyles.white_20_700())
                                      : user!.storename.isEmpty
                                          ? Text("no data",
                                              style: TextStyles.white_20_700())
                                          : customText(
                                              text: user.storename,
                                              textSize: 25.0,
                                              weight: FontWeight.bold),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        const Icon(
                                          Icons.location_on_outlined,
                                          color: AppColors.dark,
                                        ),
                                        user?.city == null
                                            ? Text("#STORE",
                                                style:
                                                    TextStyles.white_20_700())
                                            : user!.city.isEmpty
                                                ? Text("no data",
                                                    style: TextStyles
                                                        .white_20_700())
                                                : customText(
                                                    text: user.city),
                                      ])
                                  ],)),
                                
                                ),
                const SizedBox(
                  height: 30,
                ),
                
                const Divider(color: AppColors.white,),
                title(Icons.storefront_rounded, "Storename"),
                CustListTile(
                  text: user!.storename,
                ),
                const Divider(color: AppColors.white,),
                title(Icons.location_city, "City"),
                CustListTile(
                  text: user.city,
                ),
                 const Divider(color: AppColors.white,),
                title(Icons.location_city_outlined, "Address1"),
                CustListTile(
                  text: user.addressLine_1,
                ),
                 const Divider(color: AppColors.white,),
                title(Icons.location_city_outlined, "Address2"),
                CustListTile(text: user.addressLine_2),
                const Divider(color: AppColors.white,),
                title(Icons.map_outlined, "District"),
                CustListTile(text: user.district),
                 const Divider(color: AppColors.white,),
                title(Icons.maps_ugc, "Pincode"),
                CustListTile(text: user.pincode),
                const Divider(color: AppColors.white,),
                title(Icons.phone, "Contactnumber"),
                CustListTile(text: user.contactnumber),
                const Divider(color: AppColors.white,),
                title(Icons.phone, "Alternatenumber"),
                user.alternatenumber.isEmpty
                    ? const CustListTile(
                        text: "null",
                      )
                    : CustListTile(
                        text: user.alternatenumber,
                      ),
                const Divider(color: AppColors.white,),
                title(Icons.picture_in_picture_rounded, "IdProoftype"),
                CustListTile(
                  text: user.idcardtype,
                ),
                    const Divider(color: AppColors.white,),
                title(Icons.store, "Store Id"),
                CustListTile(
                  text: "${user.id}",
                ),
                 const Divider(color: AppColors.white,),
                title(Icons.person, "Username"),
                CustListTile(
                  text: user.username,
                ),
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget title(IconData icon, text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(icon),
        const SizedBox(width: 10),
        customText(text: text),
      ],
    );
  }
}
