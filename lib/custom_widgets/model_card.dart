// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/custom_widgets/activebar.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_toggleswitchbutton.dart';

class ModelCard extends StatelessWidget {
  final void Function()? ontap;
  final void Function(bool)? onhover;
  final txtactive;
  final activeontap;
  final toggleValue;
  final void Function()? toggleButton;
  final onend;
  final image;
  final modelname;
  final stock;
  final bgcolor;
  final value;
  final onchanged;
  final textadmin;
  final double? left;
  final double? right;
  final Color? togglecolor;
  final Widget? icon;
  const ModelCard(
      {Key? key,
      this.ontap,
      this.txtactive,
      this.activeontap,
      this.image,
      this.modelname,
      this.stock,
      this.bgcolor,
      this.toggleValue,
      this.toggleButton,
      this.left,
      this.right,
      this.togglecolor,
      this.icon,
      this.onend,
      this.value,
      this.onchanged,
      this.onhover, this.textadmin})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Card(
        color: AppColors.white,
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: Row(children: [
              InkWell(
                  child: Stack(children: [
                    Container(
                      width: size.width * 0.28,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15)),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(15.0),
                          child: image),
                      height: size.height * 0.15,
                    ),
                    // Container(
                    //     height: 20,
                    //     padding: const EdgeInsets.symmetric(
                    //         vertical: 2, horizontal: 10),
                    //     alignment: Alignment.centerRight,
                    //     decoration: BoxDecoration(
                    //       borderRadius: const BorderRadius.only(
                    //           topLeft: Radius.circular(10),
                    //           bottomRight: Radius.circular(10)),
                    //       color: AppColors.white10,
                    //     ),
                    //     child: stock),
                    Positioned(
                      bottom: 5,
                      left: 15,
                      child: CustomContainer(
                        align: Alignment.center,
                        padding:
                            const EdgeInsets.symmetric(horizontal: 2, vertical: 5),
                        borderRadius: BorderRadius.circular(4),
                        gradient: const LinearGradient(colors: [AppColors.dark,Colors.transparent,],begin: Alignment.topLeft,end: Alignment.bottomRight),
                      
                        child: customText(
                          color: AppColors.white,
                          weight: FontWeight.bold,
                          text: modelname,
                        ),
                      ),
                    )
                  ]),
                  onTap: ontap),
              SizedBox(width: size.width * 0.07),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Row(children: [
                  Column(
                    children: [
                      customText(text: "Status", weight: FontWeight.bold),
                      SizedBox(
                        height: size.height * 0.004,
                      ),
                      ActiveBar(
                        bgcolor: bgcolor,
                        text: txtactive,
                        onTap: activeontap,
                      ),
                    ],
                  ),
                  SizedBox(
                    width: size.width * 0.10,
                  ),
                  Column(children: [
                    customText(text: "Enable", weight: FontWeight.bold),
                    SizedBox(
                      height: size.height * 0.004,
                    ),
                    Togglebtn(
                      onhover: onhover,
                      left: left,
                      right: right,
                      togglecolor: togglecolor,
                      icon: icon,
                      toggleButton: toggleButton,
                    ),
                    // togglebtn
                  ]),
                ]),
                const SizedBox(
                  height: 20,
                ),
                customText(text: textadmin, color: AppColors.red),
                // Switch(value: value, onChanged: onchanged),
                // CustomContainer(
                //   borderRadius:
                //       BorderRadius.circular(20),
                //   width: size.width * 0.5,
                //   height: size.height * 0.05,
                //   color: Colors.white,
                //   child: Center(
                //     child: customText(text: modelname),
                //   ),
                // shadow: [
                //   BoxShadow(
                //       offset: const Offset(0, 1),
                //       blurRadius: 10,
                //       color: AppColors.black.withOpacity(0.1))
                // ],
              ]),
            ])));
  }
}
