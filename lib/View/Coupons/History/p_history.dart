import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Coupons/History/historyclass.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class ProviderHistory with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;

  ProviderHistory({
    required this.repo,
    required this.dioClient,
  });
  Mhistory? history;
  Data? data;
  late int totalPages;
  int current_page = 1;
  String _text = "";
  late List<Datum> hist = [];
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  historys(
      {BuildContext? context,
      bool isRefresh = false,
      String? psstatus,
      String? status}) async {
        if (isRefresh) {
      current_page = 1;
    } else {
      if (current_page >= totalPages) {
        refreshController.loadNoData();
        return false;
      }
    }
    ApiResponse res = await repo.getdata(StringConst.COUPON_HISTORY +
        "?page=$current_page&perpage=10&payment_status=$psstatus&status=$status");
    
    if (res.response != null && res.response!.statusCode == 200) {
      final history = Mhistory.fromJson(res.response!.data);
      final result = history.data;
      // final pagedata = result.data;
      if (isRefresh) {
        hist = result.data;
      } else {
        hist.addAll(result.data);
      }
      current_page++;
      totalPages = result.lastPage;
      notifyListeners();
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }
String get text => _text;
  Datum? historylist;
  String _activestatus = "";
  String? _value;
  String? _value2;
}
