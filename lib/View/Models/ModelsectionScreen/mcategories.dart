// ignore_for_file: unused_local_variable, unnecessary_const, unnecessary_string_interpolations

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Models/MProvider/categoryprovider.dart';
import 'package:storeadmin_app/View/Models/MProvider/updatecmod.dart';
import 'package:storeadmin_app/View/Models/ModelsectionScreen/mfilter.dart';
import 'package:storeadmin_app/custom_widgets/bulkupdate.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textbutton.dart';
import 'package:storeadmin_app/custom_widgets/custom_textfield.dart';
import 'package:storeadmin_app/custom_widgets/custom_toggleswitchbutton.dart';
import 'package:storeadmin_app/custom_widgets/pcontainer.dart';

class MCategory extends StatefulWidget {
  const MCategory({Key? key}) : super(key: key);

  @override
  _MCategoryState createState() => _MCategoryState();
}

class _MCategoryState extends State<MCategory> {
  final controller = TextEditingController();

  bool bulkupdate = false;
  bool isLoading = false;
  bool togglevalue = false;
  late int data;
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  @override
  Widget build(BuildContext context) {
    checkConnection(context);
    Size size = MediaQuery.of(context).size;
    UpdateCModel update = Provider.of<UpdateCModel>(context);
    ProviderModel moddata = Provider.of<ProviderModel>(context, listen: false);
    return Consumer<ProviderModel>(builder: (context, provider, child) {
      return Scaffold(
          backgroundColor: AppColors.white,
          body: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(children: [
                Row(
                  children: [
                    CustomContainer(
                      align: Alignment.center,
                      borderRadius: BorderRadius.circular(10),
                      width: size.width * 0.66,
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      color: AppColors.white,
                      height: size.height * 0.06,
                      child: CustomTextFormField(
                        controller: controller,
                        suffixIcon: InkWell(
                            onTap: () async {
                              final search = await provider.getModels(
                                search: controller.text.toUpperCase(),
                                isRefresh: true,
                              );
                            },
                            child: const Icon(Icons.search_outlined)),
                        hintText: "Search",
                        hintColor: AppColors.hint,
                      ),
                    ),
                    SizedBox(width: size.width * 0.01),
                    const MFilter(),
                    SizedBox(width: size.width * 0.02),
                    Bulk(
                      ontap: () {
                        showModalBottomSheet(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20))),
                            context: context,
                            builder: ((builder) => CustomContainer(
                                height: size.height * 0.2,
                                width: size.width,
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: size.height * 0.02,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          customText(
                                              text: "Bulk update",
                                              color: AppColors.dark,
                                              textSize: 20.0,
                                              weight: FontWeight.bold),
                                          CustomtextButton(
                                              onpressed: () {
                                                Navigator.pop(context);
                                              },
                                              text: "Cancel")
                                        ],
                                      ),
                                      SizedBox(
                                        height: size.height * 0.01,
                                      ),
                                      SizedBox(
                                        height: size.height * 0.03,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          customText(
                                              text: "Actions", textSize: 25.0),
                                          SizedBox(
                                            height: size.height * 0.02,
                                          ),
                                          Togglebtn(
                                            toggleButton: () {
                                              bulkupdate = !bulkupdate;

                                              if (!bulkupdate) {
                                                data = 0;
                                              } else if (bulkupdate) {
                                                data = 1;
                                              }
                                              provider.updateBulkUpload(data);
                                              Future.delayed(
                                                  Duration(seconds: 1), () {
                                                provider.getModels(
                                                    isRefresh: true);
                                                refreshController
                                                    .requestRefresh(
                                                  needMove: false,
                                                  needCallback: true,
                                                );
                                                // refreshController
                                                //       .refreshCompleted();
                                              });

                                              Navigator.pop(context);
                                            },
                                            left: provider
                                                    .modeldata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? 30.0
                                                : 0.0,
                                            right: provider
                                                    .modeldata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? 0.0
                                                : 30.0,
                                            togglecolor: provider
                                                    .modeldata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? Colors.green[200]
                                                : Colors.grey[300],
                                            icon: provider
                                                    .modeldata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  )
                                                : const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  ),
                                          ),
                                        ],
                                      ),
                                    ]))));
                      },
                      icon: Icon(
                        Icons.fact_check_outlined,
                        color: AppColors.red.withOpacity(0.8),
                      ),
                      color: AppColors.white10,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Expanded(
                    child: SmartRefresher(
                        controller: refreshController,
                        enablePullUp: true,
                        enablePullDown: true,
                        footer: CustomFooter(
                          builder: (BuildContext context, LoadStatus? mode) {
                            Widget child;
                            if (mode == LoadStatus.idle) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "Swipe up to load more",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.loading) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    CupertinoActivityIndicator(),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    customText(
                                        text: "Loading..",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.failed) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.error_outlined,
                                      color: AppColors.hint2,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    customText(
                                        text: "No more Data",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.canLoading) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "Swipe up to load more",
                                        color: AppColors.hint2)
                                  ]);
                            } else {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "No more Data",
                                        color: AppColors.hint2)
                                  ]);
                            }
                            return child;
                          },
                        ),
                        onRefresh: () async {
                          final result = await moddata.getModels(
                            isRefresh: true,
                          );
                          if (result) {
                            refreshController.refreshCompleted();
                          } else {
                            refreshController.refreshFailed();
                          }
                        },
                        onLoading: () async {
                          final result = await moddata.getModels();
                          if (result) {
                            refreshController.loadComplete();
                          } else {
                            refreshController.loadFailed();
                          }
                        },
                        child: isLoading
                            ? const Center(
                                child: const CircularProgressIndicator())
                            : provider.modeldata.isNotEmpty
                                ? ListView.separated(
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return SizedBox(
                                        height: size.height * 0.009,
                                      );
                                    },
                                    itemCount: provider.modeldata.length,
                                    itemBuilder: (BuildContext context, index) {
                                      final mod = provider.modeldata[index];
                                      return Card(
                                          elevation: 2,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Pcontainer(
                                              image: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  child: Image.network(
                                                    mod.categoriesImage,
                                                    fit: BoxFit.fill,
                                                    width: 255,
                                                  )),
                                              text: "${mod.title}",
                                              bgcolor: mod.status == 0
                                                  ? Colors.green.shade100
                                                  : Colors.red.shade700,
                                              activetext: customText(
                                                  text: mod.status == 0
                                                      ? "Active"
                                                      : "Inactive"),
                                                      onhover: (togglevalue){
                                                       
                                                          togglevalue = !togglevalue;
                                                       
                                                      },
                                              togglebutton: () {
                                                togglevalue = !togglevalue;

                                                if (!togglevalue) {
                                                  data = 0;
                                                } else if (togglevalue) {
                                                  data = 1;
                                                }
                                                
                                                provider.updateStatus(
                                                    
                                                    categoryid: mod.id,
                                                    status: data);
                                                
                                                 Future.delayed(
                                                    const Duration(
                                                        milliseconds: 200), () {
                                                  provider.getModels(
                                                      perpage: 'all',
                                                      isRefresh: true);
                                                });

                                                // refreshController.refreshCompleted();
                                              },
                                              left:
                                                  mod.status == 0 ? 30.0 : 0.0,
                                              right:
                                                  mod.status == 0 ? 0.0 : 30.0,
                                              togglecolor: mod.status == 0
                                                  ? Colors.green[200]
                                                  : Colors.grey.shade300,
                                              icon: mod.status == 0
                                                  ? const Icon(
                                                      Icons.circle,
                                                      color: Colors.white,
                                                      size: 35.0,
                                                    )
                                                  : const Icon(
                                                     Icons.circle,
                                                      color: Colors.white,
                                                      size: 35.0,
                                                    )));
                                    })
                                : Center(
                                    child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Icon(
                                        Icons.search_off_rounded,
                                        color: AppColors.hint,
                                        size: 50.0,
                                      ),
                                      customText(text: "No Model Categories.")
                                    ],
                                  ))))
              ])));
    });
  }
}
