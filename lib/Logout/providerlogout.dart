import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:storeadmin_app/Logout/logoutclass.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Login%20Screen/loginscreen.dart';
import 'package:storeadmin_app/custom_widgets/dialog.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class Logout with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;

  Logout({required this.repo, required this.dioClient});

  Future<void> userlogout(BuildContext context) async {
    Dialogs.showLoading(context: context);
    ApiResponse res = await repo.getdata(StringConst.LOGOUT);
    if (kDebugMode) {
      print(res.response!.statusCode);
    }
    var logout = Log.fromJson(res.response?.data);
    if (logout.status == "success" && res.response!.statusCode == 200) {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove("username");
      prefs.remove("UpdateKey");
      Navigator.pushAndRemoveUntil(
            context, MaterialPageRoute(builder: (context) => const LoginScreen()), (route) => false);
      Alerts.showSuccess("Logged Out SuccessFully");
    } else if (res.error != "401") {
      if (kDebugMode) {
        print(res.error);
      }
    }
    
  }
}
