import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';
import 'dart:convert';
class UpdateCModel with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;

  UpdateCModel({required this.repo, required this.dioClient});
  Future<ApiResponse?> updatecmodel(
      {required BuildContext context,
      required int categoryid,
      required int status}) async {
    // Dialogs.showLoading(context: context);
    var m2 = {
  'model_categories': [
        json.encode({'category_id': categoryid, 'status': status})
      ].toString()
    };
   
    
    ApiResponse? res = await repo.postData(StringConst.UPDATE_MODEL_CATEGORIES,
        data:m2);
    final resdata = (res.response?.data);
    if (res.response != null && res.response?.statusCode == 200) {
    } else if (res.error != "401") {
      if (kDebugMode) {
        print(res.error);
      }
      // Alerts.showError("Enter a valid Username and Password");
    }
  }
}
