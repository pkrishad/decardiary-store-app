import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/DashBoard/dashmodel.dart';
import 'package:storeadmin_app/custom_widgets/dialog.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class ProviderDash with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;
  ProviderDash({required this.dioClient, required this.repo});
  DashModel? dash;
  int _currentIndex = 0;
  int get currentIndex => _currentIndex;
  

  Future<void> getDashBoard(BuildContext context) async {
    // Dialogs.showLoading(context: context);
    ApiResponse res = await repo.getdata(StringConst.DASHBOARD);
    if (kDebugMode) {
      print(res.response?.statusCode);
    }
    if (res.response != null && res.response!.statusCode == 200) {
      dash = DashModel.fromJson(res.response?.data);
      notifyListeners();
      return res.response?.data;
    } else if (res.error != "401") {
      if (kDebugMode) {
        print(res.error);
      }
      // Alerts.showError("res.error");
    }
  }

  set currentIndex(int index) {
    _currentIndex = index;
    notifyListeners();
  }
  
}
