import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Models/ModelClass/modelclass.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class ProviderModel with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;
  late int totalPages;
  int current_page = 1;
  // String _text = "";
 
  late List<Datum> modeldata = [];
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  ProviderModel({required this.repo, required this.dioClient});
 
  getModels(
      {bool isRefresh = false,
      String sort = "",
      String status = "",
      String action = "",
      String search = '',
      String perpage = "16"}) async {
        if (isRefresh) {
      current_page = 1;
    } else {
      if (current_page >= totalPages) {
        refreshController.loadNoData();
        return false;
      }
    }
    // Dialogs.showLoading(context: context);
    ApiResponse? res = await repo.getdata(StringConst.MODELCATEGORYS +
        "?page=$current_page&perpage=$perpage&dataorder=$sort&status=$status&isdisabled=$action&search=$search");
    
    final resdata = (res.response?.data);
    if (res.response != null && res.response?.statusCode == 200) {
      final models = ModelClass.fromJson(res.response!.data);
      final result = models.data;

      if (isRefresh) {
        modeldata = result.data;
      } else {
        modeldata.addAll(result.data);
      }
      current_page++;
      totalPages = result.lastPage;
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }
    void updateStatus(
      { required int categoryid, required int status}) async {
    var m2 = {
  'model_categories': [
        json.encode({'category_id': categoryid, 'status': status})
      ].toString()
    };
    ApiResponse? res = await repo.postData(StringConst.UPDATE_MODEL_CATEGORIES,
        data:m2);

    // ApiResponse? res = await repo.postData(
    //   StringConst.UPDATE_MODEL_LIST,
    //   data: m2,
    // );
    // modeldata[index].status = 1;
    notifyListeners();
  }

  void updateBulkUpload(int status) async {
    print(status);
    var m = modeldata
        .map((e) => json.encode({'category_id': e.id, 'status': status}))
        .toList();
    var m2 = {'model_categories': m.toString()};
    print(m2);
 ApiResponse? res =
        await repo.postData(StringConst.UPDATE_MODEL_CATEGORIES, data: m2);
  }
}
//  var m2 = {
  // 'model_categories': [
  //       json.encode({'category_id': categoryid, 'status': status})
  //     ].toString()
  //   };