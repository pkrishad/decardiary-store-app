import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Widget customText(
    {required text, color, textSize, weight, align,ltrspacing}) {
  return
  Text(
    text,
    style:
     GoogleFonts.poppins(color: color,
      letterSpacing: ltrspacing,
     fontSize: textSize,fontWeight: weight,
    )
    ,overflow: TextOverflow.clip,
    textAlign: align,
  );
}
