// To parse this JSON data, do
//
//     final smsClass = smsClassFromJson(jsonString);

// ignore_for_file: constant_identifier_names

import 'dart:convert';

SmsClass smsClassFromJson(String str) => SmsClass.fromJson(json.decode(str));

String smsClassToJson(SmsClass data) => json.encode(data.toJson());

class SmsClass {
  SmsClass({
    required this.status,
    required this.data,
    required this.message,
  });

  final String status;
  final Data data;
  final String message;

  factory SmsClass.fromJson(Map<String, dynamic> json) => SmsClass(
    status: json["status"],
    data: Data.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
    "message": message,
  };
}

class Data {
  Data({
    required this.currentPage,
    required this.data,
    // required this.from,
    required this.lastPage,
    // required this.nextPageUrl,
    // required this.path,
    // required this.perPage,
    // required this.prevPageUrl,
    // required this.to,
    // required this.total,
  });

  final int currentPage;
  final List<Datum> data;
  // final int from;
  final int lastPage;
  // final String nextPageUrl;
  // final String path;
  // final int perPage;
  // final dynamic prevPageUrl;
  // final int to;
  // final int total;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    currentPage: json["current_page"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    // from: json["from"],
    lastPage: json["last_page"],
    // nextPageUrl: json["next_page_url"],
    // path: json["path"],
    // perPage: json["per_page"],
    // prevPageUrl: json["prev_page_url"],
    // to: json["to"],
    // total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    // "from": from,
    "last_page": lastPage,
    // "next_page_url": nextPageUrl,
    // "path": path,
    // "per_page": perPage,
    // "prev_page_url": prevPageUrl,
    // "to": to,
    // "total": total,
  };
}

class Datum {
  Datum({
    required this.notificationId,
    required this.type,
    required this.subject,
    required this.description,
    required this.pid,
    required this.createdAt,
    required this.model,
    required this.details,
  });

  final int notificationId;
  final int type;
  final Subject? subject;
  final Description? description;
  final String pid;
  final DateTime createdAt;
  final String model;
  final List<Detail> details;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    notificationId: json["notification_id"],
    type: json["type"],
    subject: subjectValues.map[json["subject"]],
    description: descriptionValues.map[json["description"]],
    pid: json["pid"],
    createdAt: DateTime.parse(json["created_at"]),
    model: json["model"],
    details: List<Detail>.from(json["details"].map((x) => Detail.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "notification_id": notificationId,
    "type": type,
    "subject": subjectValues.reverse[subject],
    "description": descriptionValues.reverse[description],
    "pid": pid,
    "created_at": createdAt.toIso8601String(),
    "model": model,
    "details": List<dynamic>.from(details.map((x) => x.toJson())),
  };
}

enum Description { YOU_GOT_A_NEW_CLIENT_ORDER }

final descriptionValues = EnumValues({
  "You got a new Client Order:": Description.YOU_GOT_A_NEW_CLIENT_ORDER
});

class Detail {
  Detail({
    required this.id,
    required this.imageId,
    required this.category,
    required this.product,
  });

  final int id;
  final String imageId;
  final Category? category;
  final String product;

  factory Detail.fromJson(Map<String, dynamic> json) => Detail(
    id: json["id"],
    imageId: json["image_id"],
    category: categoryValues.map[json["category"]],
    product: json["product"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "image_id": imageId,
    "category": categoryValues.reverse[category],
    "product": product,
  };
}

enum Category { MATERIAL, COLOUR }

final categoryValues = EnumValues({
  "Colour": Category.COLOUR,
  "Material": Category.MATERIAL
});

enum Subject { WORK_ADDED }

final subjectValues = EnumValues({
  "Work Added": Subject.WORK_ADDED
});

class EnumValues<T> {
  late Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}

