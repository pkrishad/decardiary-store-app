// ignore_for_file: unnecessary_import, unused_local_variable, avoid_print, unnecessary_const

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Models/MProvider/model_list.dart';
import 'package:storeadmin_app/View/Models/MProvider/updatemlist.dart';
import 'package:storeadmin_app/View/Models/ModelClass/listclass.dart';
import 'package:storeadmin_app/View/Models/ModelsectionScreen/mlistfilter.dart';
import 'package:storeadmin_app/View/Models/ModelsectionScreen/moddetails.dart';
import 'package:storeadmin_app/custom_widgets/bulkupdate.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textbutton.dart';
import 'package:storeadmin_app/custom_widgets/custom_textfield.dart';
import 'package:storeadmin_app/custom_widgets/custom_toggleswitchbutton.dart';
import 'package:storeadmin_app/custom_widgets/model_card.dart';

class Models extends StatefulWidget {
  const Models({Key? key}) : super(key: key);

  @override
  _ModelsState createState() => _ModelsState();
}

class _ModelsState extends State<Models> {
  final controller = TextEditingController();
  Datum? datum;
  bool togglevalue = false;
  bool isLoading = false;
  bool bulkupdate = false;
  late int data;
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  @override
  Widget build(BuildContext context) {
    checkConnection(context);
    Size size = MediaQuery.of(context).size;
    UpdateModellist update = Provider.of<UpdateModellist>(context);
    ProviderModellist modlist =
        Provider.of<ProviderModellist>(context, listen: false);
    print(datum?.globalStatus);
    return Consumer<ProviderModellist>(builder: (context, provider, child) {
      return Scaffold(
          backgroundColor: AppColors.white,
          body: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(children: [
                Row(
                  children: [
                    CustomContainer(
                      align: Alignment.center,
                      borderRadius: BorderRadius.circular(10),
                      width: size.width * 0.66,
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      color: AppColors.white,
                      height: size.height * 0.06,
                      child: CustomTextFormField(
                        controller: controller,
                        suffixIcon: InkWell(
                            onTap: () async {
                              final search = await provider.getModellist(
                                  search: controller.text.toUpperCase(),
                                  isRefresh: true);
                            },
                            child: const Icon(Icons.search_outlined)),
                        hintText: "Search",
                        hintColor: AppColors.hint,
                      ),
                    ),
                    SizedBox(width: size.width * 0.01),
                    const MlistFilter(),
                    SizedBox(width: size.width * 0.02),
                    Bulk(
                      ontap: () {
                        showModalBottomSheet(
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: const Radius.circular(20))),
                            context: context,
                            builder: ((builder) => CustomContainer(
                                height: size.height * 0.2,
                                width: size.width,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: size.height * 0.02,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          customText(
                                              text: "Bulk update",
                                              color: AppColors.dark,
                                              textSize: 20.0,
                                              weight: FontWeight.bold),
                                          CustomtextButton(
                                              onpressed: () {
                                                Navigator.pop(context);
                                              },
                                              text: "Cancel")
                                        ],
                                      ),
                                      SizedBox(
                                        height: size.height * 0.01,
                                      ),
                                      SizedBox(
                                        height: size.height * 0.03,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          customText(
                                            text: "Actions",
                                            textSize: 25.0,
                                          ),
                                          SizedBox(
                                            height: size.height * 0.02,
                                          ),
                                          Togglebtn(
                                            toggleButton: () {
                                              bulkupdate = !bulkupdate;

                                              if (!bulkupdate) {
                                                data = 0;
                                              } else if (bulkupdate) {
                                                data = 1;
                                              }
                                              provider.updateBulkUpload(data);
                                              Future.delayed(
                                                  const Duration(
                                                      microseconds: 10), () {
                                                refreshController
                                                    .requestRefresh(
                                                  needMove: true,
                                                  needCallback: true,
                                                );
                                                // refreshController
                                                //       .refreshCompleted();
                                              });

                                              Navigator.pop(context);
                                                                           data == 0 ?
                                                                                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: customText(text: '${provider.modellist.length} Models enabled successfully',color: AppColors.white),
            behavior: SnackBarBehavior.floating,duration: Duration(seconds: 3),
            backgroundColor: AppColors.dark)):                               
     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: customText(text: '${provider.modellist.length}  Models disabled successfully',color: AppColors.white),
            behavior: SnackBarBehavior.floating,duration: Duration(seconds: 3),
            backgroundColor: AppColors.dark));
                                             
                                            },
                                            left: provider
                                                    .modellist.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? 30.0
                                                : 0.0,
                                            right: provider
                                                    .modellist.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? 0.0
                                                : 30.0,
                                            togglecolor: provider
                                                    .modellist.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? Colors.green[200]
                                                : Colors.grey[300],
                                            icon: provider
                                                    .modellist.first.status
                                                    .toString()
                                                    .contains('0')
                                                 ? const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  )
                                                : const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  ),
                                          ),
                                        ],
                                      ),
                                    ]))));
                      },
                      icon: Icon(
                        Icons.fact_check_outlined,
                        color: AppColors.red.withOpacity(0.8),
                      ),
                      color: AppColors.white10,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Expanded(
                    child: SmartRefresher(
                        controller: refreshController,
                        enablePullUp: true,
                        enablePullDown: true,
                        footer: CustomFooter(
                          builder: (BuildContext context, LoadStatus? mode) {
                            Widget child;
                            if (mode == LoadStatus.idle) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "Swipe up to load more",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.loading) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const CupertinoActivityIndicator(),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    customText(
                                        text: "Loading..",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.failed) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Icon(
                                      Icons.error_outlined,
                                      color: AppColors.hint2,
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    customText(
                                        text: "No more Data",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.canLoading) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "Swipe up to load more",
                                        color: AppColors.hint2)
                                  ]);
                            } else {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "No more Data",
                                        color: AppColors.hint2)
                                  ]);
                            }
                            return child;
                          },
                        ),
                        onRefresh: () async {
                          final result =
                              await modlist.getModellist(isRefresh: true);
                          if (result) {
                            refreshController.refreshCompleted();
                          } else {
                            refreshController.refreshFailed();
                          }
                        },
                        onLoading: () async {
                          final result = await modlist.getModellist();
                          if (result) {
                            refreshController.loadComplete();
                          } else {
                            refreshController.loadFailed();
                          }
                        },
                        child: isLoading
                            ? const Center(
                                child: const CircularProgressIndicator())
                            : provider.modellist.isNotEmpty
                                ? ListView.separated(
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return SizedBox(
                                        height: size.height * 0.009,
                                      );
                                    },
                                    itemCount: provider.modellist.length,
                                    itemBuilder: (BuildContext context, index) {
                                      final modl = provider.modellist[index];
                                      return ModelCard(
                                        stock: customText(
                                            text: modl.globalStatus == 0
                                                ? "In Stock"
                                                : "Outof Stock"),
                                        ontap: () async {
                                          await Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      ModelDetails(
                                                        data: modl,
                                                      )));
                                        },
                                        bgcolor: modl.status == 0
                                            ? Colors.green.shade100
                                            : Colors.red.shade700,
                                        txtactive: customText(
                                            text: modl.status == 0
                                                ? "Active"
                                                : "Inactive"),
                                                onhover: (togglevalue){
                                                       
                                                          togglevalue = !togglevalue;
                                                       
                                                      },
                                        toggleButton: () {
                                          togglevalue = !togglevalue;
                                          // print(togglevalue);
                                          if (!togglevalue) {
                                            data = 0;
                                          } else if (togglevalue) {
                                            data = 1;
                                          }
                                          
                                          provider.updateStatus(
                                              // index: index,
                                              modelid: modl.id,
                                              status: data);

                                          Future.delayed(
                                              const Duration(milliseconds: 200),
                                              () {
                                           
                                            provider.getModellist(
                                                perpage: 'all',
                                                isRefresh: true);
                                                });
                                            
                                          
                                        },
                                        left: modl.status == 0 ? 30.0 : 0.0,
                                        right: modl.status == 0 ? 0.0 : 30.0,
                                        togglecolor: modl.status == 0
                                            ? Colors.green[200]
                                            : Colors.grey.shade300,
                                        icon: modl.status == 0
                                            ? const Icon(
                                               Icons.circle,
                                                color: Colors.white,
                                                size: 35.0,
                                              )
                                            : const Icon(
                                                Icons.circle,
                                                color: Colors.white,
                                                size: 35.0,
                                              ),
                                        activeontap: () {},
                                        image: modl.image.isEmpty
                                            ? const Center(
                                                child: Icon(
                                                Icons
                                                    .image_not_supported_rounded,
                                                color: AppColors.hint,
                                                size: 30.0,
                                              ))
                                            : Image.network(modl.image,
                                                filterQuality:
                                                    FilterQuality.low,
                                                loadingBuilder:
                                                    (BuildContext context,
                                                        Widget child,
                                                        ImageChunkEvent?
                                                            loadingProgress) {
                                                if (loadingProgress == null) {
                                                  return child;
                                                }
                                                return Center(
                                                  child:
                                                      CircularProgressIndicator(
                                                    color: AppColors.dark
                                                        .withOpacity(0.5),
                                                    value: loadingProgress
                                                                .expectedTotalBytes !=
                                                            null
                                                        ? loadingProgress
                                                                .cumulativeBytesLoaded /
                                                            loadingProgress
                                                                .expectedTotalBytes!
                                                        : null,
                                                  ),
                                                );
                                              }, fit: BoxFit.fill),
                                        modelname: modl.name,
                                         textadmin: modl.globalStatus == 0
                                          ? ''
                                          : 'Removed by Admin',
                                      );
                                    },
                                  )
                                : Center(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        const Icon(
                                          Icons.search_off_rounded,
                                          color: AppColors.hint,
                                          size: 50.0,
                                        ),
                                        customText(text: "No Models")
                                      ],
                                    ),
                                  )))
              ])));
    });
  }
}
