import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/custom_widgets/custom_iconbutton.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';

AppBar dashAppbar() {
  return AppBar(
    backgroundColor: Colors.blue.shade900,
    elevation: 0,
    toolbarHeight: 80.0,
    leading: CustomIconButton(
      icon: Icons.notifications_none_outlined,
      btnColor: AppColors.white,
      size: 35.0,
      onPressed: (){},
    ),
    title: Container(
      width: 250,
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(8.0)),
      child: Center(
          child: customText(text: '# STORE', textSize: 25.0, ltrspacing: 2.0,color: AppColors.white)),
    ),
    centerTitle: true,
    actions: [
      CustomIconButton(
        onPressed: () {},
        icon: Icons.person_outlined,
        btnColor: AppColors.white,
        size: 35.0,
      ),
      const SizedBox(width: 8),
    ],
  );
}



