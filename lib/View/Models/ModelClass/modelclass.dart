

// To parse this JSON data, do
//
//     final modelClass = modelClassFromJson(jsonString);

import 'dart:convert';

ModelClass modelClassFromJson(String str) => ModelClass.fromJson(json.decode(str));

String modelClassToJson(ModelClass data) => json.encode(data.toJson());

class ModelClass {
  ModelClass({
    required this.status,
    required this.data,
    required this.message,
  });

  final String status;
  final Data data;
  final String message;

  factory ModelClass.fromJson(Map<String, dynamic> json) => ModelClass(
    status: json["status"],
    data: Data.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
    "message": message,
  };
}

class Data {
  Data({
    required this.currentPage,
    required this.data,
    // required this.from,
    required this.lastPage,
    // required this.nextPageUrl,
    // required this.path,
    // required this.perPage,
    // required this.prevPageUrl,
    // required this.to,
    // required this.total,
  });

  final int currentPage;
  final List<Datum> data;
  // final int from;
  final int lastPage;
  // final dynamic nextPageUrl;
  // final String path;
  // final int perPage;
  // final dynamic prevPageUrl;
  // final int to;
  // final int total;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    currentPage: json["current_page"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    // from: json["from"],
    lastPage: json["last_page"],
    // nextPageUrl: json["next_page_url"],
    // path: json["path"],
    // perPage: json["per_page"],
    // prevPageUrl: json["prev_page_url"],
    // to: json["to"],
    // total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    // "from": from,
    "last_page": lastPage,
    // "next_page_url": nextPageUrl,
    // "path": path,
    // "per_page": perPage,
    // "prev_page_url": prevPageUrl,
    // "to": to,
    // "total": total,
  };
}

class Datum {
  Datum({
    required this.id,
    required this.title,
    required this.categoriesImage,
    required this.viewAs,
    required this.globalStatus,
    required this.storeId,
    required this.modelCategoriesId,
    required this.status,
  });

  final int id;
  final String title;
  final String categoriesImage;
  final int viewAs;
  final int globalStatus;
  final String storeId;
  final String modelCategoriesId;
  int status;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    title: json["title"],
    categoriesImage: json["categories_image"],
    viewAs: json["view_as"],
    globalStatus: json["global_status"],
    storeId: json["store_id"],
    modelCategoriesId: json["model_categories_id"],
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "categories_image": categoriesImage,
    "view_as": viewAs,
    "global_status": globalStatus,
    "store_id": storeId,
    "model_categories_id": modelCategoriesId,
    "status": status,
  };
}
