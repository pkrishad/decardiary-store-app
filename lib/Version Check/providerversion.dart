// ignore_for_file: unused_local_variable


import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/Version%20Check/versionclass.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';

import '../repo.dart';

class ProviderVersion with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;
  ProviderVersion( {
    required this.repo,
    required this.dioClient,
  });
  Version? version;

  Future<void> versionCheck(BuildContext context) async {
    ApiResponse res = await repo.getdata(StringConst.API_VERSION);
    if (kDebugMode) {
      print(res.response!.statusCode);
    }
    if (res.response != null && res.response!.statusCode == 200) {
      version = Version.fromJson(res.response!.data);
    } else if (res.error != "401") {
      if (kDebugMode) {
        print(res.error);
      }
    }
  }
}
