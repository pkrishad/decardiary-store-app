import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

class CustomCard extends StatefulWidget {
  final Widget? child;
  const CustomCard({Key? key, this.child}) : super(key: key);

  @override
  State<CustomCard> createState() => _CustomCardState();
}

class _CustomCardState extends State<CustomCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
elevation: 0.6,
        color: AppColors.white,
        shadowColor: AppColors.black,
        margin: const EdgeInsets.only(
        left: 5, right: 5, top: 6),
    shape: RoundedRectangleBorder(
    // side: BorderSide(width: 2, color: AppColors.white10),
    borderRadius: BorderRadius.circular(10)),
    child: widget.child);
  }
}
