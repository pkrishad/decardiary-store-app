import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/DashBoard/dashmodel.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textbutton.dart';
import 'package:url_launcher/url_launcher.dart';


class BtmBannerinner extends StatelessWidget {
  final BottomBanners? btm;
  const BtmBannerinner({Key? key, required this.btm}) : super(key: key);

  @override
  Widget build(BuildContext context) {
       checkConnection(context);
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.dark,
        title: customText(text: btm!.title) ,
      ),
      backgroundColor: AppColors.white,
      body: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CustomContainer(
            width: size.width,
            height: size.height * 0.3,
            color: AppColors.white,
            child: ClipRRect(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight:Radius.circular(30) ),child:  btm!.image == null
                                                  ? Icon(Icons.photo_size_select_actual,)
                                                  : Image.network(btm!.image,fit: BoxFit.fill,)),
          ),
          Padding(padding: EdgeInsets.all(30),child: 
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
           
             customText(text: "${btm!.subtitle}",textSize: 20.0,weight: FontWeight.bold),
            SizedBox(height: size.height*0.01,),
          customText(text: "${btm!.description}",textSize: 20.0,color: AppColors.hint),
  SizedBox(height: size.height*0.2),
  
  Row(mainAxisAlignment: MainAxisAlignment.center,
    children: [
      CustomtextButton(bgColor: AppColors.primaryColor,fgColor: AppColors.white,padding: EdgeInsets.symmetric(horizontal: size.width*0.3,vertical: 15),
        onpressed: ()async{
        var url = "${btm?.link}";
                                              if (await canLaunch(url)) {
                                                await launch(url);
                                              } else {
                                                throw 'Could not launch $url';
                                              }
      }, text: "Explore"),
    ],
  )
          ]),),
          
        ],
      ),
    );
  }
}
