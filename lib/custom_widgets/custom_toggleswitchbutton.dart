import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

class Togglebtn extends StatelessWidget {
  final void Function()? toggleButton;
  final void Function()? onend;
  final  void Function(bool)? onhover;
  final Color? togglecolor;
  final double? left;
  final double? right;
  final Widget? icon;
  const Togglebtn(
      {Key? key,
      this.toggleButton,
      this.onend,
      this.left,
      this.right,
      this.togglecolor,
      this.icon, this.onhover})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return InkWell(
      onTap: toggleButton,
      overlayColor: MaterialStateProperty.all(AppColors.white),
      onHover: onhover,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 150),
        height: size.height * 0.045,
        width: size.width * 0.18,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20), color: togglecolor),
        child: Stack(
          children: <Widget>[
            AnimatedPositioned(
                duration: const Duration(milliseconds: 150),
                curve: Curves.linear,
                top: size.aspectRatio * 2.0,
                left: left,
                right: right,
                onEnd: onend,
                child: AnimatedSwitcher(
                  duration: const Duration(milliseconds: 100),
                  reverseDuration: const Duration(milliseconds: 100),
                  transitionBuilder:
                      (Widget child, Animation<double> animation) {
                    return ScaleTransition(
                      filterQuality: FilterQuality.low,
                      scale: animation,
                      child: child,
                    );
                  },
                  child: icon,
                ))
          ],
        ),
      ),
    );
  }
}

//  left: toggleValue ? 33.0 : 0.0,
              // right: toggleValue ? 0.0 : 33.0,
              // toggleValue ? Colors.green[300] : AppColors.dark
              // toggleValue
              //             ? const Icon(
              //                 Icons.check_circle,
              //                 color: Colors.white,
              //                 size: 35.0,
              //               )
              //             : const Icon(
              //                 Icons.remove_circle_outline,
              //                 color: Colors.white,
              //                 size: 35.0,
              //               )