// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

class CustomDDF extends StatelessWidget {
  final List<DropdownMenuItem<String>>? list;
  final String? controller;
  final onchanged;
  final String? labeltext;
  final onTap;
  const CustomDDF({Key? key, this.controller,this.onTap, this.labeltext, this.list, this.onchanged,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      alignment: Alignment.centerRight,
      iconSize: 30.0,
      iconEnabledColor: AppColors.dark,
      onTap: onTap,
      value: controller,
      isExpanded: true,
      items: list,
      onChanged: onchanged,
     
      style: const TextStyle(
          fontFamily: 'Roboto', color: Colors.black, fontSize: 15),
      decoration: InputDecoration(
        border: InputBorder.none,
        labelText: labeltext,
        labelStyle: const TextStyle(
            fontFamily: 'Roboto', fontSize: 15, color: Colors.black),
        contentPadding: const EdgeInsets.symmetric(horizontal: 9, vertical: 5),
        floatingLabelBehavior: FloatingLabelBehavior.auto,
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: AppColors.blue,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: OutlineInputBorder(borderSide: const BorderSide(
            color: AppColors.hint,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }

 
}
