import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/DashBoard/body.dart';
import 'package:storeadmin_app/View/DashBoard/dashmodel.dart';
import 'package:storeadmin_app/View/DashBoard/providerdash.dart';
import 'package:storeadmin_app/View/Profile/profile.dart';
import 'package:storeadmin_app/View/Notifications/notification.dart';
import 'package:storeadmin_app/View/Profile/providerprofile.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_iconbutton.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/fill_outline_button.dart';
import 'package:storeadmin_app/custom_widgets/myclipper.dart';
import 'package:storeadmin_app/custom_widgets/myseperator.dart';
import 'package:storeadmin_app/notification_service.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({Key? key}) : super(key: key);
  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  bool isFilled = false;
  String notificationTitle = '';
  String notificationBody = '';
  String notificationData = '';
  String notificationImg = '';
  final firebaseMessaging = FCM();
  _changeImg(String msg) => setState(() => notificationImg = msg);
  _changeData(String msg) => setState(() => notificationData = msg);
  _changeBody(String msg) => setState(() => notificationBody = msg);
  _changeTitle(String msg) => setState(() => notificationTitle = msg);
  @override
  void initState() {
    final firebaseMessaging = FCM();
    firebaseMessaging.setNotifications();
    firebaseMessaging.imageCtlr.stream.listen(_changeImg);
    firebaseMessaging.streamCtlr.stream.listen(_changeData);
    firebaseMessaging.bodyCtlr.stream.listen(_changeBody);
    firebaseMessaging.titleCtlr.stream.listen(_changeTitle);
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      print("message recieved");
      print(event.notification!.body);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0)),
              title:
                  customText(text: notificationTitle, weight: FontWeight.bold),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  notificationImg.isEmpty ? Text(""):  const Divider(),
                  notificationImg.isEmpty ? Text(""): 
                  ClipRRect(borderRadius: BorderRadius.circular(12.0),child: Image.network(notificationImg,fit: BoxFit.fill,)),
                                    notificationImg.isEmpty ? Text(""):  const Divider(),
                  customText(text: notificationBody,align: TextAlign.center,color: AppColors.hint2),
                ],
              ),
              // actions: [
              //   FillButton(
              //     fillcolor: const Color(0xFFDF6E8A).withOpacity(0.2),
              //     text: "Ok",
              //     press: () => Navigator.of(context).pop(),
              //     isFilled: false,
              //   ),
              // ],
            );
          });
        
    });
     Provider.of<ProviderProfile>(context, listen: false).userProfile(context);
    super.initState();
  }

  DashModel? dashModel;
  bool isloading = false;
  @override
  Widget build(BuildContext context) {
    checkConnection(context);
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      Provider.of<ProviderDash>(context, listen: false).getDashBoard(context);
    });
    return Consumer<ProviderDash>(builder: (context, provider, child) {
      return SafeArea(
          child: Scaffold(
                      backgroundColor: Colors.white,
                      // appBar: AppBar(
                      //   leadingWidth: 76.0,
                      //   elevation: 0,
                      //   toolbarHeight: 70,
                      //   backgroundColor: AppColors.white,
                      //   leading: GestureDetector(
                      //     onTap: () {
                      //       Navigator.push(
                      //         context,
                      //         MaterialPageRoute(
                      //           builder: (context) {
                      //             return const Msg();
                      //           },
                      //         ),
                      //       );
                      //     },
                      //     child: const Icon(
                      //       Icons.notifications_outlined,
                      //       color: AppColors.dark,
                      //       size: 30.0,
                      //     ),
                      //   ),
                      //   actions: [
                      //     GestureDetector(
                      //       onTap: () {
                      //         Navigator.push(
                      //           context,
                      //           MaterialPageRoute(
                      //             builder: (context) {
                      //               return const Profile();
                      //             },
                      //           ),
                      //         );
                      //       },
                      //       child: const Icon(
                      //         Icons.person_outline_rounded,
                      //         color: AppColors.dark,
                      //         size: 30.0,
                      //       ),
                      //     ),
                      //     const SizedBox(
                      //       width: 25,
                      //     ),
                      //   ],
                      // ),
                      body: const Body())
                );
    });
  }
}
