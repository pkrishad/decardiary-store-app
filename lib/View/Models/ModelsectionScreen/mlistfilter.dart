import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Models/MProvider/model_list.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_dropdwnfield.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textbutton.dart';

class MlistFilter extends StatefulWidget {
  const MlistFilter({Key? key}) : super(key: key);

  @override
  _MlistFilterState createState() => _MlistFilterState();
}

class _MlistFilterState extends State<MlistFilter> {
  final sort = ["Newest first", "Oldest first"];
  final sstatus = ["All", "Active", "Inactive"];
  final actions = ["All", "Enabled", "Disabled"];
  final stock = ["All", "Active", "Inactive"];
  final catid = ["All", "Basin", "Kitchen", "Wardrobe", "Doors"];
  final page = [ "All","16", "32",];
  String? catidcontroller;
  String cat = "";
  String? pagecontroller;
  String pge = "";
  String? stcontroller;
  String sortq = "";
  String? srtcontroller;
  String statq = "";
  String actq = "";
  String? actcontroller;
  String stockq = "";
  String? stkcontroller;
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  // bool _isChecked = false;
  String? dropdownValue;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Consumer<ProviderModellist>(builder: (context, provider, child) {
      return GestureDetector(
        onTap: () async {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  customText(text: "Filter By"),
                ],
              ),
              content: Padding(
                padding: const EdgeInsets.all(0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(children: [
                          CustomContainer(
                            borderRadius: BorderRadius.circular(10),
                            height: 48,
                            color: AppColors.white10,
                            width: MediaQuery.of(context).size.width * 0.33,
                            margin: const EdgeInsets.all(5),
                            child: CustomDDF(
                              controller: srtcontroller,
                              labeltext: "Sort",
                              list: sort.map(droplist2).toList(),
                              onchanged: (value) => setState(() {
                                srtcontroller = value;
                                sortq = srtcontroller.toString();
                                if (srtcontroller.toString() ==
                                    "Newest First") {
                                  sortq = "desc";
                                }
                                if (srtcontroller.toString() ==
                                    "Oldest First") {
                                  sortq = "asc";
                                }
                              }),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          CustomContainer(
                            height: 48,
                            borderRadius: BorderRadius.circular(10),
                            color: AppColors.white10,
                            width: MediaQuery.of(context).size.width * 0.31,
                            margin: const EdgeInsets.all(5),
                            child: CustomDDF(
                              controller: actcontroller,
                              labeltext: "Actions",
                              list: actions.map(droplist2).toList(),
                              onchanged: (value) => setState(() {
                                actcontroller = value;
                                actq = actcontroller.toString();
                                if (actcontroller.toString() == "All") {
                                  actq = "all";
                                }
                                if (actcontroller.toString() == "Enabled") {
                                  actq = "0";
                                }
                                if (actcontroller.toString() == "Disabled") {
                                  actq = "1";
                                }
                              }),
                            ),
                          ),
                        ]),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(children: [
                          CustomContainer(
                            height: 48,
                            color: AppColors.white10,
                            borderRadius: BorderRadius.circular(10),
                            width: MediaQuery.of(context).size.width * 0.33,
                            margin: const EdgeInsets.all(5),
                            child: CustomDDF(
                              controller: stkcontroller,
                              labeltext: "Status",
                              list: stock.map(droplist2).toList(),
                              onchanged: (value) => setState(() {
                                stkcontroller = value;
                                stockq = stkcontroller.toString();
                                if (stkcontroller.toString() == "All") {
                                  stockq = "all";
                                }
                                if (stkcontroller.toString() == "Active") {
                                  stockq = "0";
                                }
                                if (stkcontroller.toString() ==
                                    "Inactive") {
                                  stockq = "1";
                                }
                              }),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          CustomContainer(
                            height: 48,
                            color: AppColors.white10,
                            borderRadius: BorderRadius.circular(10),
                            width: MediaQuery.of(context).size.width * 0.31,
                            margin: const EdgeInsets.all(5),
                            child: CustomDDF(
                              controller: catidcontroller,
                              labeltext: "Category",
                              list: catid.map(droplist2).toList(),
                              onchanged: (value) => setState(() {
                                catidcontroller = value;
                                cat = catidcontroller.toString();
                                if (catidcontroller.toString() == "All") {
                                  cat = "all";
                                }
                                if (catidcontroller.toString() == "Doors") {
                                  cat = "41";
                                }
                                if (catidcontroller.toString() == "Wardrobe") {
                                  cat = "42";
                                }
                                if (catidcontroller.toString() == "Kitchen") {
                                  cat = "43";
                                }
                                if (catidcontroller.toString() == "Basin") {
                                  cat = "45";
                                }
                              }),
                            ),
                          ),
                        ]),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            CustomContainer(
                              height: 48,
                              color: AppColors.white10,
                              borderRadius: BorderRadius.circular(10),
                              width: MediaQuery.of(context).size.width * 0.31,
                              margin: const EdgeInsets.all(5),
                              child: CustomDDF(
                                controller: pagecontroller,
                                labeltext: "Per page",
                                list: page.map(droplist2).toList(),
                                onchanged: (value) => setState(() {
                                  pagecontroller = value;
                                  pge = pagecontroller.toString();
                                  if (pagecontroller.toString() == "All") {
                                    pge = "all";
                                  }
                                  if (pagecontroller.toString() == "16") {
                                    pge = "16";
                                  }
                                  if (pagecontroller.toString() == "32") {
                                    pge = "32";
                                  }
                                }),
                              ),
                            ),
                          ],
                        )
                      ]),
                ),
              ),
              actions: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      CustomtextButton(
                          text: "Cancel",
                          onpressed: () {
                            statq = '';
                            sortq = '';
                            actq = '';
                            stockq = '';
                            pge = '';
                            provider.getModellist(isRefresh: true);
                           
                            stcontroller?.replaceAll(
                                statq, stcontroller = "All");
                            srtcontroller?.replaceAll(
                                sortq, srtcontroller = "Newest First");
                            actcontroller?.replaceAll(
                                actq, actcontroller = "All");
                            stkcontroller?.replaceAll(
                                stockq, stkcontroller = "All");
                            pagecontroller?.replaceAll(
                                pge, pagecontroller = "All");
                               catidcontroller?.replaceAll(
                                cat, catidcontroller = "All"); 
                            Navigator.pop(context,true);
                          }),
                      MaterialButton(
                        color: AppColors.white10,
                        onPressed: ()  {
                          Navigator.pop(context,true);

                         provider.getModellist(
                            isRefresh: true,
                            perpage: pge,
                            sort: sortq,
                            action: actq,
                            status: stockq,
                            category: cat,
                          );
                         
                        },
                        child: const Text("Apply"),
                      ),
                    ]),
              ],
            ),
          );
        },
        behavior: HitTestBehavior.translucent,
        child: CustomContainer(
          shadow: const [
            BoxShadow(blurRadius: 5.0, spreadRadius: 0.2, color: AppColors.hint)
          ],
          border: Border.all(color: AppColors.dark, width: 0.5),
          borderRadius: BorderRadius.circular(10),
          align: Alignment.center,
          padding: const EdgeInsets.all(10),
          color: AppColors.white,
          height: size.height * 0.06,
          width: size.width * 0.12,
          child: const Icon(Icons.filter_alt_outlined),
        ),
      );
    });
  }

  DropdownMenuItem<String> droplist2(String sstatus) =>
      DropdownMenuItem(value: sstatus, child: Text(sstatus));
}
