import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/products/ProProvider/pcategry.dart';
import 'package:storeadmin_app/View/products/ProductScreen/pfilter.dart';
import 'package:storeadmin_app/View/products/productClass/categryclass.dart';
import 'package:storeadmin_app/custom_widgets/bulkupdate.dart';
import 'package:storeadmin_app/custom_widgets/categorypro.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textbutton.dart';
import 'package:storeadmin_app/custom_widgets/custom_textfield.dart';
import 'package:storeadmin_app/custom_widgets/custom_toggleswitchbutton.dart';
import 'package:storeadmin_app/custom_widgets/dialog.dart';

class PCategory extends StatefulWidget {
  const PCategory({Key? key}) : super(key: key);

  @override
  _PCategoryState createState() => _PCategoryState();
}

class _PCategoryState extends State<PCategory> {
  late List<Datum> prod = [];
  bool isenabled = false;
  bool isLoading = false;
  bool bulkupdate = false;
  late int data;
  TextEditingController searchcontroller = TextEditingController();
  int page = 1;
  bool _enabled = false;
  late ScrollController _scrollController;
  String searchtext = "";
@override
  void initState() {
    super.initState();
  _scrollController = new ScrollController()..addListener(_scrollListener);
     Provider.of<ProviderCProduct>(context, listen: false).getCProducts();
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      if (page != -1) {
      Provider.of<ProviderCProduct>(context, listen: false).getCProducts();
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    checkConnection(context);
    Size size = MediaQuery.of(context).size;
    ProviderCProduct prodata =
        Provider.of<ProviderCProduct>(context, listen: false);

    return Consumer<ProviderCProduct>(builder: (context, provider, child) {
      return Scaffold(
          backgroundColor: AppColors.white,
          body: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(children: [
                Row(
                  children: [
                    CustomContainer(
                      align: Alignment.center,
                      borderRadius: BorderRadius.circular(10),
                      width: size.width * 0.66,
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      color: AppColors.white,
                      height: size.height * 0.06,
                      child: CustomTextFormField(
                          controller: searchcontroller,
                          suffixIcon: InkWell(
                              onTap: () async {
                                provider.getCProducts(
                                  
                                );
                              },
                              child: const Icon(Icons.search_outlined)),
                          hintText: "Search",
                          hintColor: AppColors.hint),
                    ),
                    SizedBox(width: size.width * 0.01),
                    const PFilter(),
                    SizedBox(width: size.width * 0.02),
                    Bulk(
                      ontap: () {
                        showModalBottomSheet(
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20))),
                            context: context,
                            builder: ((builder) => CustomContainer(
                                height: size.height * 0.2,
                                width: size.width,
                                padding: const EdgeInsets.symmetric(horizontal: 20),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: size.height * 0.02,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          customText(
                                              text: "Bulk update",
                                              color: AppColors.dark,
                                              textSize: 20.0,
                                              weight: FontWeight.bold),
                                          CustomtextButton(
                                              onpressed: () {
                                                Navigator.pop(context);
                                              },
                                              text: "Cancel")
                                        ],
                                      ),
                                      SizedBox(
                                        height: size.height * 0.01,
                                      ),
                                      SizedBox(
                                        height: size.height * 0.03,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          customText(
                                              text: "Actions", textSize: 25.0),
                                          SizedBox(
                                            height: size.height * 0.02,
                                          ),
                                        
                                         
                                          Togglebtn(
                                            toggleButton: () {
                                              bulkupdate = !bulkupdate;

                                              if (!bulkupdate) {
                                                data = 0;
                                              } else if (bulkupdate) {
                                                data = 1;
                                              }
                                              provider.updateBulkUpload(data);
                                             
                                                // refreshController
                                                //       .refreshCompleted();
                                              

                                              Navigator.pop(context);
                                            },
                                            left: provider.prodata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? 30.0
                                                : 0.0,
                                            right: provider.prodata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? 0.0
                                                : 30.0,
                                            togglecolor: provider
                                                    .prodata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? Colors.green[200]
                                                : Colors.grey[300],
                                            icon: provider.prodata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  )
                                                : const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  ),
                                          ),
                                        ],
                                      ),
                                    ]))));
                      },
                      icon: Icon(
                        Icons.fact_check_outlined,
                        color: AppColors.red.withOpacity(0.8),
                      ),
                      color: AppColors.white10,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
               Expanded(
                  
                    child:  RefreshIndicator(
      onRefresh: () async{
         prodata.getCProducts(   
                          );
       }, child :isLoading
                            ? const Center(child: CircularProgressIndicator())
                            : provider.prodata.isNotEmpty
                                ? ListView.separated(controller:_scrollController,
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return SizedBox(
                                        height: size.height * 0.009,
                                      );
                                    },
                                    itemCount: provider.prodata.length,
                                    itemBuilder: (BuildContext context, index) {
                                      final pro = provider.prodata[index];
                                      return Card(
                                        elevation: 2,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: CategoryPro(
                                            image: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                child: Image.network(
                                                  pro.image,
                                                  fit: BoxFit.fill,
                                                  width: 255,
                                                )),
                                            text: pro.name,
                                            bgcolor: pro.status == 0
                                                ? Colors.green.shade100
                                                : Colors.red.shade700,
                                            activetxt: customText(
                                                text: pro.status == 0
                                                    ? "Active"
                                                    : "Inactive"),

                                            // toggle on tap:
                                            onhover: (isenabled) {
                                              isenabled = !isenabled;
                                            },
                                            toggleButton: () {
                                              isenabled = !isenabled;
                                              if (!isenabled) {
                                                data = 0;
                                              } else {
                                                data = 1;
                                              }

                                              provider.updateStatus(
                                                // index: index,
                                                categoryid: pro.id,
                                                status: data,
                                              );

                                              // Future.delayed(
                                              //     const Duration(
                                              //         milliseconds: 200), () {
                                              // //   provider.getCProducts(context,
                                              // //       perpage: 'all',
                                              // //       isRefresh: true);
                                              // });
                                            },
                                            left: pro.status == 0 ? 30.0 : 0.0,
                                            right: pro.status == 0 ? 0.0 : 30.0,
                                            togglecolor: pro.status == 0
                                                ? Colors.green[200]
                                                : Colors.grey.shade300,
                                            icon: pro.status == 0
                                                ? const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  )
                                                : const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  )),
                                      );
                                    })
                                : Center(
                                    child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Icon(
                                        Icons.search_off_rounded,
                                        color: AppColors.hint,
                                        size: 50.0,
                                      ),
                                      customText(text: "No Product Categories.")
                                    ],
                                  ))))
              ])));
    });
  }
}
