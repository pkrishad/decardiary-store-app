// ignore_for_file: prefer_const_constructors_in_immutables, avoid_print

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/Version%20Check/donotask.dart';
import 'package:storeadmin_app/Version%20Check/providerversion.dart';
import 'package:storeadmin_app/custom_widgets/custom_button.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:url_launcher/url_launcher.dart';

class UpdateApp extends StatefulWidget {
  final Widget? child;

  UpdateApp({Key? key, this.child}) : super(key: key);

  @override
  _UpdateAppState createState() => _UpdateAppState();
}

class _UpdateAppState extends State<UpdateApp> {
  @override
  void initState() {
    super.initState();
    final version = Provider.of<ProviderVersion>(context, listen: false);
    version.versionCheck(context);
    checkLatestVersion(context);
  }

  checkLatestVersion(context) async {
    await Future.delayed(const Duration(seconds: 7));
    final ver = Provider.of<ProviderVersion>(context, listen: false);
    double minAppVersion = double.parse(ver.version!.supportingVersion);
    double latestAppVersion = double.parse(ver.version!.version);
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    double apkVersion =
        double.parse(packageInfo.version.trim().replaceAll(".", ""));
    var currentVersion = (apkVersion / 100.0);

// condition
    if (minAppVersion > currentVersion) {
      _showCompulsoryUpdateDialog(
        context,
        "Please update the app to continue",
      );
    } else if (latestAppVersion > currentVersion) {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      bool? showUpdates = false;
      showUpdates = sharedPreferences.getBool("UpdateKey");
      if (showUpdates != null && showUpdates == false) {
        return;
      }
      _showOptionalUpdateDialog(
        context,
        "A newer version of the app is available",
      );
      print('Update available');
    } else {
      print('App is up to date');
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.child!;
  }

  _showOptionalUpdateDialog(context, String message) async {
    await showDialog<String>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        String title = "App Update Available";
        String btnLabel = "Update Now";
        String btnLabelCancel = "Later";
        String btnLabelDontAskAgain = "Don't ask me again";
        return DoNotAskAgainDialog(
          "UpdateKey",
          title,
          message,
          btnLabel,
          btnLabelCancel,
          _onUpdateNowClicked,
          doNotAskAgainText:
              Platform.isIOS ? btnLabelDontAskAgain : 'Never ask again',
        );
      },
    );
  }

  _onUpdateNowClicked() async {
    final ver = Provider.of<ProviderVersion>(context, listen: false);
    print('On update app clicked');
    var whatsappUrl =
        "whatsapp://send?phone=${ver.version!.supportNumber}&text=Can't having access to the app due to update dialog";
    await canLaunch(whatsappUrl)
        ? launch(whatsappUrl)
        : print(
            "open whatsapp app link or do a snackbar with notification that there is no whatsapp installed");
  }

  _showCompulsoryUpdateDialog(context, String message) async {
    await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "App Update Available";
        String btnLabel = "Update Now";
        return Platform.isIOS
            ? CupertinoAlertDialog(
                title: customText(text: title),
                content: customText(text: message, textSize: 8.0),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text(
                      btnLabel,
                    ),
                    isDefaultAction: true,
                    onPressed: _onUpdateNowClicked,
                  ),
                ],
              )
            : AlertDialog(
                title: customText(
                    text: title,
                    color: AppColors.dark,
                    weight: FontWeight.bold),
                content: customText(text: message, textSize: 18.0),
                actions: <Widget>[
                  CustomElevatedButton(
                      bgColor: AppColors.white,
                      text: customText(text: "Update Now"),fgColor: AppColors.primaryColor,
                      onpress: () async {
                        final ver = Provider.of<ProviderVersion>(context,
                            listen: false);
                        if (kDebugMode) {
                          print('On update app clicked');
                        }
                        var whatsappUrl =
                            "whatsapp://send?phone=${ver.version!.supportNumber}&text=Can't having access to the app due to update dialog";
                        await canLaunch(whatsappUrl)
                            ? launch(whatsappUrl)
                            : print(
                                "open whatsapp app link or do a snackbar with notification that there is no whatsapp installed");
                      }),
                ],
              );
      },
    );
  }
}
// final postModel = Provider.of<PostDataProvider>(context);
