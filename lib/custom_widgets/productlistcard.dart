// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/custom_widgets/activebar.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_toggleswitchbutton.dart';

class ProductlistCard extends StatelessWidget {
  final void Function()? ontap;
  final txtactive;
  final activeontap;

  final void Function()? toggleButton;
  final void Function(bool)? onhover;
  final void Function(bool)? onhover2;
  final onend;
  final image;
  final modelname;
  final stock;
  final bgcolor;
  final value;
  final onchanged;
  final double? left;
  final textadmin;

  final double? right;
  final Color? togglecolor;
  final Widget? icon;
  final double? left2;

  final double? right2;
  final Color? togglecolor2;
  final Widget? icon2;

  final void Function()? stocktoggleButton;
  const ProductlistCard(
      {Key? key,
      this.ontap,
      this.stocktoggleButton,
      this.txtactive,
      this.activeontap,
      this.image,
      this.modelname,
      this.stock,
      this.bgcolor,
      this.toggleButton,
      this.onend,
      this.value,
      this.onchanged,
      this.left,
      this.right,
      this.togglecolor,
      this.icon,
      this.left2,
      this.right2,
      this.togglecolor2,
      this.icon2,
      this.textadmin, this.onhover, this.onhover2})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Card(
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: Row(children: [
              InkWell(
                  child: Stack(
                    
                    children: [
                    Container(
                      width: size.width * 0.28,
                      decoration: BoxDecoration(
                          color: AppColors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(15.0),
                          child: image),
                      height: size.height * 0.15,
                    ),
                    Container(
                        height: 20,
                        padding: const EdgeInsets.symmetric(
                            vertical: 2, horizontal: 10),
                        alignment: Alignment.centerRight,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10)),
                          color: AppColors.white10,
                        ),
                        child: stock),
                         Positioned(bottom: 5,left: 20,
                           child: CustomContainer(padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                            borderRadius:  BorderRadius.circular(4),
                        gradient: const LinearGradient(colors: [AppColors.dark,Colors.transparent,],begin: Alignment.topLeft,end: Alignment.bottomRight),
                            child: customText(color: AppColors.white,weight: FontWeight.bold,
                              text: modelname,
                            ),),
                         ) 
                  ]),
                  onTap: ontap),
              SizedBox(width: size.width * 0.07),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Row(children: [
                  Column(children: [
                    customText(text: "Stock", weight: FontWeight.bold),
                    SizedBox(
                      height: size.height * 0.004,
                    ),
                    Togglebtn(
                      onhover: onhover,
                      left: left,
                      right: right,
                      togglecolor: togglecolor,
                      icon: icon,
                      toggleButton: stocktoggleButton,
                    ),
                  ]),
                  SizedBox(
                    width: size.width * 0.12,
                  ),
                  Column(children: [
                    customText(text: "Enable", weight: FontWeight.bold),
                    SizedBox(
                      height: size.height * 0.004,
                    ),
                    Togglebtn(
                      onhover: onhover2,
                      left: left2,
                      right: right2,
                      togglecolor: togglecolor2,
                      icon: icon2,
                      toggleButton: toggleButton,
                    ),
                    // togglebtn
                  ]),
                ]),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: size.width * 0.038,
                    ),
                    customText(text: "Status", weight: FontWeight.bold),
                    SizedBox(
                      width: size.width * 0.14,
                    ),
                    ActiveBar(
                      bgcolor: bgcolor,
                      text: txtactive,
                      onTap: activeontap,
                    ),
                  ],
                ),
                SizedBox(
                  height: size.height * 0.01,
                ),
                customText(text: textadmin, color: AppColors.red),
              ]),
            ])));
  }
}
