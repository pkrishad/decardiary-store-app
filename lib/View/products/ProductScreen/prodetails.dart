import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/products/ProProvider/proprovider.dart';
import 'package:storeadmin_app/View/products/productClass/productclass.dart';
import 'package:storeadmin_app/custom_widgets/activebar.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_button.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';

class ProductDetails extends StatefulWidget {
 Datum productdetails;
  ProductDetails({
    Key? key,
    required this.productdetails,
  }) : super(key: key);

  @override
  State<ProductDetails> createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  bool togglevalue = false;

  @override
  Widget build(BuildContext context) {
    late int data;
    checkConnection(context);
    Size size = MediaQuery.of(context).size;
    ProviderProduct prodlist =
        Provider.of<ProviderProduct>(context, listen: true);
    return Consumer<ProviderProduct>(builder: (context, provider, child) {
      return SmartRefresher(
          controller: refreshController,
          onRefresh: () async {
            final result = await provider.getProducts(isRefresh: true);
            if (result) {
              refreshController.refreshCompleted();
            } else {
              refreshController.refreshFailed();
            }
          },
          onLoading: () async {
            final result = await prodlist.getProducts();
            if (result) {
              refreshController.loadComplete();
            } else {
              refreshController.loadFailed();
            }
          },
          child: Scaffold(
              backgroundColor: AppColors.white,
              appBar: AppBar(
                elevation: 0,
                backgroundColor: AppColors.white,
                leading: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                    provider.getProducts(isRefresh: true, perpage: 'all');
                  },
                  icon: const Icon(
                    Icons.arrow_back_ios_outlined,
                    color: AppColors.black,
                    size: 20.0,
                  ),
                ),
                title: customText(
                    text: widget.productdetails.name, color: AppColors.black),
              ),
              body: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(children: [
                    CustomContainer(
                      color: AppColors.white,
                      height: size.height * 0.45,
                      borderRadius: BorderRadius.circular(20),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Image.network(
                            widget.productdetails.image,
                            fit: BoxFit.cover,
                          )),
                    ),
                    SizedBox(
                      height: size.height * 0.01,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Column(
                        children: [
                          SizedBox(
                            height: size.height * 0.02,
                          ),
                          CustomContainer(
                            color: AppColors.white,
                            height: size.height * 0.07,
                            borderRadius: BorderRadius.circular(10),
                            shadow: [
                              BoxShadow(
                                  offset: const Offset(0, 1),
                                  blurRadius: 5,
                                  color: AppColors.black.withOpacity(0.2))
                            ],
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 25),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  customText(text: "Status", textSize: 25.0),
                                  ActiveBar(
                                      bgcolor: widget.productdetails.status == 0
                                          ? Colors.green.shade100
                                          : Colors.red.shade700,
                                      text: customText(
                                          text: widget.productdetails.status == 0
                                              ? "Active"
                                              : "Inactive"),
                                      onTap: () {}),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: size.height * 0.03,
                          ),
                          CustomContainer(
                            color: AppColors.white,
                            height: size.height * 0.07,
                            borderRadius: BorderRadius.circular(10),
                            shadow: [
                              BoxShadow(
                                  offset: const Offset(0, 1),
                                  blurRadius: 5,
                                  color: AppColors.black.withOpacity(0.2))
                            ],
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 25),
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    customText(text: "Stock", textSize: 25.0),
                                    CustomElevatedButton(
                                        fgColor: AppColors.black,
                                        bgColor: widget.productdetails.stockStatus == 0
                                            ? Colors.green.shade100
                                            : Colors.red.shade700,
                                        text: customText(
                                            text:
                                                widget.productdetails.stockStatus == 0
                                                    ? "In Stock"
                                                    : "Outof Stock"),
                                        onpress: () {
                                          togglevalue = !togglevalue;
                                          if (!togglevalue) {
                                            data = 0;
                                          } else if (togglevalue) {
                                            data = 1;
                                          }
                                          provider.updateStock(
                                            productid: widget.productdetails.id,
                                            status: widget.productdetails.status,
                                            stock: data,
                                          );
                                          Future.delayed(
                                              const Duration(milliseconds: 200),
                                              () {
                                            refreshController.isRefresh;
                                            refreshController.requestRefresh(
                                                needCallback: true);
                                            refreshController
                                                .refreshCompleted();
                                          });
                                        }),
                                  ]),
                            ),
                          ),
                          SizedBox(
                            height: size.height * 0.03,
                          ),
                          CustomContainer(
                            color: AppColors.white,
                            height: size.height * 0.07,
                            borderRadius: BorderRadius.circular(10),
                            shadow: [
                              BoxShadow(
                                  offset: const Offset(0, 1),
                                  blurRadius: 5,
                                  color: AppColors.black.withOpacity(0.2))
                            ],
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 25),
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    customText(text: "Actions", textSize: 25.0),
                                    CustomElevatedButton(
                                        fgColor: AppColors.black,
                                        bgColor: widget.productdetails.status == 0
                                            ? Colors.green.shade100
                                            : Colors.red.shade700,
                                        text: customText(
                                            text: widget.productdetails.status == 0
                                                ? "Enabled"
                                                : "Disabled"),
                                        onpress: () {
                                         
                                            togglevalue = !togglevalue;
                                      
                                          if (!togglevalue) {
                                            data = 0;
                                          } else if (togglevalue) {
                                            data = 1;
                                          }
                                          provider.updateStatus(
                                            productid: widget.productdetails.id,
                                            status: data,
                                            stock: widget.productdetails.stockStatus,
                                          );
                                          Future.delayed(
                                              const Duration(milliseconds: 200),
                                              () {
                                            refreshController.isRefresh;
                                            refreshController.requestRefresh(
                                                needCallback: true);
                                            refreshController
                                                .refreshCompleted();
                                          });
                                        }),
                                  ]),
                            ),
                          )
                        ],
                      ),
                    ),
                  ]))));
    });
  }
}
