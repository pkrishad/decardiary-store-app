// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

Widget CustomProgress({val,bgcolor,strokeB, color}) {
  return CircularProgressIndicator(
    color: color,
    value: val,
    backgroundColor: bgcolor,
    strokeWidth: strokeB,

  );
}
