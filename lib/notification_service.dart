// ignore_for_file: unused_local_variable, avoid_print

import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

Future<void> onBackgroundMessage(RemoteMessage message) async {
  await Firebase.initializeApp();
  await FirebaseMessaging.instance.subscribeToTopic('sub');
  if (message.data.containsKey('to')) {
    final tot = message.data['to'];
  }
  if (message.data.containsKey('data')) {
    // Handle data message
    final data = message.data['data'];
  }

  if (message.data.containsKey('notification')) {
    // Handle notification message
    final notification = message.data['notification'];
  }
  // Or do other work.
}

class FCM {
  final _firebaseMessaging = FirebaseMessaging.instance;

  final streamCtlr = StreamController<String>.broadcast();
  final titleCtlr = StreamController<String>.broadcast();
  final bodyCtlr = StreamController<String>.broadcast();
  final imageCtlr = StreamController<String>.broadcast();
  final clickCtlr = StreamController<String>.broadcast();
  final smallIcon = StreamController<String>.broadcast();

  setNotifications() {
    FirebaseMessaging.onBackgroundMessage(onBackgroundMessage);
    FirebaseMessaging.onMessage.listen(
      (message) async {
        if (message.data.containsKey('to')) {
          // handle topic message
          streamCtlr.sink.add(message.data['to']);
        }
        if (message.data.containsKey('data')) {
          // Handle data message
          streamCtlr.sink.add(message.data['data']);
        }
        if (message.data.containsKey('notification')) {
          // Handle notification message

          streamCtlr.sink.add(message.data['notification']);
        }

        // Or do other work.
        titleCtlr.sink.add(message.notification!.title!);
        bodyCtlr.sink.add(message.notification!.body!);
        imageCtlr.sink.add(message.notification!.android!.imageUrl!);
        clickCtlr.sink.add(message.notification!.android!.clickAction!);
        smallIcon.sink.add(message.notification!.android!.smallIcon!);
        // to.sink.add(message.notification.titleLocArgs);
      },
    );
    // With this token you can test it easily on your phone
    final token =
        _firebaseMessaging.getToken().then((value) => print('Token: $value'));
  }

  dispose() {
    streamCtlr.close();
    bodyCtlr.close();
    titleCtlr.close();
  }
}
