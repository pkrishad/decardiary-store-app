// ignore_for_file: non_constant_identifier_names, unused_local_variable

import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

import 'couponclass.dart';

class ProviderCoupon with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;

  ProviderCoupon({
    required this.repo,
    required this.dioClient,
  });
  CouponList? couponList;
  List<Datum>? data;
  late int totalPages;
  int current_page = 1;
  Datum? keylist;
  Data? datas;
  late List<Datum> couponlist = [];
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  Future<bool> allCoupons(
      {bool isRefresh = false, String? keystatus, String? status}) async {
          if (isRefresh) {
      current_page = 1;
    } else {
      if (current_page >= totalPages) {
        refreshController.loadNoData();
        return false;
      }
    }
    ApiResponse res = await repo.getdata(
      StringConst.COUPONS_LIST +
          "?page=$current_page&perpage=10&keystatus=$keystatus&status=$status"
    );
  
    if (kDebugMode) {
      print(res.response!.statusCode);
    }
    // ?page=1&perpage=16&keystatus=all&status=all
    if (res.response != null && res.response!.statusCode == 200) {
      final data = CouponList.fromJson(res.response?.data);
      final result = data.data;
      // keylist = result.data as Datum?;
      // final pagedata = result.data;
      // print(result.data.first.keystatus ==0);
     
      if (isRefresh) {
        couponlist = result.data;
      } else {
        couponlist.addAll(result.data);
      }

      current_page++;
      totalPages = result.lastPage;
      notifyListeners();
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  String _text = "";
  String _activestatus = "";
  String? _value;
  String? _value2;

  onchangedCs({String? cscontroller, String? cs, String? val}) async {
    cscontroller = val;
    cs = cscontroller.toString();
    if (cscontroller.toString() == "All") {
      cs = "all";
    }
    if (cscontroller.toString() == "Used") {
      cs = "0";
    }
    if (cscontroller.toString() == "UnUsed") {
      cs = "1";
    }
    final search = await ProviderCoupon(
      dioClient: dioClient,
      repo: repo,
    ).allCoupons(isRefresh: true, keystatus: cs);
    if (search) {
      refreshController.refreshCompleted();
    } else {
      refreshController.refreshFailed();
    }
    notifyListeners();
    // });
  }

  ontap({String? cs}) async {
    final search = await ProviderCoupon(
      dioClient: dioClient,
      repo: repo,
    ).allCoupons(isRefresh: true, keystatus: cs);
    if (search) {
      refreshController.refreshCompleted();
    } else {
      refreshController.refreshFailed();
    }
    notifyListeners();
  }

  void onchangedSt(String? stcontroller) {
    stcontroller = _value2;
    notifyListeners();
  }

  // Future<void> checkkeystatus() async {
  //   if (data?.first.keystatus == 1) {
  //    _text = "used";
  //     notifyListeners();
  //   } else {
  //     _text = "UnUsed";
  //     notifyListeners();
  //   }
  // }

  Future<ApiResponse?> checkstatus() async {
    if (data?.first.status == 0) {
      _activestatus = "Active";
      notifyListeners();
    } else {
      _activestatus = "Inactive";
      notifyListeners();
    }
  }
}
