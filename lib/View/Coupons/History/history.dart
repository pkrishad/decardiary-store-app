import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Coupons/History/p_history.dart';
import 'package:storeadmin_app/custom_widgets/activebar.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_dropdwnfield.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/paidbar.dart';

class History extends StatefulWidget {
  const History({Key? key}) : super(key: key);
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  final paymentstatus = ["All", "Paid", "Unpaid"];
  final status = ["All", "Active", "Inactive"];

  String? pscontroller;
  String? stcontroller;
  String? ps;
  String? st;
  bool isLoading = false;
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);
  @override
  Widget build(BuildContext context) {
       checkConnection(context);
    Size size = MediaQuery.of(context).size;
    ProviderHistory hist = Provider.of<ProviderHistory>(context, listen: false);
    return Consumer<ProviderHistory>(builder: (context, provider, child) {
      return Scaffold(backgroundColor: AppColors.white,
          body: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(children: [
                SizedBox(
                  width: double.infinity,
                  height: size.height * 0.065,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                      Expanded(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              
                              Container(
                                decoration: BoxDecoration(
                                    color: AppColors.white,
                                    // border: Border.all(
                                    //     color: AppColors.hint2, width: 1.0),
                                    borderRadius: BorderRadius.circular(10)),
                                alignment: Alignment.center,
                                width: 160,
                                height: 55,
                                child: CustomDDF(
                                  controller: pscontroller,
                                  labeltext: "Payment Status",
                                  list: paymentstatus.map(droplist1).toList(),
                                  onchanged: (value) => setState(() async {
                                    pscontroller = value;
                                    // stcontroller = value;
                                    ps = pscontroller.toString();
                                    if (pscontroller.toString() == "All") {
                                      ps = "all";
                                    }
                                    if (pscontroller.toString() == "Paid") {
                                      ps = "0";
                                    }
                                    if (pscontroller.toString() == "Unpaid") {
                                      ps = "1";
                                    }
                                    final search = await hist.historys(
                                        isRefresh: true, psstatus: ps);
                                    if (search) {
                                      refreshController.refreshCompleted();
                                    } else {
                                      refreshController.refreshFailed();
                                    }
                                  }),
                                  onTap: () async {
                                    final search = await hist.historys(
                                        isRefresh: true, psstatus: ps);
                                    if (search) {
                                      refreshController.refreshCompleted();
                                    } else {
                                      refreshController.refreshFailed();
                                    }
                                  },
                                ),
                              ),
                              const SizedBox(width: 15),
                              Container(
                                decoration: BoxDecoration(
                                    color: AppColors.white,
                                    // border: Border.all(
                                    //     color: AppColors.hint2, width: 1.0),
                                    borderRadius: BorderRadius.circular(10)),
                                alignment: Alignment.center,
                                width: 120,
                                height: 55,
                                child: CustomDDF(
                                    controller: stcontroller,
                                    labeltext: "Status",
                                    list: status.map(droplist2).toList(),
                                    onchanged: (value) => setState(() async {
                                          stcontroller = value;
                                          // stcontroller = value;
                                          st = stcontroller.toString();
                                          if (stcontroller.toString() ==
                                              "All") {
                                            st = "all";
                                          }
                                          if (stcontroller.toString() ==
                                              "Active") {
                                            st = "0";
                                          }
                                          if (stcontroller.toString() ==
                                              "Inactive") {
                                            st = "1";
                                          }
                                          final search = await hist.historys(
                                              isRefresh: true, status: st);
                                          if (search) {
                                            refreshController
                                                .refreshCompleted();
                                          } else {
                                            refreshController.refreshFailed();
                                          }
                                        }),
                                    onTap: () async {
                                      final search = await hist.historys(
                                          isRefresh: true, status: st);
                                      if (search) {
                                        refreshController.refreshCompleted();
                                      } else {
                                        refreshController.refreshFailed();
                                      }
                                    }),
                              ),
                              const SizedBox(width: 30),
                            ],
                          ),
                        ),
                      ),
                    ]),
                  ),
                ),
                Expanded(
                    child: SmartRefresher(
                        controller: refreshController,
                        enablePullUp: true,
                        footer: CustomFooter(
                          builder: (BuildContext context, LoadStatus? mode) {
                            Widget child;
                            if (mode == LoadStatus.idle) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "Swipe up to load more",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.loading) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const CupertinoActivityIndicator(),
                                    const SizedBox(width: 5,),
                                    customText(
                                        text: "Loading..",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.failed) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Icon(
                                      Icons.error_outlined,
                                      color: AppColors.hint2,
                                      
                                    ),
                                    const SizedBox(width: 5,),
                                    customText(
                                        text: "No more Data",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.canLoading) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "Swipe up to load more",
                                        color: AppColors.hint2)
                                  ]);
   
                            } else {
                              child =Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "No more Data",
                                        color: AppColors.hint2)
                                  ]); 
                            }
                            return child;
                          },
                        ),
                        onRefresh: () async {
                          final result = await hist.historys(isRefresh: true);
                          if (result) {
                            refreshController.refreshCompleted();
                          } else {
                            refreshController.refreshFailed();
                          }
                        },
                        onLoading: () async {
                          final result = await hist.historys();
                          if (result) {
                            refreshController.loadComplete();
                          } else {
                            refreshController.loadFailed();
                          }
                        },
                        child: isLoading
                            ? const Center(
                                child: const CircularProgressIndicator())
                            : provider.hist.length != 0
                                ? ListView.separated(
                                    itemCount: provider.hist.length,
                                    itemBuilder: (BuildContext context, index) {
                                      final history = provider.hist[index];
                                      return Card(elevation: 2,
                                          color: AppColors.white,
                                          shape: RoundedRectangleBorder(

                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 30,
                                                      horizontal: 17),
                                              child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: [
                                                    Column(
                                                      children: [
                                                        customText(
                                                            text:
                                                                "${history.totalkeys}",
                                                            textSize: 20.0),
                                                        SizedBox(
                                                          height: size.height *
                                                              0.006,
                                                        ),
                                                        CircleAvatar(
                                                            radius: 20.0,
                                                            backgroundColor:
                                                                AppColors
                                                                    .white10,
                                                            child: const Icon(
                                                              Icons
                                                                  .confirmation_num_outlined,
                                                              size: 30.0,
                                                              color: AppColors
                                                                  .blue,
                                                            )),
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      width: size.width * 0.05,
                                                    ),
                                                    Column(
                                                      children: [
                                                        customText(
                                                            text:
                                                                "₹${history.keyamount}",
                                                            textSize: 20.0),
                                                        const ActiveBar(
                                                          bgcolor: AppColors.white,
                                                          text: const Text("Amount"),
                                                        )
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      width: size.width * 0.03,
                                                    ),
                                                    Column(
                                                      children: [
                                                        customText(
                                                            text:
                                                                "₹${history.totalamount}",
                                                            textSize: 20.0),
                                                        const ActiveBar(
                                                          bgcolor: AppColors.white,
                                                          text: Text("Total"),
                                                        )
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      width: size.width * 0.03,
                                                    ),
                                                    Column(
                                                      children: [
                                                        customText(
                                                            text: "Status",
                                                            weight: FontWeight
                                                                .bold),
                                                        SizedBox(
                                                          height: size.height *
                                                              0.004,
                                                        ),
                                                        ActiveBar(
                                                          bgcolor: history.paymentStatus==0?Colors.lightBlue.shade100:Colors.red.shade700,
                                                          text: customText(text: history.paymentStatus==0?"Paid":"Unpaid"),
                                                          onTap: () {},
                                                          //  bgcolor: Colors.greenAccent.shade100,
                                                          //     fgcolor: AppColors.black,
                                                        )
                                                      ],
                                                    ),
                                                  ])));
                                    },
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return const SizedBox(height: 10);
                                    },
                                  )
                                : Center(
                                    child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Icon(
                                        Icons.search_off_rounded,
                                        color: AppColors.hint,
                                        size: 50.0,
                                      ),
                                      customText(text: "No History.")
                                    ],
                                  ))))
              ])));
    });
  }


  DropdownMenuItem<String> droplist1(String paymentstatus) => DropdownMenuItem(
        value: paymentstatus,
        child: Text(paymentstatus),
      );

  DropdownMenuItem<String> droplist2(String status) => DropdownMenuItem(
        value: status,
        child: Text(status),
      );
}
