// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String? hint;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final hintText;
  final lbltxt;
  final textcolor;
  // final onSaved;

  final font;
  final hintColor;

  final FloatingLabelBehavior? floatbehavior;

  final labelColor;
  // final FormFieldValidator? validator;

  const CustomTextField({Key? key, this.hint, this.controller,this.focusNode, this.hintText, this.lbltxt, this.textcolor, this.font, this.hintColor, this.floatbehavior, this.labelColor,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
    TextField(

    autofocus: false,
    focusNode: focusNode,
    // validator: validator,
    style: TextStyle(color: textcolor, fontFamily: font),
    controller: controller,
    decoration: InputDecoration(
            hintText: hintText,
            // hintStyle: GoogleFonts.arya(color: hintColor, fontSize: 12),
            floatingLabelBehavior: floatbehavior,
            labelText: lbltxt,
            labelStyle: TextStyle(color: labelColor),
            contentPadding:
            const EdgeInsets.symmetric(horizontal: 22, vertical: 10),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              gapPadding: 10,
            )));
  }
}

