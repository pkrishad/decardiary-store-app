// ignore_for_file: non_constant_identifier_names

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Coupons/All%20Coupons/coupons.dart';
import 'package:storeadmin_app/View/Coupons/History/history.dart';
import 'package:storeadmin_app/custom_widgets/custom_appbar.dart';
import 'package:storeadmin_app/custom_widgets/custom_slidecont.dart';

class Keys extends StatefulWidget {
  const Keys({Key? key}) : super(key: key);

  @override
  _KeysState createState() => _KeysState();
}

class _KeysState extends State<Keys> {
  int? groupValue3 = 0;
  List<Widget> Pages3 = [
    const Coupons(),
    const History(),
  ];
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    return Scaffold(backgroundColor: AppColors.white,
      appBar: customAppBar(
        fntcolor: AppColors.black,
        textSize: 20.0,
        bottom: CupertinoSlidingSegmentedControl<int>(
            backgroundColor: AppColors.white10,
            padding: EdgeInsets.symmetric(horizontal: padding.horizontal*0,vertical:  padding.vertical*0,),
            groupValue: groupValue3,
            children: {
              0: buildSegment('All Coupons'),
              1: buildSegment('History'),
            },
            onValueChanged: (groupValue3) {
              setState(() {
                this.groupValue3 = groupValue3;
              });
            }),
      ),
      body: Pages3[groupValue3!],
    );
  }
}
