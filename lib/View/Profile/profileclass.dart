class Profile {
  Profile({
    required this.status,
    required this.message,
    required this.data,
  });
  late final String status;
  late final String message;
  late final Data data;

  Profile.fromJson(Map<String, dynamic> json){
    status = json['status'];
    message = json['message'];
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['status'] = status;
    _data['message'] = message;
    _data['data'] = data.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.id,
    required this.storeid,
    required this.storename,
    required this.storeimage,
    required this.addressLine_1,
    required this.addressLine_2,
    required this.city,
    required this.district,
    required this.pincode,
    required this.contactnumber,
    required this.alternatenumber,
    required this.idcardtype,
    required this.idproofimg,
    required this.username,
  });


  late final int id;
  late final String storeid;
  late final String storename;
  late final String storeimage;
  late final String addressLine_1;
  late final String addressLine_2;
  late final String city;
  late final String district;
  late final String pincode;
  late final String contactnumber;
  late final String alternatenumber;
  late final String idcardtype;
  late final String idproofimg;
  late final String username;


  Data.fromJson(Map<String, dynamic> json){
    id = json['id'];
    storeid = json['storeid'];
    storename = json['storename'];
    storeimage = json['storeimage'];
    addressLine_1 = json['address_line_1'];
    addressLine_2 = json['address_line_2'];
    city = json['city'];
    district = json['district'];
    pincode = json['pincode'];
    contactnumber = json['contactnumber'];
    alternatenumber = json['alternatenumber'];
    idcardtype = json['idcardtype'];
    idproofimg = json['idproofimg'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['storeid'] = storeid;
    _data['storename'] = storename;
    _data['storeimage'] = storeimage;
    _data['address_line_1'] = addressLine_1;
    _data['address_line_2'] = addressLine_2;
    _data['city'] = city;
    _data['district'] = district;
    _data['pincode'] = pincode;
    _data['contactnumber'] = contactnumber;
    _data['alternatenumber'] = alternatenumber;
    _data['idcardtype'] = idcardtype;
    _data['idproofimg'] = idproofimg;
    _data['username'] = username;
    return _data;
  }
}