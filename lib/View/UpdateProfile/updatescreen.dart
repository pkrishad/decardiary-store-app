// ignore_for_file: unrelated_type_equality_checks

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/UpdateProfile/p_update.dart';
import 'package:storeadmin_app/View/UpdateProfile/p_updatepwd.dart';
import 'package:storeadmin_app/View/UpdateProfile/password.dart';
import 'package:storeadmin_app/View/UpdateProfile/username.dart';
import 'package:storeadmin_app/custom_widgets/custom_button.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textfield.dart';
import 'package:storeadmin_app/dio/api_response.dart';

class UpdateScreen extends StatefulWidget {
  const UpdateScreen({Key? key}) : super(key: key);

  @override
  _UpdateScreenState createState() => _UpdateScreenState();
}

class _UpdateScreenState extends State<UpdateScreen> {
  final updateKey = GlobalKey<FormState>();
  final TextEditingController _newusername = TextEditingController();
  final TextEditingController _currentpassword = TextEditingController();
  final TextEditingController _cpwd = TextEditingController();
  final TextEditingController _pnewpassword = TextEditingController();
  final TextEditingController _confirmpassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    UpdateProfile user = Provider.of<UpdateProfile>(context, listen: false);
    UpdatePwd pwd = Provider.of<UpdatePwd>(context, listen: false);

    return Scaffold(
        appBar: AppBar(
          title: customText(text: "Update Profile",color: AppColors.dark,weight: FontWeight.bold),
          centerTitle: true,
          elevation: 0,
          backgroundColor: AppColors.white10,
          leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(
                Icons.arrow_back_rounded,
                color: AppColors.black,
              )),
        ),
        body: Padding(
            padding: const EdgeInsets.all(20),
            child: SingleChildScrollView(
              child: Column(
            
                children: [
                Username(),
                Password(),
              ],),
            )));
  }
}
