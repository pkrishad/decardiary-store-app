import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Notifications/smsclass.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';

class SmsInner extends StatelessWidget {
  final Datum data;
  const SmsInner({Key? key, required this.data}) : super(key: key);

 
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: AppBar(
        backgroundColor: AppColors.dark,
      ),
      body: Padding(padding:EdgeInsets.all(20),child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustomContainer(borderRadius: BorderRadius.circular(20),
          shadow: [
            BoxShadow(
                                                    offset: const Offset(0, 3),
                                                    blurRadius: 5,
                                                    color: AppColors.dark
                                                        .withOpacity(0.2)),
            
          ],
            color: AppColors.white,
            width: double.infinity,
            height: size.height*0.8,
            child:Padding(padding: EdgeInsets.all(20),child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(mainAxisAlignment: MainAxisAlignment.end,children: [
   customText(
                                                                text:
                                                                    "${data.createdAt.toString().substring(0, 10)}",
                                                                textSize: 15.0,
                                                                color: AppColors
                                                                    .hint),
],),
SizedBox(height: size.height*0.02),
              customText(text:"${data.subject.toString().substring(8)}",color: AppColors.green.withOpacity(0.7),weight: FontWeight.bold,textSize: 30.0 ),


SizedBox(height: size.height*0.03),
customText(
                                                                text:
                                                                    "${data.description.toString().substring(12)}",
                                                                textSize: 15.0,
                                                                color: AppColors
                                                                    .hint),
                                                                    Divider(),
                                                                    SizedBox(height: size.height*0.03),

   customText(
                                                                  text:
                                                                      "Model",color: AppColors.hint,textSize: 20.0),
                                                                      SizedBox(height: size.height*0.02),
                                                                  data.model == null ? customText(text: ""): CustomContainer(
                                                              child:  customText(
                                                                  text:
                                                                      "${data.model}"),
                                                              color:
                                                                  AppColors.blue,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10),
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(10),
                                                            ),                                                                 
SizedBox(height: size.height*0.05),

 data.details.map((e) => e.product).join( " , ").isEmpty ? customText(text: ""): customText(
                                                                  text:
                                                                      "Product Details",color: AppColors.hint,textSize: 20.0),
                                                                      SizedBox(height: size.height*0.02),
                                                                   data.details.map((e) => e.product).join( " , ").isEmpty ? customText(text: ""): CustomContainer(
                                                              child: customText(
                                                                  text:data.details.map((e) => e.product).join( " , ").isEmpty ? "":
                                                                      '${data.details.map((e) => e.product).join( " , ")}'),
                                                              color:
                                                                  AppColors.blue,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10),
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(10),
                                                            ),

            ],),
            ),

          )
        ],
      ),
    ));
  }
}
