// To parse this JSON data, do
//
//     final mhistory = mhistoryFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

Mhistory mhistoryFromJson(String str) => Mhistory.fromJson(json.decode(str));

String mhistoryToJson(Mhistory data) => json.encode(data.toJson());

class Mhistory {
    Mhistory({
        required this.status,
        required this.data,
        required this.c,
        required this.message,
    });

    final String status;
    final Data data;
    final int c;
    final String message;

    factory Mhistory.fromJson(Map<String, dynamic> json) => Mhistory(
        status: json["status"],
        data: Data.fromJson(json["data"]),
        c: json["c"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
        "c": c,
        "message": message,
    };
}

class Data {
    Data({
        required this.currentPage,
        required this.data,
        // required this.from,
        required this.lastPage,
        // required this.nextPageUrl,
        // required this.path,
        // required this.perPage,
        // required this.prevPageUrl,
        // required this.to,
        required this.total,
    });

    final int currentPage;
    final List<Datum> data;
    // final int from;
    final int lastPage;
    // final dynamic nextPageUrl;
    // final String path;
    // final int perPage;
    // final dynamic prevPageUrl;
    // final int to;
    final int total;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        currentPage: json["current_page"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        // from: json["from"],
        lastPage: json["last_page"],
        // nextPageUrl: json["next_page_url"],
        // path: json["path"],
        // perPage: json["per_page"],
        // prevPageUrl: json["prev_page_url"],
        // to: json["to"],
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        // "from": from,
        "last_page": lastPage,
        // "next_page_url": nextPageUrl,
        // "path": path,
        // "per_page": perPage,
        // "prev_page_url": prevPageUrl,
        // "to": to,
        "total": total,
    };
}

class Datum {
    Datum({
        required this.id,
        required this.totalkeys,
        required this.keyamount,
        required this.totalamount,
        required this.paymentStatus,
        required this.status,
        required this.createdAt,
    });

    final int id;
    final int totalkeys;
    final String keyamount;
    final String totalamount;
    final int paymentStatus;
    final int status;
    final DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        totalkeys: json["totalkeys"],
        keyamount: json["keyamount"],
        totalamount: json["totalamount"],
        paymentStatus: json["payment_status"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "totalkeys": totalkeys,
        "keyamount": keyamount,
        "totalamount": totalamount,
        "payment_status": paymentStatus,
        "status": status,
        "created_at": createdAt.toIso8601String(),
    };
}
