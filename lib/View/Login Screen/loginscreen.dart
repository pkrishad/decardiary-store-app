// ignore_for_file: avoid_print

import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iconsax/iconsax.dart';
import 'package:progress_state_button/iconed_button.dart';
import 'package:progress_state_button/progress_button.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/Version%20Check/providerversion.dart';
import 'package:storeadmin_app/Version%20Check/versionclass.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_circlebutton.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textfield.dart';
import 'package:storeadmin_app/custom_widgets/dialog.dart';
import 'package:storeadmin_app/notification_service.dart';
import 'package:url_launcher/url_launcher.dart';
import 'login_int.dart';

class LoginScreen extends StatefulWidget {
  final SharedPreferences? sharedPreferences;
  final Version? version;

  const LoginScreen({
    Key? key,
    this.sharedPreferences,
    this.version,
  }) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();
  ButtonState stateOnlyText = ButtonState.idle;
  ButtonState stateTextWithIcon = ButtonState.idle;

  @override
  Widget build(BuildContext context) {
    LoginUser auth = Provider.of<LoginUser>(context);
    Size size = MediaQuery.of(context).size;
    checkConnection(context);
    return Scaffold(
        body: Consumer<ProviderVersion>(builder: (context, provider, child) {
      return Form(
          key: _formKey,
          child: Stack(children: [
            // Container(
            //     height: size.height,
            //     child: Image.asset(
            //       "assets/images/login.jpg",
            //       fit: BoxFit.fill,
            //     )),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: SingleChildScrollView(
                physics: const NeverScrollableScrollPhysics(),
                child: Column(children: [
                  const SizedBox(
                    height: 130,
                  ),
                  SizedBox(
                    height: 40,
                    child: Row(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          child: customText(
                              text: "LOGIN",
                              textSize: 36.0,
                              color: AppColors.primaryColor,
                              weight: FontWeight.bold),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const SizedBox(
                              height: 12,
                            ),
                            Container(
                              width: size.width - 145,
                              height: 10,
                              color: AppColors.hint2.withOpacity(0.3),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Container(
                    width: size.width,
                    height: 500,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: AppColors.white),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              const Icon(
                                Iconsax.user,
                                size: 30,
                              ),
                              SizedBox(
                                width: size.width * 0.70,
                                height: size.height * 0.06,
                                child: CustomTextFormField(
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return "UserName can't be empty";
                                    } else if (value.length < 5) {
                                      return "Invalid UserName";
                                    }
                                    return null;
                                  },
                                  controller: _username,
                                  labelColor: AppColors.hint2,
                                  hintText: "User Name",
                                  hintColor: Colors.grey,
                                ),
                              ),
                            ]),
                        SizedBox(
                          height: size.height * 0.03,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              const Icon(
                                Iconsax.password_check,
                                size: 30,
                              ),
                              SizedBox(
                                // width: size.width * 0.66,
                                width: size.width * 0.70,
                                height: size.height * 0.06,
                                child: CustomTextFormField(
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return "Password can't be empty";
                                    } else if (value.length < 5) {
                                      return "Invalid Password";
                                    }
                                    return null;
                                  },
                                  controller: _password,
                                  hintText: "Password",
                                  hintColor: Colors.grey,
                                  labelColor: AppColors.hint2,
                                ),
                              ),
                            ]),
                        SizedBox(
                          height: size.height * 0.03,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            RichText(
                              text: TextSpan(
                                style: GoogleFonts.poppins(
                                    fontSize: 12.0,
                                    color: AppColors.primaryColor),
                                text: "FORGOT PASSWORD",
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () async {
                                    var whatsappUrl =
                                        "whatsapp://send?phone=${provider.version!.supportNumber}&text=Forgot password of my store. Please help with it.";
                                    await canLaunch(whatsappUrl)
                                        ? launch(whatsappUrl)
                                        : print(
                                            "open whatsapp app link or do a snackbar with notification that there is no whatsapp installed");
                                  },
                              ),
                            ),
                            SizedBox(
                              width: size.width * 0.06,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: size.height * 0.03,
                        ),
                        const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: const Divider(),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              RichText(
                                  text: TextSpan(children: [
                                TextSpan(
                                    style: TextStyle(
                                        fontSize: 13.0,
                                        color:
                                            AppColors.black.withOpacity(0.6)),
                                    text:
                                        "By logging in,you are agreeing to decARdiary's"),
                              ])),
                            ]),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                  style: const TextStyle(
                                      fontSize: 13.0,
                                      color: AppColors.primaryColor),
                                  text: "Terms and Conditions",
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () async {
                                      var url = "${provider.version?.terms}";
                                      if (await canLaunch(url)) {
                                        await launch(url);
                                      } else {
                                        print('Could not launch $url');
                                      }
                                    }),
                              const TextSpan(
                                  style: TextStyle(
                                      fontSize: 9.0, color: AppColors.black),
                                  text: " and "),
                              TextSpan(
                                  style: const TextStyle(
                                      fontSize: 13.0,
                                      color: AppColors.primaryColor),
                                  text: "Privacy Policy",
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () async {
                                      var url = "${provider.version?.privacy}";
                                      if (await canLaunch(url)) {
                                        await launch(url);
                                      } else {
                                        print('Could not launch $url');
                                      }
                                    }),
                            ])),
                          ],
                        ),
                        SizedBox(
                          height: size.height * 0.03,
                        ),
                       
                               ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              auth.loginUser(context, _username, _password);
                            } else {
                              if (kDebugMode) {
                                print("form is invalid");
                              }
                            }
                          },
                          child: customText(text: "Login"),
                          style: ElevatedButton.styleFrom(fixedSize: Size(size.width - 50,50),
                              primary: AppColors.primaryColor,
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                              textStyle: GoogleFonts.poppins(fontSize: 20)),
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 60,
                  ),
                ]),
              ),
            ),
          ]));
    }));
  }

// successfulMessage.then((response) {
//   if (response['status']) {
//     User user = response['data'];
//     Provider.of<UserProvider>(context, listen: false).setUser(user);
//     Navigator.pushReplacementNamed(context, '/dashboard');
//   } else {
//     Alerts.showError("Login Failed ");
//   }
// });

  
}
