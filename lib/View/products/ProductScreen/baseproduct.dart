// ignore_for_file: non_constant_identifier_names, unnecessary_import

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/products/ProductScreen/pcategories.dart';
import 'package:storeadmin_app/View/products/ProductScreen/products.dart';
import 'package:storeadmin_app/custom_widgets/custom_appbar.dart';
import 'package:storeadmin_app/custom_widgets/custom_slidecont.dart';

class BaseProducts extends StatefulWidget {
  const BaseProducts({Key? key}) : super(key: key);

  @override
  _BaseProductsState createState() => _BaseProductsState();
}

class _BaseProductsState extends State<BaseProducts> {
  int? groupValue = 0;
  List<Widget> Pages = [
    const PCategory(),
    const Products(),
  ];

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    return Scaffold(backgroundColor: AppColors.white,
      appBar: customAppBar(
        fntcolor: AppColors.black,
        textSize: 20.0,
        bottom: CupertinoSlidingSegmentedControl<int>(
          backgroundColor: AppColors.white10,
            padding: EdgeInsets.symmetric(horizontal: padding.horizontal*0,vertical:  padding.vertical*0,),
            groupValue: groupValue,
            children: {
              0: buildSegment('Category'),
              1: buildSegment('Products'),
            },
            onValueChanged: (groupValue) {
              setState(() {
                this.groupValue = groupValue;
              });
            }),
      ),
      body: Pages[groupValue!],
    );
  }

}
