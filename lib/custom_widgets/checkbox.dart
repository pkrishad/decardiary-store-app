import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
class Check extends StatefulWidget {
  const Check({Key? key}) : super(key: key);

  @override
  _CheckState createState() => _CheckState();
}

class _CheckState extends State<Check> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Checkbox(
      value: isChecked,
      onChanged: (bool? value) {
        // This is where we update the state when the checkbox is tapped
        setState(() {
          isChecked = value!;
        });
      },
      fillColor: MaterialStateProperty.all(
          AppColors.primaryColor),
    );
  }
}
