import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Models/ModelClass/listclass.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class ProviderModellist with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;
  late int totalPages;
  int current_page = 1;
  late List<Datum> modellist = [];
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  ProviderModellist({required this.repo, required this.dioClient});
  getModellist(
      {
      bool isRefresh = false,
      String perpage = "16",
      String sort = "",
      String status = "",
      String action = "",
      String search = '',
      String category = ''}) async{
    if(isRefresh) {
    current_page = 1;
    }else{
      if(current_page >= totalPages) {
        refreshController.loadNoData();
        return false;
      }
    }
    ApiResponse? res = await repo.getdata(StringConst.MODELLIST +
        "?dataorder=$sort&status=$status&isdisabled=$action&category_id=$category&search=$search&page=$current_page&perpage=$perpage");

    final resdata = (res.response?.data);
    if (res.response != null && res.response?.statusCode == 200) {
      var models = ModellistClass.fromJson(res.response!.data);
      var result = models.data;
      if (isRefresh) {
        modellist = result.data;
      } else {
        modellist.addAll(result.data);
      }
      current_page++;
      totalPages = result.lastPage;
      notifyListeners();
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  // String get text => _text;

  void updateStatus({required int modelid, required int status}) async {
    var mapData = {
      "models": [
        json.encode({"model_id": modelid, "status": status})
      ].toString()
    };
    ApiResponse? res = await repo.postData(
      StringConst.UPDATE_MODEL_LIST,
      data: mapData,
    );
    // modellist[index].status = 1;
    notifyListeners();
  }

  void updateBulkUpload(int status) async {
    print(status);
    var m = modellist
        .map((e) => json.encode({'model_id': e.id, 'status': status}))
        .toList();
    var m2 = {'models': m.toString()};
    print(m2);
    ApiResponse? res =
        await repo.postData(StringConst.UPDATE_MODEL_LIST, data: m2);
  }
}
