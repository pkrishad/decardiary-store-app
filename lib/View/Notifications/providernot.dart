// ignore_for_file: non_constant_identifier_names, prefer_typing_uninitialized_variables

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Notifications/smsclass.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class ProviderNot with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;
  ProviderNot({
    required this.repo,
    required this.dioClient,
  });
  late int totalPages;
  int current_page = 1;
  late List<Datum> notification = [];
  // late List<Detail> details = [];
  late Map detail;
  var result;
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);
  bool value = false;
  Future<bool> getNotification({
    bool isRefresh = false,
  }) async {
    ApiResponse res =
        await repo.getdata(StringConst.NOTIFICATIONS + "?page=$current_page");
    // if (kDebugMode) {
    //   print(res.response!.statusCode);
    // }
    if (isRefresh) {
      current_page = 0;
    } else {
      if (current_page >= totalPages) {
        refreshController.loadNoData();
        return false;
      }
    }
    final resdata = (res.response?.data);
    if (res.response != null && res.response!.statusCode == 200) {
      final data = SmsClass.fromJson(res.response!.data);
      final result = data.data;
      if (isRefresh) {
        notification = result.data;
      } else {
        notification.addAll(result.data);
      }
      current_page++;
      totalPages = result.lastPage;
      notifyListeners();
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  void deletesms(int smsid) async {
    print(smsid);
    ApiResponse? reso =
        await repo.getdata(StringConst.DELETE_NOTIFICATIONS + "/${smsid}");
    notifyListeners();
  }
}
