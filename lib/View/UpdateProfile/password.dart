import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/UpdateProfile/p_updatepwd.dart';
import 'package:storeadmin_app/custom_widgets/custom_button.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textfield.dart';
import 'package:storeadmin_app/dio/api_response.dart';
class Password extends StatefulWidget {
  const Password({ Key? key }) : super(key: key);

  @override
  _PasswordState createState() => _PasswordState();
}

class _PasswordState extends State<Password> {
   final updateKey2 = GlobalKey<FormState>();
    final TextEditingController _cpwd = TextEditingController();
  final TextEditingController _pnewpassword = TextEditingController();
  final TextEditingController _confirmpassword = TextEditingController();
  @override
  Widget build(BuildContext context) {
       UpdatePwd pwd = Provider.of<UpdatePwd>(context, listen: false);
    return Form(key:updateKey2 ,child: 
    Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
      const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Icon(Icons.lock_outline_rounded),
                        const SizedBox(
                          width: 10,
                        ),
                        customText(
                            text: "Create new password",
                            weight: FontWeight.bold,
                            textSize: 25.0),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    customText(
                        text:
                            "Your new password must be different \n from previous used password",
                        color: AppColors.hint),
                    const SizedBox(
                      height: 20,
                    ),
                    CustomTextFormField(
                      hintColor: AppColors.hint,
                      controller: _cpwd,
                      hintText: "Enter current password",
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Password can't be empty";
                        } else if (value.length < 5) {
                          return "Invalid Password";
                        }
                        return null;
                      },
                    ),
                    const Divider(
                      color: AppColors.white,
                    ),
                    CustomTextFormField(
                        hintColor: AppColors.hint,
                        controller: _pnewpassword,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Password can't be empty";
                          } else if (value.length < 5) {
                           return "Invalid Password";
                          }
                          return null;
                        },
                        hintText: "Enter new password"),
                    const SizedBox(
                      height: 20,
                    ),
                    CustomTextFormField(
                        hintColor: AppColors.hint,
                        controller: _confirmpassword,
                        validator: (value) {
                          if (value!.isEmpty) {
                           return "Password can't be empty";
                          } else if (value.length < 5) {
                            return "Invalid Password";
                          } else if (_pnewpassword.text != _confirmpassword) {
                            return "different Password";
                          }
                          return null;
                        },
                        hintText: "Confirm new password"),
                    const SizedBox(
                      height: 20,
                    ),
                    // _confirmpassword

                    CustomElevatedButton(
                        text:customText(text:"Update Password") ,
                        onpress: () {
                          if (updateKey2.currentState!.validate()) {
                            final Future<ApiResponse?> successfulMessage =
                                pwd.updatepwd(
                                    context: context,
                                    currentpwd: _cpwd,
                                    newpassword: _pnewpassword,
                                    cnfmpassword: _confirmpassword);
                            successfulMessage.then((response) {
                              _cpwd.clear();
                              _pnewpassword.clear();
                              _confirmpassword.clear();
                            });
                          } else {
                            if (kDebugMode) {
                              print("form is invalid");
                            }
                          }
                        },
                        bgColor: AppColors.dark,
                        fgColor: AppColors.white)
    ],)
      
    );
  }
}