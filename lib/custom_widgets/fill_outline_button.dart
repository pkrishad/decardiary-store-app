import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

class FillButton extends StatelessWidget {
  final bool isFilled;
  final VoidCallback press;
  final String? text;
  final Color? fillcolor;
  const FillButton(
      {Key? key, required this.isFilled, required this.press, this.text, this.fillcolor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(padding: const EdgeInsets.all(6.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
        side: BorderSide(color: AppColors.dark.withOpacity(0.3)),
      ),
      elevation: isFilled ? 2 : 0,
      color: isFilled ? fillcolor : Colors.transparent,
      onPressed: press,
      child: Text(
        text!,
        style: TextStyle(
          color: isFilled ? AppColors.dark : const Color(0xFFDF6E8A),
          fontSize: 15,
        ),
      ),
    );
  }
}
