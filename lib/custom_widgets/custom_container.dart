// ignore_for_file: prefer_typing_uninitialized_variables, use_key_in_widget_constructors

import 'package:flutter/material.dart';

class CustomContainer extends StatelessWidget {
  final double? width;
  final double? height;
  final BorderRadiusGeometry? borderRadius;
  final List<BoxShadow>? shadow;
  final BoxBorder? border;
  final Widget? child;
  final EdgeInsetsGeometry? padding;
  final Color? color;
  final Gradient? gradient;
  final Alignment? align;
  final EdgeInsetsGeometry? margin;
  final image;
  const CustomContainer(
      {this.width,
      this.height,
      this.borderRadius,
      this.shadow,
      this.border,
      this.child,
      this.padding,
      this.color,
      this.gradient,
      this.align,
      this.margin, this.image});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      alignment: align,
      padding: padding,
      width: width,
      height: height,
      decoration: BoxDecoration(
          image:  image,
          gradient: gradient,
          color: color,
          borderRadius: borderRadius,
          boxShadow: shadow,
          border: border),
      child: child,
    );
  }
}
