import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Users/usersclass.dart';
import 'package:storeadmin_app/custom_widgets/dialog.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class ProviderUser with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;
  UsersData? users;
  Data? data;
  Datum? datum;
  String _text = "";
   late int totalPages;
  int current_page = 1;
   late List<Datum> userdata = [];
   final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  ProviderUser({required this.repo, required this.dioClient});

  getUser({bool isRefresh = false,String? sort}) async {
        if (isRefresh) {
      current_page = 1;
    } else {
      if (current_page >= totalPages) {
        refreshController.loadNoData();
        return false;
      }
    }
    // Dialogs.showLoading(context: context);
    ApiResponse? res = await repo.getdata(
        StringConst.USERS + "?page=$current_page&perpage=10&dataorder=$sort");
    
 

    final resdata = (res.response?.data);
    if (resdata["status"] == "success" && res.response?.statusCode == 200) {  
        final history = UsersData.fromJson(res.response!.data);
      final result = history.data;
     
      if (isRefresh) {
        userdata = result.data;
      } else {
        userdata.addAll(result.data);
      }
      current_page++;
      totalPages = result.lastPage;
      notifyListeners();
      notifyListeners();
      return true;
    } else {
      return false;
    }
    
  }

  String? _value;
  onchangedCs(String cscontroller) {
    cscontroller = _value as String;
    notifyListeners();
  }
   String? get value => _value;
   String get text => _text;
}
