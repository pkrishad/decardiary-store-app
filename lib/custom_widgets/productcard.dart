



// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/custom_widgets/activebar.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';

class ProductCard extends StatelessWidget {
  final ontap;
  final txtactive;
  final activeontap;
  final togglebtn;
  final image;
  final modelname;
  const ProductCard(
      {Key? key,
      this.ontap,
      this.txtactive,
      this.activeontap,
      this.togglebtn,
      this.image, this.modelname})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Card(
        child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: Row(children: [
              InkWell(
                  child: Container(
                    width: size.width * 0.28,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(15),image: DecorationImage(image:  NetworkImage(image) )  ),
                    height: size.height * 0.15,
                  ),
                  onTap: ontap
                  // () async {
                  //   await Navigator.push(
                  //       context,
                  //       MaterialPageRoute(
                  //           builder: (context) =>
                  //               ModelDetails()));
                  // },
                  ),
              SizedBox(width: size.width * 0.07),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Row(children: [
                  Column(
                    children: [
                      customText(text: "Status", weight: FontWeight.bold),
                      SizedBox(
                        height: size.height * 0.004,
                      ),
                      ActiveBar(
                        text: txtactive,
                        onTap: activeontap,
                      ),
                    ],
                  ),
                  SizedBox(
                    width: size.width * 0.12,
                  ),
                  Column(children: [
                    customText(text: "Enable", weight: FontWeight.bold),
                    SizedBox(
                      height: size.height * 0.004,
                    ),
                    togglebtn
                  ]),
                ]),
                const SizedBox(
                  height: 20,
                ),
                CustomContainer(
                  borderRadius:
                      const BorderRadius.only(bottomRight: Radius.circular(15)),
                  width: size.width * 0.5,
                  height: size.height * 0.05,
                  color: Colors.white,
                  child: Center(
                    child: customText(text: modelname),
                  ),
                  shadow: [
                    BoxShadow(
                        offset: const Offset(0, 1),
                        blurRadius: 10,
                        color: AppColors.black.withOpacity(0.1))
                  ],
                )
              ]),
            ])));
  }
}