import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

class CircleButton extends StatelessWidget {
  final Widget? child;
  final void Function()? onpressed;
  const CircleButton({Key? key, this.child, this.onpressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child:child,
      // Icon(Icons.arrow_forward_ios_outlined),
      onPressed: onpressed,
      style: ButtonStyle(
          shadowColor: MaterialStateProperty.all(Colors.white),
          shape: MaterialStateProperty.all(const CircleBorder()),
          backgroundColor:
          MaterialStateProperty.all(AppColors.primaryColor)),
    );
  }
}
