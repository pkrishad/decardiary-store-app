// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

class Bulk extends StatelessWidget {
  // ignore: prefer_typing_uninitialized_variables
  final color;
  // ignore: prefer_typing_uninitialized_variables
  final ontap;
  // ignore: prefer_typing_uninitialized_variables
  final icon;
  const Bulk({Key? key, this.color, this.ontap, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
        onTap: ontap,
        //  () {
        //   setState(() {
        //     user.isFollowedByMe = !user.isFollowedByMe;
        //   });
        // },
        child: AnimatedContainer(
            height: size.height * 0.06,
            width: size.width * 0.12,
            duration: Duration(milliseconds: 0),
            decoration: BoxDecoration(
                boxShadow:  const [
                  BoxShadow(
                      blurRadius: 5.0, spreadRadius: 0.2, color: AppColors.hint)
                ],
                color: color,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: AppColors.dark, width: 0.5)),
            child: icon));

  }
}
