import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

class SearchWidget extends StatefulWidget {
  final String text;
  final ValueChanged<String> onchanged;
  final String hinttext;
  const SearchWidget(
      {Key? key,
      required this.text,
      required this.onchanged,
      required this.hinttext})
      : super(key: key);

  @override
  _SearchWidgetState createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  final controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    const styleActive = TextStyle(color: AppColors.black);
    const styleHint = TextStyle(color: Colors.black54);
    final style = widget.text.isEmpty ? styleHint : styleActive;
    return TextField(
        controller: controller,
        decoration: InputDecoration(
            icon: const Icon(
              Icons.search_outlined,
              color: AppColors.hint,
            ),
            suffixIcon: widget.text.isEmpty
                ? GestureDetector(
                    child: const Icon(
                      Icons.close,
                      color: AppColors.dark,
                    ),
                    onTap: () {
                      controller.clear();
                      widget.onchanged('');
                      FocusScope.of(context).requestFocus(FocusNode(canRequestFocus: true));
                    },
                  )
                : null
                ,hintText: widget.hinttext,
                hintStyle: style,
                border: InputBorder.none,

                ),style: style,
                onChanged: widget.onchanged,
                
                );
  }
}
