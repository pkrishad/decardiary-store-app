import 'package:flutter/material.dart';

 Widget buildSegment(String text) {
    return Container(
      padding: const EdgeInsets.all(12),
      child: Text(
        text,
        style: const TextStyle(fontSize: 20.0,),
      ),
    );
  }