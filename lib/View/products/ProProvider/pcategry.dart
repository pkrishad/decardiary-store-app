// ignore_for_file: unnecessary_import, unused_field, non_constant_identifier_names, prefer_typing_uninitialized_variables, avoid_print, unused_local_variable

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/products/productClass/categryclass.dart';
import 'package:storeadmin_app/custom_widgets/dialog.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class ProviderCProduct with ChangeNotifier {
  bool isloading = false;
  final Repo repo;
  final DioClient dioClient;
  late int totalPages;
  final String _text = "";
  int current_page = 1;
  late List<Datum> prodata = [];
  var result;
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);
  bool value = false;

  ProviderCProduct({required this.repo, required this.dioClient});

  void getCProducts({GetCProductsReq? param, bool? enabled}) async {
    //FocusScope.of(context).requestFocus(FocusNode());
    enabled = true;
    ApiResponse? res =
        await repo.getdata(StringConst.PRODUCT_CATEGORIES);

    if (res.response != null && res.response?.statusCode == 200) {
      final models = CProductClass.fromJson(res.response!.data);
      result = models.data;
      if (models.data.currentPage == models.data.lastPage ||
          models.data.lastPage == 0) {
        param!.page = -1;
      } else {
        param!.page = models.data.currentPage + 1;
      }

      if (models.data.currentPage == 1) {
        prodata = models.data.data;
      } else {
        prodata.addAll(models.data.data);
      }
      enabled = false;
      notifyListeners();
    }
  }

    // Future<ApiResponse> getCProducts(
    //   BuildContext context, {
    //   bool isRefresh = false,
    //   bool? isloading,
    //   String sort = '',
    //   String status = '',
    //   String action = '',
    //   String search = '',
    //   String perpage = '10',
    // }) async {
    //   if (isRefresh) {
    //     current_page = 1;
    //   } else {
    //     if (current_page >= totalPages) {
    //       refreshController.loadNoData();
    //       return false;
    //     }
    //   }

    //   ApiResponse? res = await repo.getdata(StringConst.PRODUCT_CATEGORIES +
    //       "?page=$current_page&perpage=$perpage&dataorder=$sort&status=$status&isdisabled=$action&search=$search");

    //   if (res.response != null && res.response?.statusCode == 200) {
    //     final models = CProductClass.fromJson(res.response!.data);
    //     result = models.data;
    //     if (isRefresh) {
    //       prodata = result.data;
    //     } else {
    //       prodata.addAll(result.data);
    //     }
    //     current_page++;
    //     totalPages = result.lastPage;
    //     notifyListeners();
    //     notifyListeners();
    //     return true;
    //   } else{

    //     return false;
    //   }

    // }

    void updateStatus({
      // required int index,
      required int categoryid,
      required int status,
    }) async {
      var mapData = {
        "product_categories": [
          json.encode({"category_id": categoryid, "status": status})
        ].toString()
      };
      ApiResponse? reso = await repo
          .postData(StringConst.UPDATE_PRODUCT_CATEGORIES, data: mapData);
      // prodata[index].status = 1;
      notifyListeners();
    }

    //
    // void values(value) async {
    //   value = !value;
    //   notifyListeners();
    // }
    void updateBulkUpload(
      int status,
    ) async {
      print(status);
      var m = prodata
          .map((e) => json.encode({'category_id': e.id, 'status': status}))
          .toList();
      var m2 = {'product_categories': m.toString()};
      print(m2);
      ApiResponse? res =
          await repo.postData(StringConst.UPDATE_PRODUCT_CATEGORIES, data: m2);
      // final search = await repo.getdata(StringConst.PRODUCT_CATEGORIES);
    }
  }

