// ignore_for_file: constant_identifier_names

part of values;

class StringConst {
  static const String BASE_URL = "https://sappapi.decardiary.com/api";
  static const String TOKEN = "token";

  static const String API_VERSION = BASE_URL + "/version_check";
  static const String LOGIN = BASE_URL + "/login";
  static const String DASHBOARD = BASE_URL + "/dashboard";
  static const String PROFILE = BASE_URL + "/profile";
  static const String MODELCATEGORYS = BASE_URL + "/model_categories";
  static const String MODELLIST = BASE_URL + "/models";
  static const String PRODUCT_CATEGORIES = BASE_URL + "/product_categories";
  static const String UPDATE_MODEL_CATEGORIES = BASE_URL + "/update_model_categories";
  static const String UPDATE_MODEL_LIST = BASE_URL + "/update_models";
  static const String UPDATE_PRODUCT_CATEGORIES = BASE_URL + "/update_product_categories";
  static const String UPDATE_PRODUCTS = BASE_URL + "/update_products";
  static const String UPDATE_PRODUCTLIST = BASE_URL + "/update_products";
  static const String PRODUCT_LIST = BASE_URL + "/products";
  static const String COUPONS_LIST = BASE_URL + "/coupons";
  static const String COUPON_HISTORY = BASE_URL + "/coupons_history";
  static const String USERS = BASE_URL + "/users";
  static const String UPDATE_USERNAME = BASE_URL + "/update_username";
  static const String UPDATE_PASSWORD = BASE_URL + "/update_password";
  static const String NOTIFICATIONS = BASE_URL + "/notifications";
  static const String DELETE_NOTIFICATIONS = BASE_URL + "/delete_notifications";
  static const String LOGOUT = BASE_URL + "/logout";
}

// update_products
// https://sappapi.decardiary.com/api/product_categories?page=all&perpage=all&dataorder=desc&status=all&isdisabled=all&search=
// https://sappapi.decardiary.com/api/products?page=1&perpage=16&dataorder=desc&status=all&isdisabled=all&category_id=all&stock=all&search=
// /models?page=1&perpage=16&dataorder=desc&status=all&isdisabled=all&category_id=all&search='
//https://sappapi.decardiary.com/api/model_categories?page=1&perpage=all&dataorder=desc&status=all&isdisabled=all&search=
// https://sappapi.decardiary.com/api/notifications?page=1
// https://sappapi.decardiary.com/api/update_username
// https://sappapi.decardiary.com/api/coupons?page=1&perpage=16&keystatus=all&status=all
// https://sappapi.decardiary.com/api/coupons_history?page=1&perpage=16&payment_status=all&status=all
// https://sappapi.decardiary.com/api/users?page=1&perpage=6&dataorder=asc