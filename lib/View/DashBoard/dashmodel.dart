class DashModel {
  DashModel({
    required this.status,
    required this.topBanners,
    required this.bottomBanners,
    required this.users,
    required this.coupons,
    required this.products,
    required this.models,
    required this.message,
  });
  late final String status;
  late final List<TopBanners> topBanners;
  late final List<BottomBanners> bottomBanners;
  late final int users;
  late final Coupons coupons;
  late final Products products;
  late final Models models;
  late final String message;
  
  DashModel.fromJson(Map<String, dynamic> json){
    status = json['status'];
    topBanners = List.from(json['top_banners']).map((e)=>TopBanners.fromJson(e)).toList();
    bottomBanners = List.from(json['bottom_banners']).map((e)=>BottomBanners.fromJson(e)).toList();
    users = json['users'];
    coupons = Coupons.fromJson(json['coupons']);
    products = Products.fromJson(json['products']);
    models = Models.fromJson(json['models']);
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['status'] = status;
    _data['top_banners'] = topBanners.map((e)=>e.toJson()).toList();
    _data['bottom_banners'] = bottomBanners.map((e)=>e.toJson()).toList();
    _data['users'] = users;
    _data['coupons'] = coupons.toJson();
    _data['products'] = products.toJson();
    _data['models'] = models.toJson();
    _data['message'] = message;
    return _data;
  }
}

class TopBanners {
  TopBanners({
    required this.bannerId,
    required this.bannerType,
    required this.image,
    required this.image2,
    required this.subtitle,
    required this.title,
    required this.description,
    required this.link,
    required this.categoryId,
    required this.itemId,
  });
  late final int bannerId;
  late final int bannerType;
  late final String image;
  late final String image2;
  late final String subtitle;
  late final String title;
  late final String description;
  late final String link;
  late final int categoryId;
  late final int itemId;
  
  TopBanners.fromJson(Map<String, dynamic> json){
    bannerId = json['banner_id'];
    bannerType = json['banner_type'];
    image = json['image'];
    image2 = json['image2'];
    subtitle = json['subtitle'];
    title = json['title'];
    description = json['description'];
    link = json['link'];
    categoryId = json['category_id'];
    itemId = json['item_id'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['banner_id'] = bannerId;
    _data['banner_type'] = bannerType;
    _data['image'] = image;
    _data['image2'] = image2;
    _data['subtitle'] = subtitle;
    _data['title'] = title;
    _data['description'] = description;
    _data['link'] = link;
    _data['category_id'] = categoryId;
    _data['item_id'] = itemId;
    return _data;
  }
}

class BottomBanners {
  BottomBanners({
    required this.bannerId,
    required this.bannerType,
    required this.image,
    required this.image2,
    required this.subtitle,
    required this.title,
    required this.description,
    required this.link,
    required this.categoryId,
    required this.itemId,
  });
  late final int bannerId;
  late final int bannerType;
  late final String image;
  late final String image2;
  late final String subtitle;
  late final String title;
  late final String description;
  late final String link;
  late final int categoryId;
  late final int itemId;
  
  BottomBanners.fromJson(Map<String, dynamic> json){
    bannerId = json['banner_id'];
    bannerType = json['banner_type'];
    image = json['image'];
    image2 = json['image2'];
    subtitle = json['subtitle'];
    title = json['title'];
    description = json['description'];
    link = json['link'];
    categoryId = json['category_id'];
    itemId = json['item_id'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['banner_id'] = bannerId;
    _data['banner_type'] = bannerType;
    _data['image'] = image;
    _data['image2'] = image2;
    _data['subtitle'] = subtitle;
    _data['title'] = title;
    _data['description'] = description;
    _data['link'] = link;
    _data['category_id'] = categoryId;
    _data['item_id'] = itemId;
    return _data;
  }
}

class Coupons {
  Coupons({
    required this.total,
    required this.used,
  });
  late final int total;
  late final int used;
  
  Coupons.fromJson(Map<String, dynamic> json){
    total = json['total'];
    used = json['used'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['total'] = total;
    _data['used'] = used;
    return _data;
  }
}

class Products {
  Products({
    required this.total,
    required this.activated,
  });
  late final int total;
  late final int activated;
  
  Products.fromJson(Map<String, dynamic> json){
    total = json['total'];
    activated = json['activated'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['total'] = total;
    _data['activated'] = activated;
    return _data;
  }
}

class Models {
  Models({
    required this.total,
    required this.activated,
  });
  late final int total;
  late final int activated;
  
  Models.fromJson(Map<String, dynamic> json){
    total = json['total'];
    activated = json['activated'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['total'] = total;
    _data['activated'] = activated;
    return _data;
  }
}