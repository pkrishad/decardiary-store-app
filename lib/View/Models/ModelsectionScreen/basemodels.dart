// ignore_for_file: non_constant_identifier_names

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Models/ModelsectionScreen/mcategories.dart';
import 'package:storeadmin_app/View/Models/ModelsectionScreen/models.dart';
import 'package:storeadmin_app/custom_widgets/custom_appbar.dart';
import 'package:storeadmin_app/custom_widgets/custom_slidecont.dart';

class BaseModels extends StatefulWidget {
  const BaseModels({Key? key}) : super(key: key);

  @override
  _BaseModelsState createState() => _BaseModelsState();
}
class _BaseModelsState extends State<BaseModels> {
  int? groupValue2 = 0;
  List<Widget> Pages2 = [
    const MCategory(),
    const Models(),
  ];
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    return Scaffold(backgroundColor: AppColors.white,
      appBar: customAppBar(
        fntcolor: AppColors.black,
        textSize: 20.0,
        bottom: CupertinoSlidingSegmentedControl<int>(
            backgroundColor: AppColors.white10,
            padding: EdgeInsets.symmetric(horizontal: padding.horizontal*0,vertical:  padding.vertical*0,),
            groupValue: groupValue2,
            children: {
              0: buildSegment('Category'),
              1: buildSegment('Models'),
            },
            onValueChanged: (groupValue2) {
              setState(() {
                this.groupValue2 = groupValue2;
              });
            }),
      ),
      body: Pages2[groupValue2!],
    );
  }
}
