// ignore_for_file: unnecessary_import, avoid_print

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:storeadmin_app/Logout/providerlogout.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/Version%20Check/providerversion.dart';
import 'package:storeadmin_app/View/Profile/prodetails.dart';
import 'package:storeadmin_app/View/Profile/providerprofile.dart';
import 'package:storeadmin_app/View/UpdateProfile/updatescreen.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_button.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_profilecontainers.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:url_launcher/url_launcher.dart';

import '../Notifications/notification.dart';

class Profile extends StatefulWidget {
  final String? title;
  const Profile({Key? key, this.title}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
    buildSignature: 'Unknown',
  );

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  Widget _infoTile(String subtitle) {
    return Text("Ver${subtitle.isEmpty ? 'Not set' : subtitle}");
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      Provider.of<ProviderProfile>(context, listen: false).userProfile(context);
    });
    checkConnection(context);

    Size size = MediaQuery.of(context).size;
     ProviderVersion version = Provider.of<ProviderVersion>(context);
    return Consumer<ProviderProfile>(builder: (context, provider, child) {
      var user = provider.data;
      return SafeArea(
          child: Scaffold(
              appBar: AppBar(
                leading: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(
                    Icons.arrow_back_ios_outlined,
                    color: AppColors.black,
                    size: 20.0,
                  ),
                ),
                backgroundColor: AppColors.white,
                toolbarHeight: 80,
                elevation: 0,
                title: Padding(
                  padding: const EdgeInsets.all(6),
                  child: Container(
                    width: 200,
                    height: 50,
                    decoration: BoxDecoration(
                        color: AppColors.white,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                              offset: const Offset(0, 1),
                              blurRadius: 5,
                              color: AppColors.black.withOpacity(0.23))
                        ]),
                    child: Center(
                        child: customText(
                            text: "Settings",
                            textSize: 22.0,
                            color: AppColors.dark,
                           )),
                  ),
                ),
                centerTitle: true,
              ),
              backgroundColor: AppColors.white,
              body: Padding(
                  padding: const EdgeInsets.all(20),
                  child: SingleChildScrollView(
                    child: Column(children: [user?.storeimage == null? const Icon(Icons.storefront_rounded,size: 100,color: AppColors.dark,):
                  InkWell(onTap: (){Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) => const ProDetails()));},
                                  child: CircleAvatar(backgroundColor: AppColors.white10,
                                    backgroundImage: NetworkImage(user!.storeimage),
                                    radius: 70,foregroundColor: AppColors.dark,
                                  ),),
                                  CustomContainer(borderRadius: BorderRadius.circular(10),
                                    shadow: const [BoxShadow(
                                      color:AppColors.blue,
                                    blurRadius:0.1 ,spreadRadius:0.01 ,
                  
                                  offset: Offset(0,2) 
                                    )],
                                    color: AppColors.white,
                            child:Padding(padding: const EdgeInsets.all(20),child:
                     Column(children: [
                  user?.storename == null 
                   ? Text("#STORE",
                                            style: TextStyles.white_20_700())
                                        : user!.storename.isEmpty
                                            ? Text("no data",
                                                style: TextStyles.white_20_700())
                                            : customText(
                                                text: user.storename,
                                                textSize: 25.0,
                                                weight: FontWeight.bold),
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          const Icon(
                                            Icons.location_on_outlined,
                                            color: AppColors.dark,
                                          ),
                                          user?.city == null
                                              ? Text("#STORE",
                                                  style:
                                                      TextStyles.white_20_700())
                                              : user!.city.isEmpty
                                                  ? Text("no data",
                                                      style: TextStyles
                                                          .white_20_700())
                                                  : customText(
                                                      text: user.city),
                                        ])
                                    ],)),
                                  
                                  ),
                          
                    SizedBox(height: size.height * 0.01),
                      ListBody(children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ProContainer(
                              text: 'Update Profile',
                              icon: Icons.update,
                              onTap: () {
                                 Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => const UpdateScreen()));
                              },
                            ),
                           SizedBox(height: size.height * 0.01),
                            ProContainer(
                              text: 'Notifications',
                              icon: Icons.notifications,
                              onTap: () {
                                 Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => const Msg()));
                              },
                            ),
                            SizedBox(height: size.height * 0.01),
                            ProContainer(
                              text: 'Terms & Condition',
                              icon: Icons.copy,
                              onTap: () async {
                                              var url =
                                                  "${version.version?.terms}";
                                              if (await canLaunch(url)) {
                                                await launch(url);
                                              } else {
                                                print('Could not launch $url');
                                              }
                             
                              },
                            ),
                            SizedBox(height: size.height * 0.01),
                            ProContainer(
                              text: 'Privacy & Policy',
                              icon: Icons.lock_rounded,
                              onTap: ()async {
                                              var url =
                                                  "${version.version?.privacy}";
                                              if (await canLaunch(url)) {
                                                await launch(url);
                                              } else {
                                                print('Could not launch $url');
                                              }
                              },
                            ),
                          SizedBox(height: size.height * 0.01),
                            ProContainer(
                              text: 'Help',
                              icon: Icons.help,
                              onTap: () async {
                                final ver = Provider.of<ProviderVersion>(context,
                                    listen: false);
                                var whatsappUrl =
                                    "whatsapp://send?phone=${ver.version?.supportNumber}&text=I would like to know more..";
                                await canLaunch(whatsappUrl)
                                    ? launch(whatsappUrl)
                                    : print(
                                        "open whatsapp app link or do a snackbar with notification that there is no whatsapp installed");
                              },
                            ),
                           SizedBox(height: size.height * 0.01),
                            ProContainer(
                              text: 'Log Out',
                              icon: Icons.logout,
                              onTap: () {
                                logOut();
                              },
                            ),
                            SizedBox(height: size.height * 0.02),
                            _infoTile(_packageInfo.version),
                          ],
                        )
                      ]),
                    ]),
                  ))));
    });
  }

  logOut() {
    return
        // isLoading = true;
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                actions: [
                  Column(children: [
                    const SizedBox(height: 20,),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                       CustomElevatedButton(onpress: (){
                                                       Navigator.pop(context,true);

                       },text: customText(text: "CANCEL"),bgColor: AppColors.blue,fgColor: AppColors.black,size: Size(90.0,30.0)),
                       CustomElevatedButton(onpress: (){
                         Provider.of<Logout>(context, listen: false)
                                  .userlogout(context);
                       },text: customText(text: "YES"),bgColor: AppColors.blue,fgColor: AppColors.black,size: Size(90.0,30.0))
                      ]),
                ]),
                ],
                content: const Text(
                  "Are you sure you want to Log out?",
                  style: TextStyle(fontFamily: 'Glory', fontSize: 20),
                ),
              );
            });
  }
}
