// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';

class CustListTile extends StatelessWidget {
  final IconData? icon;
  final text;
  final trailing;
  const CustListTile({Key? key, this.icon, this.text, this.trailing}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: customText(text: text,color: AppColors.hint),
      tileColor: AppColors.white10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
    );
  }
}
