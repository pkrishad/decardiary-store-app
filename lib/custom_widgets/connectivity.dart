import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:storeadmin_app/Values/values.dart';

import 'custom_text.dart';

  checkConnection(BuildContext context) async {
    var connection = await Connectivity().checkConnectivity();
    if (connection == ConnectivityResult.none) {
      return showSimpleNotification(Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
                backgroundColor: Colors.grey.shade100, radius: 14,
                child: const Icon(Icons.wifi_off_outlined, color: Colors.black,)),
            const SizedBox(width: 20,),
            customText(text:
            "No Internet Connection",
                color: AppColors.white
            ),
          ]),
          background: AppColors.dark
      );
    }
  }
