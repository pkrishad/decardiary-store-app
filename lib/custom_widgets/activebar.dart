import 'package:flutter/material.dart';

class ActiveBar extends StatelessWidget {
  // ignore: prefer_typing_uninitialized_variables
  final text;
  final void Function()? onTap;
  // ignore: prefer_typing_uninitialized_variables
  final bgcolor;
  const ActiveBar({Key? key, this.text, this.onTap, this.bgcolor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(alignment: Alignment.center,
    width: size.width*0.2,
      height: size.height*0.04,
      decoration: BoxDecoration(color: bgcolor,borderRadius: BorderRadius.circular(7)),child: text,);
  }
}
