// ignore_for_file: non_constant_identifier_names, unused_local_variable, avoid_print

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/products/productClass/productclass.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class ProviderProduct with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;
  late int totalPages;
  int current_page = 1;

  late List<Datum> proddata = [];
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  ProviderProduct({required this.repo, required this.dioClient});

  getProducts(
      {bool isRefresh = false,
      String sort = "",
      String status = "",
      String action = "",
      String stock = "",
      String search = '',
      String categoryid = "",
      String perpage = '16'}) async {
    if (isRefresh) {
      current_page = 1;
    } else {
      if (current_page >= totalPages) {
        refreshController.loadNoData();
        return false;
      }
    }

    ApiResponse? res = await repo.getdata(StringConst.PRODUCT_LIST +
        "?page=$current_page&perpage=$perpage&dataorder=$sort&status=$status&isdisabled=$action&category_id=$categoryid&stock=$stock&search=$search");

    final resdata = (res.response?.data);
    if (res.response != null && res.response?.statusCode == 200) {
      final models = ProductClass.fromJson(res.response!.data);
      final result = models.data;
      if (isRefresh) {
        proddata = result.data;
      } else {
        proddata.addAll(result.data);
      }
      current_page++;
      totalPages = result.lastPage;
      notifyListeners();
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  Future<void> getmdl(BuildContext context) async {
    ApiResponse res = await repo.getdata(StringConst.PRODUCT_LIST);
    if (res.response != null && res.response?.statusCode == 200) {
      final models = ProductClass.fromJson(res.response!.data);
    } else if (res.error != "401") {}
  }

  void updateStatus(
      {required int productid, required int status, required int stock,}) async {
    var mapData = {
      "products": [
        json.encode({"product_id": productid, "status": status, "stock": stock})
      ].toString()
    };
    print(mapData);
    ApiResponse? res = await repo.postData(
      StringConst.UPDATE_PRODUCTLIST,
      data: mapData,
    );
    // Future.delayed(Duration(seconds: 1), () async{
    //   ApiResponse res = await repo.getdata(StringConst.PRODUCT_LIST);
    // });
    // proddata[index!].status = 1;
    notifyListeners();
  }

  void updateStock(
      {required int productid, required int status, required int stock,}) async {
    var mapData = {
      "products": [
        json.encode({"product_id": productid, "status": status, "stock": stock})
      ].toString()
    };
    print(mapData);
    ApiResponse? res = await repo.postData(
      StringConst.UPDATE_PRODUCTLIST,
      data: mapData,
    );
    // proddata[index!].stockStatus = 0;
    notifyListeners();
  }

  void updateBulkUpload(int status) async {
    print(status);
    var m = proddata
        .map((e) => json.encode(
            {'product_id': e.id, 'status': status, 'stock': e.stockStatus}))
        .toList();
    var m2 = {'products': m.toString()};
    print(m2);
    ApiResponse? res =
        await repo.postData(StringConst.UPDATE_PRODUCTLIST, data: m2);
  }

  void updatestockUpload(int stock, {index}) async {
    print(stock);
    var m = proddata
        .map((e) => json
            .encode({'product_id': e.id, 'status': e.status, 'stock': stock}))
        .toList();
    var m2 = {'products': m.toString()};
    print(m2);
    ApiResponse? res =
        await repo.postData(StringConst.UPDATE_PRODUCTLIST, data: m2);
  }
}
