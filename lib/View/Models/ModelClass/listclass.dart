// To parse this JSON data, do
//
//     final modellistClass = modellistClassFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ModellistClass modellistClassFromJson(String str) => ModellistClass.fromJson(json.decode(str));

String modellistClassToJson(ModellistClass data) => json.encode(data.toJson());

class ModellistClass {
    ModellistClass({
        required this.status,
        required this.data,
        required this.message,
    });

    final String status;
    final Data data;
    final String message;

    factory ModellistClass.fromJson(Map<String, dynamic> json) => ModellistClass(
        status: json["status"],
        data: Data.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
        "message": message,
    };
}

class Data {
    Data({
        // required this.currentPage,
        required this.data,
        // required this.from,
        required this.lastPage,
        // required this.nextPageUrl,
        // required this.path,
        // required this.perPage,
        // @required this.prevPageUrl,
        // required this.to,
        // required this.total,
    });

    // final int currentPage;
    final List<Datum> data;
    // final int from;
    final int lastPage;
    // final String nextPageUrl;
    // final String path;
    // final int perPage;
    // final dynamic prevPageUrl;
    // final int to;
    // final int total;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        // currentPage: json["current_page"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        // from: json["from"],
        lastPage: json["last_page"],
        // nextPageUrl: json["next_page_url"],
        // path: json["path"],
        // perPage: json["per_page"],
        // prevPageUrl: json["prev_page_url"],
        // to: json["to"],
        // total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        // "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        // "from": from,
        "last_page": lastPage,
        // "next_page_url": nextPageUrl,
        // "path": path,
        // "per_page": perPage,
        // "prev_page_url": prevPageUrl,
        // "to": to,
        // "total": total,
    };
}

class Datum {
    Datum({
        required this.id,
        required this.name,
        required this.image,
        required this.categoryId,
        required this.viewAs,
        required this.globalStatus,
        required this.storeId,
        required this.modelId,
        required this.status,
    });

    final int id;
    final String name;
    final String image;
    final int categoryId;
    final int viewAs;
    final int globalStatus;
    final String storeId;
    final String modelId;
     int status;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        image: json["image"],
        categoryId: json["category_id"],
        viewAs: json["view_as"],
        globalStatus: json["global_status"],
        storeId: json["store_id"],
        modelId: json["model_id"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image": image,
        "category_id": categoryId,
        "view_as": viewAs,
        "global_status": globalStatus,
        "store_id": storeId,
        "model_id": modelId,
        "status": status,
    };
}
