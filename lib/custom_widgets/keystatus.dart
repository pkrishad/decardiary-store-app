// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';


class KeyStatus extends StatelessWidget {
  final void Function()? onTap;
  final text;
  const KeyStatus( {Key? key, this.onTap, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  ElevatedButton(
          onPressed: onTap,
          style: ButtonStyle(
              overlayColor: MaterialStateProperty.all(AppColors.white10),
              foregroundColor:
                  MaterialStateProperty.all<Color>(AppColors.black),
              backgroundColor:
                  MaterialStateProperty.all<Color>(AppColors.white),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6)))),
          child: Padding(
            padding: const EdgeInsets.all(4),
            child: Text(text),
          ));
 
  }
}


// CustomContainer(
//       width: size.width * 0.23,
//       height: size.height * 0.045,
//       color: Colors.white,
//       shadow: [
//         BoxShadow(
//             offset: const Offset(0, 1),
//             blurRadius: 2,
//             color: AppColors.black.withOpacity(0.2))
//       ],
//       borderRadius: BorderRadius.circular(4.0),
//       child: Center(
//         child: customText(text: "Used"),),
//     );
