// ignore_for_file: unrelated_type_equality_checks

import 'package:clipboard/clipboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Coupons/All%20Coupons/p_couponlist.dart';
import 'package:storeadmin_app/custom_widgets/activebar.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_dropdwnfield.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';

import '../../../custom_widgets/custom_dropdwnfield.dart';

class Coupons extends StatefulWidget {
  const Coupons({
    Key? key,
  }) : super(key: key);

  @override
  _CouponsState createState() => _CouponsState();
}

class _CouponsState extends State<Coupons> {
  String? cscontroller;
  String? stcontroller;
  String cs = "";
  String st = "";
  String text = "";
  final couponstatus = ["All", "Used", "Unused"];
  final sstatus = ["All", "Active", "Inactive"];
  bool isLoading = false;
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);
    

  @override
  Widget build(BuildContext context) {
       checkConnection(context);
    Size size = MediaQuery.of(context).size;
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      // Provider.of<ProviderCoupon>(context, listen: false).checkkeystatus();
      Provider.of<ProviderCoupon>(context, listen: false).checkstatus();
      // Provider.of<ProviderCoupon>(context, listen: false)
      //     .onchangedCs(cs);
      // Provider.of<ProviderCoupon>(context, listen: false)
      //     .onchangedSt(st);
    });
    // ProviderCoupon coup = Provider.of<ProviderCoupon>(context);
    ProviderCoupon coup = Provider.of<ProviderCoupon>(context, listen: false);

   

    return Consumer<ProviderCoupon>(builder: (context, provider, child) {
      return Scaffold(backgroundColor: AppColors.white,
          body: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(children: [
                Container(
                  width: double.infinity,
                  height: size.height * 0.065,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                      Expanded(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              // const SizedBox(width: ),
                              Container(
                                decoration: BoxDecoration(
                                    color: AppColors.white,
                                    // border: Border.all(
                                    //     color: AppColors.hint2, width: 1.0),
                                    borderRadius: BorderRadius.circular(10)),
                                alignment: Alignment.center,
                                width: 150,
                                height: 55,
                                child: CustomDDF(
                                  controller: cscontroller,
                                  labeltext: "Coupon Status",
                                  list: couponstatus.map(droplist1).toList(),
                                  onchanged: (value) => setState(() async {
                                    cscontroller = value;
                                    cs = cscontroller.toString();
                                    if (cscontroller.toString() == "All") {
                                      cs = "all";
                                    }
                                    if (cscontroller.toString() == "Used") {
                                      cs = "0";
                                    }
                                    if (cscontroller.toString() == "Unused") {
                                      cs = "1";
                                    }
                                    final search = await coup.allCoupons(
                                        isRefresh: true, keystatus: cs);
                                    if (search) {
                                      refreshController.refreshCompleted();
                                    } else {
                                      refreshController.refreshFailed();
                                    }
                                  }),
                                  onTap: () async {
                                    final search = await coup.allCoupons(
                                        isRefresh: true, keystatus: cs);
                                    if (search) {
                                      refreshController.refreshCompleted();
                                    } else {
                                      refreshController.refreshFailed();
                                    }
                                  },
                                ),
                              ),
                              const SizedBox(width: 15),
                              Container(
                                decoration: BoxDecoration(
                                    color: AppColors.white,
                                    // border: Border.all(
                                    //     color: AppColors.hint2, width: 1.0),
                                    borderRadius: BorderRadius.circular(10)),
                                alignment: Alignment.center,
                                width: 120,
                                height: 55,
                                child: CustomDDF(
                                  controller: stcontroller,
                                  labeltext: "Status",
                                  list: sstatus.map(droplist2).toList(),
                                  onchanged: (value) => setState(() async {
                                    stcontroller = value;
                                    st = stcontroller.toString();
                                    if (stcontroller.toString() == "All") {
                                      st = "all";
                                    }
                                    if (stcontroller.toString() == "Active") {
                                      st = "0";
                                    }
                                    if (stcontroller.toString() == "Inactive") {
                                      st = "1";
                                    }
                                    final search = await coup.allCoupons(
                                        isRefresh: true, status: st);
                                    if (search) {
                                      refreshController.refreshCompleted();
                                    } else {
                                      refreshController.refreshFailed();
                                    }
                                  }),
                                  onTap: () async {
                                    final search = await coup.allCoupons(
                                        isRefresh: true, status: st);
                                    if (search) {
                                      refreshController.refreshCompleted();
                                    } else {
                                      refreshController.refreshFailed();
                                    }
                                  },
                                ),
                              ),
                              const SizedBox(width: 30,),
                            ],
                          ),
                        ),
                      ),
                    ]),
                  ),
                ),
                Expanded(
                    child: SmartRefresher(
                        controller: refreshController,
                        enablePullUp: true,
                        footer: CustomFooter(
                          builder: (BuildContext context, LoadStatus? mode) {
                            Widget child;
                            if (mode == LoadStatus.idle) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "Swipe up to load more",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.loading) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const CupertinoActivityIndicator(),
                                    const SizedBox(width: 5,),
                                    customText(
                                        text: "Loading..",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.failed) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Icon(
                                      Icons.error_outlined,
                                      color: AppColors.hint2,
                                      
                                    ),
                                    const SizedBox(width: 5,),
                                    customText(
                                        text: "No more Data",
                                        color: AppColors.hint2)
                                  ]);
                            } else if (mode == LoadStatus.canLoading) {
                              child = Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "Swipe up to load more",
                                        color: AppColors.hint2)
                                  ]);
   
                            } else {
                              child =Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    customText(
                                        text: "No more Data",
                                        color: AppColors.hint2)
                                  ]); 
                            }
                            return child;
                          },
                        ),
                        onRefresh: () async {
                          final result = await coup.allCoupons(isRefresh: true);
                          if (result) {
                            refreshController.refreshCompleted();
                          } else {
                            refreshController.refreshFailed();
                          }
                        },
                        onLoading: () async {
                          final result = await coup.allCoupons();
                          if (result) {
                            refreshController.loadComplete();
                          } else {
                            refreshController.loadFailed();
                          }
                        },
                        child: isLoading
                            ? const Center(child: const CircularProgressIndicator())
                            : provider.couponlist.length != 0
                                ? ListView.separated(
                                    itemCount: provider.couponlist.length,
                                    itemBuilder: (BuildContext context, index) {
                                      final key = provider.couponlist[index];
                                      return Card(elevation: 2,
                                          color: AppColors.white,
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                                          child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 10,
                                                      horizontal: 0),
                                              child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Row(children: [
                                                      SizedBox(
                                                        width:
                                                            size.width * 0.01,
                                                      ),
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                           CustomContainer(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10),
                                                            width:
                                                                  size.width *
                                                                      0.58,
                                                            
                                                                    
                                                            
                                                            child: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  CircleAvatar(
                                                                    backgroundColor: AppColors
                                                                        .white10
                                                                        .withOpacity(
                                                                            0.3),
                                                                    radius:
                                                                        15.0,
                                                                    child: const Icon(
                                                                        Icons
                                                                            .date_range,size: 20,),
                                                                  ),
                                                                  SizedBox(
                                                                    width: size
                                                                            .width *
                                                                        0.02,
                                                                  ),
                                                                  customText(
                                                                      text: key
                                                                          .createdAt
                                                                          .toString()
                                                                          .substring(
                                                                              0,
                                                                              10),
                                                                      textSize:
                                                                          12.0),
                                                                ]),
                                                            // shadow: [
                                                            //   BoxShadow(
                                                            //       offset: const Offset(0, 1),
                                                            //       blurRadius: 10,
                                                            //       color: AppColors.black
                                                            //           .withOpacity(0.1))
                                                            // ],
                                                          ),
                                                          SizedBox(height: size.height*0.01,),
                                                          InkWell(
                                                            overlayColor:
                                                                MaterialStateProperty
                                                                    .all(AppColors
                                                                        .white10),
                                                            onTap: () async {
                                                              await FlutterClipboard
                                                                  .copy(key
                                                                      .keyid);
                                                              Alerts.showSuccess(
                                                                  "copied to clipboard");
                                                            },
                                                            child:
                                                                CustomContainer(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(10),

                                                              width:
                                                                  size.width *
                                                                      0.58,
                                                              
                                                              color:
                                                                  AppColors.white10.withOpacity(0.4),
                                                                  borderRadius: BorderRadius.circular(20),
                                                              child: Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceAround,
                                                                  children: [
                                                                    customText(
                                                                        text: key
                                                                            .keyid.toString().substring(0,9),
                                                                        weight: FontWeight
                                                                            .bold,
                                                                        textSize:
                                                                            15.0),
                                                                     CircleAvatar(
                                                                      backgroundColor:
                                                                          AppColors
                                                                              .white10,
                                                                      radius:
                                                                          20.0,
                                                                      child:
                                                                          const Icon(
                                                                        Icons
                                                                            .copy_all_rounded,
                                                                        color: AppColors
                                                                            .hint,
                                                                      ),
                                                                    )
                                                                  ]),

                                                            ),
                                                          ),
                                                          // SizedBox(
                                                          //   height:
                                                          //       size.height *
                                                          //           0.014,
                                                          // ),
                                                         
                                                        ],
                                                      ),
                                                      Column(children: [
                                                        SizedBox(
                                                            width: size.width *
                                                                0.29),
                                                        Column(children: [
                                                          customText(
                                                              text:
                                                                  "Coupon Status",
                                                              weight: FontWeight
                                                                  .bold),
                                                          SizedBox(
                                                            height:
                                                                size.height *
                                                                    0.004,
                                                          ),
                                                          ActiveBar(
                                                            bgcolor: key.keystatus==0?Colors.lightBlue.shade100:AppColors.white10,
                                                            text: customText(text: key.keystatus==0?"Used":"Unused"),
                                                            onTap: () {},
                                                          ),
                                                        ]),
                                                        SizedBox(
                                                          height: size.height *
                                                              0.02,
                                                        ),
                                                        Column(
                                                          children: [
                                                            customText(
                                                                text: "Status",
                                                                weight:
                                                                    FontWeight
                                                                        .bold),
                                                            SizedBox(
                                                              height:
                                                                  size.height *
                                                                      0.004,
                                                            ),
                                                            ActiveBar(
                                                              bgcolor: key.status ==0? Colors.green.shade100:Colors.red.shade700,
                                                              text: customText(text:key.status ==0? "Active": "In active"),
                                                              onTap: () {},
                                                              // bgcolor: Colors.greenAccent.shade100,
                                                              // fgcolor: AppColors.black,
                                                            ),
                                                          ],
                                                        ),
                                                      ]),
                                                      // const SizedBox(
                                                      //   height: 20,
                                                      // ),
                                                    ]),
                                                  ])));
                                    },
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return const SizedBox(height: 10);
                                    },
                                  )
                                : Center(
                                    child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Icon(
                                        Icons.search_off_rounded,
                                        color: AppColors.hint,
                                        size: 50.0,
                                      ),
                                      customText(text: "No Coupons.")
                                    ],
                                  ))))
              ])));
              
    },
    );
  }
  
   

  DropdownMenuItem<String> droplist1(String couponstatus) => DropdownMenuItem(
        value: couponstatus,
        child: Text(couponstatus),
      );

  DropdownMenuItem<String> droplist2(String sstatus) => DropdownMenuItem(
        value: sstatus,
        child: Text(sstatus),
      );
}
