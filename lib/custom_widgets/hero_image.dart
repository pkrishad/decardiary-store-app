// ignore_for_file: use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';

class HeroImage extends StatelessWidget {
  final String img;
  final double imgheight;
  const HeroImage({required this.img, required this.imgheight});

  @override
  Widget build(BuildContext context) {
    return CustomContainer(
        height:imgheight, child: SvgPicture.asset(img,color: AppColors.primaryColor.withOpacity(0.9),));
  }
}
