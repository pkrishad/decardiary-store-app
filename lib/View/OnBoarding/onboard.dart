import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iconsax/iconsax.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Login%20Screen/loginscreen.dart';
import 'package:storeadmin_app/View/OnBoarding/onboarddata.dart';
import 'package:storeadmin_app/View/OnBoarding/splash.dart';
import 'package:storeadmin_app/custom_widgets/custom_button.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textbutton.dart';
import 'package:storeadmin_app/custom_widgets/hero_image.dart';

class Welcome extends StatelessWidget {
  const Welcome({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    int totalpages = OnboardingItems.loadOnboardingItem().length;
    return Scaffold(
      body: PageView.builder(
        itemCount: totalpages,
        itemBuilder: (BuildContext context, int index) {
          var data = OnboardingItems.loadOnboardingItem()[index];
          return CustomContainer(
            height: size.height,
            width: size.width,
            color: data.color,
            padding: const EdgeInsets.fromLTRB(
              20,
              0,
              20,
              0,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    CustomtextButton(
                        onpressed: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LoginScreen()),
                              (route) => false);
                        },
                        text: "Skip",
                        fgColor: AppColors.white,
                        bgColor: Colors.transparent),
                  ],
                ),
                HeroImage(img: data.image, imgheight: size.height * 0.25),
                Column(children: [
                  customText(
                      text: data.title,
                      textSize: 25.0,
                      weight: FontWeight.bold,
                      color: AppColors.primaryColor),
                  SizedBox(height: size.height * 0.01),
                  SizedBox(
                    height: 100,
                    child: AnimatedTextKit(totalRepeatCount: 1, animatedTexts: [
                      TyperAnimatedText(data.subtititle,
                          speed: Duration(milliseconds: 60),
                          textStyle: GoogleFonts.poppins(
                              fontWeight: FontWeight.bold,
                              color: AppColors.primaryLightColor,
                              fontSize: 20.0))
                    ]),
                  ),

                  // customText(text: data.subtititle,color: AppColors.primaryLightColor,textSize: 20.0),
                  SizedBox(
                    height: size.height * 0.1,
                  ),
                  index == (totalpages - 1)
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MaterialButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 50.0, vertical: 15.0),
                                color: Colors.grey.shade50,
                                onPressed: () {
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      PageTransition(LoginScreen()),
                                      (route) => false);
                                },
                                child: Row(
                                  children: [
                                    customText(
                                        text: "Get Started", textSize: 20.0),
                                    SizedBox(
                                      width: size.width * 0.02,
                                    ),
                                    const Icon(Iconsax.arrow_right)
                                  ],
                                )),
                          ],
                        )
                      : Container()
                ]),
                CustomContainer(
                  width: size.width,
                  height: 13,
                  child: ListView.separated(
                      separatorBuilder: (context, index) {
                        return SizedBox(
                          width: 5,
                        );
                      },
                      itemCount: totalpages,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int i) {
                        return Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: CustomContainer(
                            borderRadius: BorderRadius.circular(20),
                            width: index == i ? 40 : 10,
                            color: index == i
                                ? AppColors.primaryColor
                                : Colors.grey.shade200,
                          ),
                        );
                      }),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
