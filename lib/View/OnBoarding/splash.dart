// ignore_for_file: prefer_const_constructors, unnecessary_import

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Login%20Screen/loginscreen.dart';
import 'package:storeadmin_app/View/OnBoarding/onboard.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/home/home.dart';

// String? finalUserName;

class Splash extends StatefulWidget {
  const Splash({
    Key? key,
  }) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with  TickerProviderStateMixin {
 double _fontSize = 2;
  double _containerSize = 1.5;
  double _textOpacity = 0.0;
  double _containerOpacity = 0.0;
  late AnimationController _controller;
  Animation<double>? animation1;

  @override
  void initState() {
     _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 3));

    animation1 = Tween<double>(begin: 40, end: 20).animate(CurvedAnimation(
        parent: _controller, curve: Curves.fastLinearToSlowEaseIn))
      ..addListener(() {
        setState(() {
          _textOpacity = 1.0;
        });
      });

    _controller.forward();

    Timer(Duration(seconds: 2), () {
      setState(() {
        _fontSize = 1.06;
      });
    });

    Timer(Duration(seconds: 2), () {
      setState(() {
        _containerSize = 2;
        _containerOpacity = 1;
      });
    });

    // Timer(Duration(seconds: 4), () {
    //   setState(() {
    //             Navigator.pushReplacement(context, PageTransition(SecondPage()));
    //   });
    // });
    Timer(Duration(seconds: 4), () => autologin(context));
    super.initState();
  }

  Future autologin(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var username = prefs.getString("username");
      bool postingData = await getPostingData();
            setPostingData();
            if (postingData == false || username == null) {
             Navigator.pushAndRemoveUntil(
          context,
          PageTransition(Welcome()),
          (route) => false);
            
    } else {
      Navigator.pushAndRemoveUntil(
          context,
          PageTransition(BaseBNBar()),
          (route) => false);
    }
  }

 
 


  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Column(
            children: [
              AnimatedContainer(
                duration: Duration(milliseconds: 2000),
                curve: Curves.fastLinearToSlowEaseIn,
                height: _height / _fontSize
              ),
              AnimatedOpacity(
                duration: Duration(milliseconds: 1000),
                opacity: _textOpacity,
                child: 
                 Text(
              'decARdiary store',
              style: TextStyle(fontFamily: 'Amsi', fontSize: animation1!.value, color: AppColors.primaryColor),
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
            ),
                // Text(
                //   '',
                //   style: TextStyle(
                //     color: Colors.white,
                //     fontWeight: FontWeight.bold,
                //     fontSize: animation1!.value,
                //   ),
                // ),
              ),
            ],
          ),
          Center(
            child: AnimatedOpacity(
              duration: Duration(milliseconds: 2000),
              curve: Curves.fastLinearToSlowEaseIn,
              opacity: _containerOpacity,
              child: AnimatedContainer(
                duration: Duration(milliseconds: 2000),
                curve: Curves.fastLinearToSlowEaseIn,
                height: _width / _containerSize,
                width: _width / _containerSize,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30),
                ),
                // child: Image.asset('assets/images/file_name.png')
                child:  SvgPicture.asset('assets/svg/Store mangr white.svg')
              ),
            ),
          ),
        ],
      ),
    );
  }
}

//   @override
//   Widget build(BuildContext context) {
//      checkConnection(context);
//       return Scaffold(
//       backgroundColor: AppColors.white,
//       body: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             Row(mainAxisAlignment: MainAxisAlignment.center, children: [
//               Container(
//                 width: 200,height: 200,
//                 child:ClipRRect(borderRadius: BorderRadius.circular(600),child:SvgPicture.asset('assets/svg/Store mangr white.svg')),
//              )]),
//           ]),
//     );
//   }
// }
setPostingData() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.setBool("alreadyVisited", true);
}

getPostingData() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  bool alreadyVisited = preferences.getBool("alreadyVisited") ?? false;
  return alreadyVisited;
}

class PageTransition extends PageRouteBuilder {
  final Widget page;

  PageTransition(this.page)
      : super(
          pageBuilder: (context, animation, anotherAnimation) => page,
          transitionDuration: Duration(milliseconds: 2500),
          transitionsBuilder: (context, animation, anotherAnimation, child) {
            animation = CurvedAnimation(
              curve: Curves.fastLinearToSlowEaseIn,
              parent: animation,
            );
            return Align(
              alignment: Alignment.bottomCenter,
              child: SizeTransition(
                sizeFactor: animation,
                child: page,
                axisAlignment: 0,
              ),
            );
          },
        );
}

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        brightness: Brightness.dark,
        backgroundColor: Colors.deepPurple,
        centerTitle: true,
        title: Text(
          'YOUR APP\'S NAME',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
    );
  }
}