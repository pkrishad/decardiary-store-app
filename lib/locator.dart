import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:storeadmin_app/Navigation/nav.dart';
import 'package:storeadmin_app/View/Coupons/All%20Coupons/p_couponlist.dart';
import 'package:storeadmin_app/View/Coupons/History/p_history.dart';
import 'package:storeadmin_app/View/DashBoard/providerdash.dart';
import 'package:storeadmin_app/View/Models/MProvider/model_list.dart';
import 'package:storeadmin_app/View/Models/MProvider/updatecmod.dart';
import 'package:storeadmin_app/View/Models/MProvider/updatemlist.dart';
import 'package:storeadmin_app/View/Notifications/providernot.dart';
import 'package:storeadmin_app/View/Profile/providerprofile.dart';
import 'package:storeadmin_app/View/UpdateProfile/p_update.dart';
import 'package:storeadmin_app/View/UpdateProfile/p_updatepwd.dart';
import 'package:storeadmin_app/View/Users/p_users.dart';
import 'package:storeadmin_app/View/products/ProProvider/pcategry.dart';
import 'package:storeadmin_app/View/products/ProProvider/proprovider.dart';
import 'package:storeadmin_app/View/products/ProProvider/updatecproducts.dart';
import 'package:storeadmin_app/View/products/ProProvider/updateplist.dart';
import 'package:storeadmin_app/repo.dart';
import 'Logout/providerlogout.dart';
import 'Values/values.dart';
import 'Version Check/providerversion.dart';
import 'View/Login Screen/login_int.dart';
import 'View/Models/MProvider/categoryprovider.dart';
import 'dio/dio_client.dart';
import 'dio/log_interceptor.dart';

final loc = GetIt.instance;

Future<void> init() async {
  final sharedPreferences = await SharedPreferences.getInstance();
  loc.registerLazySingleton(() => sharedPreferences);
  loc.registerLazySingleton(() => Dio());
  loc.registerLazySingleton(() => LoggingInterceptor());
  loc.registerLazySingleton(() => NavigationService());

  // loc.registerLazySingleton(() => UserPreferences());
  loc.registerLazySingleton(() => DioClient(
        StringConst.BASE_URL,
        loc(),
        loggingInterceptor: loc(),
        sharedPreferences: loc(),
        navigationService: loc(),
      ));

  // Repository
  loc.registerLazySingleton(
      () => Repo(sharedPreferences: loc(), dioClient: loc()));

  //ModelClass
  // loc.registerLazySingleton(() => CoupModelProvider());

  // Provider
  loc.registerFactory(
      () => LoginUser(dioClient: loc(), repo: loc(), sharedPreferences: loc()));
  loc.registerFactory(() => ProviderVersion(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => Logout(repo: loc(), dioClient: loc()));
  loc.registerFactory(() => ProviderDash(repo: loc(), dioClient:loc()));
  loc.registerFactory(() => ProviderProfile(repo: loc(), dioClient: loc()));
  loc.registerFactory(() => ProviderModel(repo: loc(), dioClient: loc()));
  loc.registerFactory(() => ProviderCoupon(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => ProviderHistory(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => ProviderUser(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => UpdateProfile(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => UpdatePwd(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => ProviderModellist(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => ProviderCProduct(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => ProviderProduct(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => UpdateCProduct(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => UpdateProductlist(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => UpdateCModel(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => UpdateModellist(dioClient: loc(), repo: loc()));
  loc.registerFactory(() => ProviderNot(dioClient: loc(), repo: loc()));
}
