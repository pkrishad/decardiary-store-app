import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/products/ProProvider/proprovider.dart';
import 'package:storeadmin_app/View/products/ProductScreen/plistfilter.dart';
import 'package:storeadmin_app/View/products/ProductScreen/prodetails.dart';
import 'package:storeadmin_app/View/products/productClass/productclass.dart';
import 'package:storeadmin_app/custom_widgets/bulkupdate.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_button.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_dropdwnfield.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_textbutton.dart';
import 'package:storeadmin_app/custom_widgets/custom_textfield.dart';
import 'package:storeadmin_app/custom_widgets/custom_toggleswitchbutton.dart';
import 'package:storeadmin_app/custom_widgets/dialog.dart';
import 'package:storeadmin_app/custom_widgets/keystatus.dart';
import 'package:storeadmin_app/custom_widgets/productlistcard.dart';

class Products extends StatefulWidget {
  const Products({Key? key}) : super(key: key);

  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  final controller = TextEditingController();
  Datum? datum;
  String stock = "";
  final list = {"All", "Small", "Medium", "Big"};
  final page = ["All", "16", "32"];
  String pge = "";
  String? pagecontroller;
  bool bulkupdate = false;
  bool togglevalue = false;
  bool stocktoggle = false;
  bool isLoading = false;
  late int data;
  late int stok;
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);
  @override
  Widget build(BuildContext context) {
    checkConnection(context);
    Size size = MediaQuery.of(context).size;
    ProviderProduct prodlist =
        Provider.of<ProviderProduct>(context, listen: true);
    return Consumer<ProviderProduct>(builder: (context, provider, child) {
      return Scaffold(
          backgroundColor: AppColors.white,
          body: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(children: [
                Row(
                  children: [
                    CustomContainer(
                      align: Alignment.center,
                      borderRadius: BorderRadius.circular(10),
                      width: size.width * 0.66,
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      color: AppColors.white,
                      height: size.height * 0.06,
                      child: CustomTextFormField(
                        controller: controller,
                        suffixIcon: InkWell(
                            onTap: () async {
                              await provider.getProducts(
                                search: controller.text.toUpperCase(),
                                isRefresh: true,
                              );
                            },
                            child: const Icon(Icons.search_outlined)),
                        hintText: "Search",
                        hintColor: AppColors.hint,
                      ),
                    ),
                    SizedBox(width: size.width * 0.01),
                    const PlistFilter(),
                    SizedBox(width: size.width * 0.02),
                    Bulk(
                      ontap: () {
                        showModalBottomSheet(
                            shape: const RoundedRectangleBorder(
                                borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20))),
                            context: context,
                            builder: ((builder) => CustomContainer(
                                height: size.height * 0.3,
                                width: size.width,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: size.height * 0.02,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          customText(
                                              text: "Bulk update",
                                              color: AppColors.dark,
                                              textSize: 20.0,
                                              weight: FontWeight.bold),
                                          CustomtextButton(
                                              onpressed: () {
                                                Navigator.pop(context);
                                              },
                                              text: "Cancel")
                                        ],
                                      ),
                                      SizedBox(
                                        height: size.height * 0.01,
                                      ),
                                      SizedBox(
                                        height: size.height * 0.03,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          customText(
                                              text: "Stocks", textSize: 25.0),
                                          SizedBox(
                                            height: size.height * 0.02,
                                          ),
                                          Togglebtn(
                                            toggleButton: () {
                                              bulkupdate = !bulkupdate;

                                              if (bulkupdate) {
                                                data = 1;
                                              } else if (!bulkupdate) {
                                                data = 0;
                                              }
                                              provider.updatestockUpload(data);
                                              Future.delayed(
                                                  const Duration(seconds: 1),
                                                  () {
                                                refreshController
                                                    .requestRefresh(
                                                  needMove: false,
                                                  needCallback: false,
                                                );
                                              });

                                              Navigator.pop(context);
                                              data == 0 ?
               ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: customText(text: '${provider.proddata.length}  Stock enabled successfully',color: AppColors.white),
            behavior: SnackBarBehavior.floating,duration: Duration(seconds: 3),
            backgroundColor: AppColors.dark)):                               
     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: customText(text: '${provider.proddata.length}  Stock disabled successfully',color: AppColors.white),
            behavior: SnackBarBehavior.floating,duration: Duration(seconds: 3),
            backgroundColor: AppColors.dark));
                                            },
                                            left: provider
                                                    .proddata.first.stockStatus
                                                    .toString()
                                                    .contains('0')
                                                ? 30.0
                                                : 0.0,
                                            right: provider
                                                    .proddata.first.stockStatus
                                                    .toString()
                                                    .contains('0')
                                                ? 0.0
                                                : 30.0,
                                            togglecolor: provider
                                                    .proddata.first.stockStatus
                                                    .toString()
                                                    .contains('0')
                                                ? Colors.green[200]
                                                : Colors.grey[300],
                                            icon: provider
                                                    .proddata.first.stockStatus
                                                    .toString()
                                                    .contains('0')
                                                ? const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  )
                                                : const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: size.height * 0.03,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          customText(
                                              text: "Actions", textSize: 25.0),
                                          SizedBox(
                                            height: size.height * 0.02,
                                          ),
                                          Togglebtn(
                                            toggleButton: () {
                                              bulkupdate = !bulkupdate;

                                              if (bulkupdate) {
                                                data = 1;
                                              } else if (!bulkupdate) {
                                                data = 0;
                                              }
                                              provider.updateBulkUpload(data);
                                              Future.delayed(
                                                  const Duration(
                                                      microseconds: 10), () {
                                                refreshController
                                                    .requestRefresh(
                                                  needMove: false,
                                                  needCallback: true,
                                                );
                                                // refreshController
                                                //       .refreshCompleted();
                                              });

                                              Navigator.pop(context);
                                              data == 0?
                                                 ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: customText(text: '${provider.proddata.length}  Products enabled successfully',color: AppColors.white),
            behavior: SnackBarBehavior.floating,dismissDirection: DismissDirection.startToEnd,duration: Duration(seconds: 3),
            backgroundColor: AppColors.dark)):                               
     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: customText(text: '${provider.proddata.length}  Products disabled successfully',color: AppColors.white),
            behavior: SnackBarBehavior.floating,dismissDirection: DismissDirection.startToEnd,duration: Duration(seconds: 3),
            backgroundColor: AppColors.dark));
                                            },
                                            left: provider.proddata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? 33.0
                                                : 0.0,
                                            right: provider
                                                    .proddata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? 0.0
                                                : 33.0,
                                            togglecolor: provider
                                                    .proddata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? Colors.green[200]
                                                : Colors.grey[300],
                                            icon: provider.proddata.first.status
                                                    .toString()
                                                    .contains('0')
                                                ? const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  )
                                                : const Icon(
                                                    Icons.circle,
                                                    color: Colors.white,
                                                    size: 35.0,
                                                  ),
                                          ),
                                        ],
                                      ),
                                    ]))));
                      },
                      icon: Icon(
                        Icons.fact_check_outlined,
                        color: AppColors.red.withOpacity(0.8),
                      ),
                      color: AppColors.white10,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: SmartRefresher(
                      controller: refreshController,
                      enablePullUp: true,
                      footer: CustomFooter(
                        builder: (BuildContext context, LoadStatus? mode) {
                          Widget child;
                          if (mode == LoadStatus.idle) {
                            child = Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  customText(
                                      text: "Swipe up to load more",
                                      color: AppColors.hint2)
                                ]);
                          } else if (mode == LoadStatus.loading) {
                            child = Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const CupertinoActivityIndicator(),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  customText(
                                      text: "Loading..", color: AppColors.hint2)
                                ]);
                          } else if (mode == LoadStatus.failed) {
                            child = Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Icon(
                                    Icons.error_outlined,
                                    color: AppColors.hint2,
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  customText(
                                      text: "No more Data",
                                      color: AppColors.hint2)
                                ]);
                          } else if (mode == LoadStatus.canLoading) {
                            child = Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  customText(
                                      text: "Swipe up to load more",
                                      color: AppColors.hint2)
                                ]);
                          } else {
                            child = Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  customText(
                                      text: "No more Data",
                                      color: AppColors.hint2)
                                ]);
                          }
                          return child;
                        },
                      ),
                      onRefresh: () async {
                        final result =
                            await prodlist.getProducts(isRefresh: true);
                        if (result) {
                          refreshController.refreshCompleted();
                        } else {
                          refreshController.refreshFailed();
                        }
                      },
                      onLoading: () async {
                        final result = await prodlist.getProducts();
                        if (result) {
                          refreshController.loadComplete();
                        } else {
                          refreshController.loadFailed();
                        }
                      },
                      child: isLoading
                          ? const Center(child: CircularProgressIndicator())
                          : provider.proddata.isNotEmpty
                              ? ListView.separated(
                                  shrinkWrap: true,
                                  separatorBuilder: (
                                    BuildContext context,
                                    int index,
                                  ) {
                                    return SizedBox(
                                      height: size.height * 0.009,
                                    );
                                  },
                                  itemCount: provider.proddata.length,
                                  itemBuilder: (BuildContext context, index) {
                                    final prod = provider.proddata[index];
                                    return ProductlistCard(
                                      ontap: () async {
                                        await Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ProductDetails(
                                                      productdetails: prod,
                                                    )));
                                      },
                                      stock: customText(
                                          text: prod.stockStatus == 0
                                              ? "In Stock"
                                              : "Out Of Stock"),
                                      onhover: (stocktoggle) {
                                        setState(() {
                                          stocktoggle = !stocktoggle;
                                        });
                                        
                                      },
                                      stocktoggleButton: () {
                                        setState(() {
                                          stocktoggle = !stocktoggle;
                                        if (stocktoggle) {
                                          stok = 1;
                                        } else if (!stocktoggle) {
                                          stok = 0;
                                        }
                                        });
                                        
                                        provider.updateStock(
                                          productid: prod.id,
                                          status: prod.status,
                                          stock: stok,
                                        );
                                        Future.delayed(
                                            const Duration(milliseconds: 100),
                                            () {
                                          provider.getProducts(
                                              perpage: 'all', isRefresh: true);
                                        });
                                      },
                                      left: prod.stockStatus == 0 ? 33.0 : 0.0,
                                      right: prod.stockStatus == 0 ? 0.0 : 33.0,
                                      togglecolor: prod.stockStatus == 0
                                          ? Colors.green[200]
                                          : Colors.grey.shade300,
                                      icon: prod.stockStatus == 0
                                          ? const Icon(
                                              Icons.circle,
                                              color: Colors.white,
                                              size: 35.0,
                                            )
                                          : const Icon(
                                             Icons.circle,
                                              color: Colors.white,
                                              size: 35.0,
                                            ),
                                            onhover2: (togglevalue) {
                                       
                                          togglevalue = !togglevalue;
                                       
                                        
                                      },
                                      toggleButton: () {
                                        togglevalue = !togglevalue;

                                        if (!togglevalue) {
                                          data = 0;
                                        } else if (togglevalue) {
                                          data = 1;
                                        }

                                        provider.updateStatus(
                                          productid: prod.id,
                                          status: data,
                                          stock: prod.stockStatus,
                                        );

                                        Future.delayed(
                                            const Duration(milliseconds: 200),
                                            () {
                                          provider.getProducts(
                                              perpage: 'all', isRefresh: true);
                                        });
                                      },
                                      left2: prod.status == 0 ? 30.0 : 0.0,
                                      right2: prod.status == 0 ? 0.0 : 30.0,
                                      togglecolor2: prod.status == 0
                                          ? Colors.green[200]
                                          : Colors.grey.shade300,
                                      icon2: prod.status == 0
                                          ? const Icon(
                                              Icons.circle,
                                              color: Colors.white,
                                              size: 35.0,
                                            )
                                          : const Icon(
                                              Icons.circle,
                                              color: Colors.white,
                                              size: 35.0,
                                            ),
                                      bgcolor: prod.status == 0
                                          ? Colors.green.shade100
                                          : Colors.red.shade700,
                                      txtactive: customText(
                                          text: prod.status == 0
                                              ? "Active"
                                              : "Inactive"),
                                      modelname: prod.name,
                                      activeontap: () {},
                                      image: prod.image.isEmpty
                                          ? const Center(
                                              child: Icon(
                                              Icons.image_not_supported_rounded,
                                              color: AppColors.hint,
                                              size: 30.0,
                                            ))
                                          : Image.network(prod.image,
                                              loadingBuilder:
                                                  (BuildContext context,
                                                      Widget child,
                                                      ImageChunkEvent?
                                                          loadingProgress) {
                                              if (loadingProgress == null) {
                                                return child;
                                              }
                                              return Center(
                                                child:
                                                    CircularProgressIndicator(
                                                  color: AppColors.dark
                                                      .withOpacity(0.5),
                                                  value: loadingProgress
                                                              .expectedTotalBytes !=
                                                          null
                                                      ? loadingProgress
                                                              .cumulativeBytesLoaded /
                                                          loadingProgress
                                                              .expectedTotalBytes!
                                                      : null,
                                                ),
                                              );
                                            }, fit: BoxFit.fill),
                                      textadmin: prod.globalStatus == 0
                                          ? ''
                                          : 'Removed by Admin',
                                    );
                                  },
                                )
                              : Center(
                                  child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Icon(
                                      Icons.search_off_rounded,
                                      color: AppColors.hint,
                                      size: 50.0,
                                    ),
                                    customText(text: "No Products")
                                  ],
                                ))),
                )
              ])));
    });
  }

  DropdownMenuItem<String> droplist2(String sstatus) =>
      DropdownMenuItem(value: sstatus, child: Text(sstatus));
      void _displaySuccessMotionToast() {
    
  }
}
  
