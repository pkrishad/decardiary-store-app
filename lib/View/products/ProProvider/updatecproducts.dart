// ignore_for_file: avoid_print, unused_local_variable

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class UpdateCProduct with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;

  UpdateCProduct({required this.repo, required this.dioClient});
// "product_categories"
  Future<ApiResponse?> updateprocategory(
      {required BuildContext context,
      required int categoryid,
      required int status}) async {
    // Dialogs.showLoading(context: context);
    var mapData = {
      "product_categories": [
        json.encode({"category_id":categoryid,"status":status})
      ].toString()
    };
    print(mapData);
   
    ApiResponse? res = await repo.postData(
        StringConst.UPDATE_PRODUCT_CATEGORIES,
        data: mapData,);
    final resdata = (res.response?.data);
    if (res.response != null && res.response?.statusCode == 200) {
    } else if (res.error != "401") {
      if (kDebugMode) {
        print(res.error);
      }
      // Alerts.showError("Enter a valid Username and Password");
    }
  }
}
