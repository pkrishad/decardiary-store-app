// ignore_for_file: prefer_const_literals_to_create_immutables, non_constant_identifier_names, avoid_print
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iconsax/iconsax.dart';
import 'package:provider/provider.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/Version%20Check/providerversion.dart';
import 'package:storeadmin_app/View/Coupons/keys.dart';
import 'package:storeadmin_app/View/DashBoard/dashboard.dart';
import 'package:storeadmin_app/View/Models/ModelsectionScreen/basemodels.dart';
import 'package:storeadmin_app/View/products/ProductScreen/baseproduct.dart';
import 'package:storeadmin_app/View/Users/users.dart';
import 'package:storeadmin_app/custom_widgets/custom_bnbar.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:url_launcher/url_launcher.dart';

class BaseBNBar extends StatefulWidget {
  const BaseBNBar({Key? key}) : super(key: key);
  @override
  State<BaseBNBar> createState() => _BaseBNBarState();
}

class _BaseBNBarState extends State<BaseBNBar> {
  int phoneno = 918589011223;
  int index = 0;
  int _selectedIndex = 0;
  late PageController _pageController;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List<Widget> listScreens = [
    // Tab1(),
    // Tab2(),
    // Tab3(),
    // Tab4(),
    // ProfileActivity(),
  ];
  // Widget Pages() {
  //   switch (index) {
  //     case 1:
  //       return const BaseProducts();
  //     case 2:
  //       return const BaseModels();
  //     case 3:
  //       return const Users();
  //     case 4:
  //       return const Keys();
  //     default:
  //       return const DashBoard();
  //   }
  // final isDialOpen = ValueNotifier(false);

  DateTime pre_backpress = DateTime.now();

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // checkConnection(context);
    return Consumer<ProviderVersion>(builder: (context, provider, child) {
      return WillPopScope(
          onWillPop: () async {
            final timegap = DateTime.now().difference(pre_backpress);
            final cantExit = timegap >= const Duration(seconds: 2);
            pre_backpress = DateTime.now();
            if (cantExit) {
              //show snackbar
              final snack = SnackBar(
                content: customText(
                    text: 'Press Again to Exit', color: AppColors.dark),
                backgroundColor: AppColors.white,
                duration: const Duration(seconds: 2),
              );
              ScaffoldMessenger.of(context).showSnackBar(snack);
              return false;
            } else {
              return true;
            }
          },
          child: SafeArea(
              child: Scaffold(
                  key: _scaffoldKey,
                  body: Pages(),
                  floatingActionButton: FloatingActionButton(
                    elevation: 0,
                    backgroundColor: Colors.transparent,
                    onPressed: () async {
                      var whatsappUrl =
                          "whatsapp://send?phone=${provider.version?.supportNumber}&text=I would like to know more..";
                      await canLaunch(whatsappUrl)
                          ? launch(whatsappUrl)
                          : print(
                              "open whatsapp app link or do a snackbar with notification that there is no whatsapp installed");
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(
                            colors: [
                              Colors.green.shade300,
                              Colors.green.shade600
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter),
                      ),
                      child: SvgPicture.asset(
                        "assets/svg/wtsapp.svg",
                        width: 55.0,
                        color: AppColors.white,
                      ),
                    ),
                  ),
                  bottomNavigationBar: Container(
                    // foregroundDecoration:BoxDecoration(color: Colors.transparent),
                    margin: EdgeInsets.all(0),
                    height: size.width * .158,
                    width: size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: AppColors.primaryColor.withOpacity(.15),
                          blurRadius: 30,
                          offset: Offset(0, 10),
                        ),
                      ],

                      // only(
                      //     topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                    ),
                    child: ListView.builder(
                      itemCount: 5,
                      scrollDirection: Axis.horizontal,
                      padding:
                          EdgeInsets.symmetric(horizontal: size.width * .01),
                      itemBuilder: (context, index) => InkWell(
                        onTap: () {
                          setState(
                            () {
                              this.index = index;
                            },
                          );
                        },
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(height: size.width * .021),
                            Container(padding: EdgeInsets.all(8),decoration: BoxDecoration(color: Colors.grey.shade50,borderRadius: BorderRadius.circular(8.0)),
                              child: Icon(
                                  listOfIcons[index],
                                  // size: size.width * .076,
                                  
                                  size: 25,
                                  color: this.index == index? AppColors.primaryColor:AppColors.black,
                                ),
                            ),
                            
                            SizedBox(height: 5,),
                            AnimatedContainer(
                              duration: Duration(milliseconds: 600),
                              curve: Curves.linear,
                              margin: EdgeInsets.only(
                                bottom: this.index == index
                                    ? 0
                                    : size.width * 0,
                                right: size.width * .0322,
                                left: size.width * .0322,
                              ),
                              width: size.width * .128,
                              height: this.index == index
                                  ? size.width * .014
                                  : 0,
                              decoration: BoxDecoration(
                                color: AppColors.primaryColor,
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            SizedBox(height: size.width * .0001),
                          ],
                        ),
                      ),
                    ),
                    // CustomBnBar(
                    //   selectedIndex: index,
                    //   onItemSelected: (index) =>setState(() {
                    //     this.index = index;
                    //   }),
                    //   icon1: const Icon(Icons.home_outlined),
                    //   text1: "Home",
                    //   icon2: const Icon(Icons.shopping_bag_outlined),
                    //   text2: "Products",
                    //   icon3: const Icon(Icons.view_module_outlined),
                    //   text3: "Models",
                    //   icon4: const Icon(Icons.supervisor_account_outlined),
                    //   text4: "Users",
                    //   icon5: const Icon(Icons.confirmation_num_outlined),
                    //   text5: "Coupons"
                    // ),
                  ))));
    });
  }

  List<IconData> listOfIcons = [
    Iconsax.home,
    Iconsax.bag,
    Iconsax.picture_frame,
    Iconsax.people,
    Icons.confirmation_num_outlined
  ];
  Widget Pages() {
    switch (index) {
      case 1:
        return const BaseProducts();
      case 2:
        return const BaseModels();
      case 3:
        return const Users();
      case 4:
        return const Keys();
      default:
        return const DashBoard();
    }
  }
}



// icon1: CircleAvatar(radius:17.0,backgroundColor: AppColors.white10,child: SvgPicture.asset("assets/svg/home.svg",fit: BoxFit.contain,width: 23.0,color: AppColors.pink,)),
//             text1: "Home",
//             icon2: CircleAvatar(radius:17.0,backgroundColor: AppColors.white10,child: SvgPicture.asset("assets/svg/shopping-bag.svg",width: 23.0,color: Colors.red.shade700,)),
//             text2: "Products",
//             icon3: CircleAvatar(radius:17.0,backgroundColor: AppColors.white10,child: SvgPicture.asset("assets/svg/cube.svg",fit: BoxFit.contain,width: 23.0,color: Colors.green.shade900)),
//             text3: "Models",
//             icon4: CircleAvatar(radius:17.0,backgroundColor: AppColors.white10,child: SvgPicture.asset("assets/svg/users-three.svg",fit: BoxFit.contain,width: 23.0,color: AppColors.darkblue,)),
//             text4: "Users",
//             icon5: CircleAvatar(radius:17.0,backgroundColor: AppColors.white10,child: SvgPicture.asset("assets/svg/coupon.svg",fit: BoxFit.contain,width: 23.0,)),
//             text5: "Coupons"