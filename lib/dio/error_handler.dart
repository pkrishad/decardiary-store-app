// ignore_for_file: curly_braces_in_flow_control_structures, prefer_is_empty

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:storeadmin_app/dio/err_response.dart';

class ApierrorHandler {
  static dynamic getMessage(error) {
    dynamic errorDescription = "";
    if(error is Exception){
      try { 
        if(error is DioError) {
          switch (error.type) {
            case DioErrorType.cancel:
            errorDescription = "Request to the api server was cancelled";
            break;
            case DioErrorType.connectTimeout:
            errorDescription = "connection timeout with API server";
            break;
            case DioErrorType.other:
            errorDescription = "Connection to API server failed due to internet connection";
            break;
            case DioErrorType.receiveTimeout:
            errorDescription = 
            "Receive timeout in connection with API server";
            break;
            case DioErrorType.response:
            switch (error.response!.statusCode){
              case 401:
              if (kDebugMode) {
                print("401");
              }
              break;
              case 404:
              case 500:
              case 503:
              errorDescription = error.response!.statusMessage;
         break;
         default:
                  ErrorResponse errorResponse =
                  ErrorResponse.fromJson(error.response!.data);
                  if (errorResponse.errors.length > 0)
                    errorDescription = errorResponse;
                  else
                    errorDescription =
                    "Failed to load data - status code: ${error.response!.statusCode}";
              }
            break;
            case DioErrorType.sendTimeout:
            errorDescription = "send timeout with server";
            break;
          }
          }else {
            errorDescription = "unexpected error occured";
          }
      }on FormatException catch (e){
        errorDescription = e.toString();
      }
      } else{
        errorDescription = "is not a subtype of exeption";
      }
      return errorDescription;
      }
  }

