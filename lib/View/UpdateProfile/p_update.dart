import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class UpdateProfile with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;
  UpdateProfile({
    required this.repo,
    required this.dioClient,
  });

  Future<ApiResponse?> updateuser(
      {required BuildContext context,
      required TextEditingController username,
      required TextEditingController password}) async {
    final Map<String, dynamic> updateData = {
      "current_password": password.text,
      "username": username.text,
    };

    ApiResponse res = await repo.postData(StringConst.UPDATE_USERNAME,
        data: jsonEncode(updateData));
    if (kDebugMode) {
      print(res.response!.statusCode);
    }
    final resdata = (res.response?.data);
    if (resdata["status"] == "success" && res.response!.statusCode == 200) {
      Alerts.showSuccess("Username changed successfully");
      notifyListeners();
    } else if (res.error != "401") {
      if (kDebugMode) {
        print(res.error);
      }
      Alerts.showError("Enter valid Password");
    }
  }
}
