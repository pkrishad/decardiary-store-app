// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

import 'custom_text.dart';

class CustomBnBar extends StatelessWidget {
  final selectedIndex;
  final onItemSelected;
  final icon1;
  final text1;
  final icon2;
  final icon3;
  final icon4;
  final icon5;
  final text2;
  final text3;
  final text4;
  final text5;
  const CustomBnBar({Key? key, this.selectedIndex, this.onItemSelected, this.icon1, this.text1, this.icon2, this.icon3, this.icon4, this.icon5, this.text2, this.text3, this.text4, this.text5}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavyBar(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      itemCornerRadius: 80,
      containerHeight: 68,
      backgroundColor: AppColors.white,
      showElevation: false,
      iconSize: 30,
      selectedIndex: selectedIndex,
      onItemSelected: onItemSelected,
      items: [
        BottomNavyBarItem(
            icon: icon1,
            title: customText(text:text1),
            textAlign: TextAlign.center,
            activeColor: const Color(0xFFDF6E8A),
            inactiveColor: AppColors.dark.withOpacity(0.7)),
        BottomNavyBarItem(
            icon: icon2,
            title: customText(text: text2),
            textAlign: TextAlign.center,
             activeColor: const Color(0xFFDF6E8A),
            inactiveColor: AppColors.dark.withOpacity(0.7)),
        BottomNavyBarItem(
            icon: icon3,
            title: customText(text: text3),
            textAlign: TextAlign.center,
           activeColor: const Color(0xFFDF6E8A),
            inactiveColor: AppColors.dark.withOpacity(0.7)),
        BottomNavyBarItem(
            icon: icon4,
            title: customText(text: text4),
            textAlign: TextAlign.center,
            activeColor: const Color(0xFFDF6E8A),
            inactiveColor: AppColors.dark.withOpacity(0.7)),
        BottomNavyBarItem(
            icon: icon5,
            title: customText(text: text5),
            textAlign: TextAlign.center,
              activeColor: const Color(0xFFDF6E8A),
            inactiveColor: AppColors.dark.withOpacity(0.7)),
      ],
    );
  }
}


// BottomNavyBar(
//       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       itemCornerRadius: 80,
//       containerHeight: 68,
//       backgroundColor: AppColors.white,
//       showElevation: false,
//       iconSize: 20,
//       selectedIndex: selectedIndex,
//       onItemSelected: onItemSelected,
//       items: [
//         BottomNavyBarItem(
//             icon: icon1 ,
//             title: customText(text:text1),
//             textAlign: TextAlign.center,
//             activeColor: const Color(0xFFDF6E8A),
//             inactiveColor: AppColors.dark.withOpacity(0.7)),
//         BottomNavyBarItem(
//             icon:  icon2,
//             title: customText(text: text2),
//             textAlign: TextAlign.center,
//              activeColor: Colors.red.shade900,
//             inactiveColor: AppColors.dark.withOpacity(0.7)),
//         BottomNavyBarItem(
//             icon:  icon3 ,
//             title: customText(text: text3),
//             textAlign: TextAlign.center,
//            activeColor: Colors.green.shade900,
//             inactiveColor: AppColors.dark.withOpacity(0.7)),
//         BottomNavyBarItem(
//             icon:  icon4 ,
//             title: customText(text: text4),
//             textAlign: TextAlign.center,
//             activeColor: AppColors.darkblue,
//             inactiveColor: AppColors.dark.withOpacity(0.7)),
//         BottomNavyBarItem(
//             icon:  icon5 ,
//             title: customText(text: text5),
//             textAlign: TextAlign.center,
//               activeColor:  Colors.amber.shade800,
//             inactiveColor: AppColors.dark.withOpacity(0.7)),
//       ],
//     );