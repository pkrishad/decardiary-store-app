import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/custom_widgets/dialog.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/home/home.dart';
import 'package:storeadmin_app/repo.dart';

class LoginUser with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;
  final SharedPreferences sharedPreferences;

  LoginUser(
      {required this.repo,
      required this.dioClient,
      required this.sharedPreferences});

  Future<void> loginUser(BuildContext context, TextEditingController _username,
      TextEditingController _password) async {
    // Dialogs.showLoading(context: context);
    String? fcmtoken = await FirebaseMessaging.instance.getToken();
    final Map<String, dynamic> loginData = {
      "userName": _username.text,
      "password": _password.text,
      "fcm": fcmtoken.toString()
    };
    ApiResponse? res =
        await repo.postData(StringConst.LOGIN, data: jsonEncode(loginData));

    if (kDebugMode) {
      print(res.response?.statusCode);
    }
    final resdata = (res.response?.data);

    if (res.response != null &&
        resdata["status"] == "success" &&
        res.response?.statusCode == 200) {
      Alerts.showSuccess("Logged In successfully");

      final token = resdata["data"]["token"];
      dioClient.dio.options.headers['Authorization'] = 'Bearer $token';
      sharedPreferences.setString(StringConst.TOKEN, token);
      sharedPreferences.setString("username", _username.text);
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => const BaseBNBar()),
          (route) => false);
    } else if (res.error != "401") {
      if (kDebugMode) {
        print(res.error);
      }
      Alerts.showError("Enter a valid Username and Password");
    }
  }
}
