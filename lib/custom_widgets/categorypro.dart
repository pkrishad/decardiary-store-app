// ignore_for_file: prefer_typing_uninitialized_variables, duplicate_ignore

import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/custom_widgets/activebar.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/custom_toggleswitchbutton.dart';

class CategoryPro extends StatelessWidget {
  final String text;

  final void Function()? toggleButton;
  final void Function(bool)? onhover;
  // ignore: prefer_typing_uninitialized_variables
  final activetxt;
  // ignore: prefer_typing_uninitialized_variables
  final bgcolor;
  final onend;
  final double? left;
  final double? right;
  final Color? togglecolor;
  final Widget? icon;
  final Widget? image;
  const CategoryPro({
    Key? key,
    required this.text,
    required this.toggleButton,
    this.activetxt,
    this.bgcolor,
    this.onend,
    this.left,
    this.right,
    this.togglecolor,
    this.icon,
    this.image, this.onhover,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.2,
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(10),
          // gradient: LinearGradient(colors:[AppColors.white,AppColors.primaryLightColor],begin: Alignment.topCenter),
        ),
        child: Stack(children: [
          Positioned(
              top: size.aspectRatio * 40.0,
              bottom: size.aspectRatio * 40.0,
              left: size.aspectRatio * 35.0,
              right: size.aspectRatio * 235.0,
              child: Container(
                  height: 94,
                  decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      boxShadow: [
                        BoxShadow(
                            offset: const Offset(0, 1),
                            blurRadius: 10,
                            color: AppColors.black.withOpacity(0.1)),
                      ]),
                  child: Stack(children: [
                    image!,
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: Container(
                        height: 35,
                        padding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 10),
                        alignment: Alignment.centerRight,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10)),
                          color: AppColors.white10,
                        ),
                        child: customText(
                            text: text, textSize: 20.0, color: AppColors.dark),
                      ),
                    )
                  ]))),
          Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Column(children: [
                      Column(children: [
                        customText(text: "Enable", weight: FontWeight.bold),
                        SizedBox(
                          height: size.height * 0.004,
                        ),
                        Togglebtn(
                          onhover: onhover,
                          togglecolor: togglecolor,
                          icon: icon,
                          left: left,
                          right: right,
                          onend: onend,
                          toggleButton: toggleButton,
                        ),
                      ]),
                      SizedBox(
                        height: size.height * 0.02,
                      ),
                      Column(
                        children: [
                          customText(text: "Status", weight: FontWeight.bold),
                          SizedBox(
                            height: size.height * 0.004,
                          ),
                          ActiveBar(
                            bgcolor: bgcolor,
                            text: activetxt,
                            onTap: () {},
                          ),
                        ],
                      ),
                    ]),
                  ],
                ),
              ),
            ],
          ),
        ]));
  }
}
