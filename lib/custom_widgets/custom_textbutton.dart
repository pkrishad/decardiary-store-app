// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

Widget CustomtextButton({required onpressed, required String text,txtclr,shadowcolor,font,padding,bgColor,fgColor,ovrlayColor}) {
  return TextButton(
    style: ButtonStyle(
      shape:MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))) ,
      shadowColor: MaterialStateProperty.all(shadowcolor),
    
      textStyle: MaterialStateProperty.all(
          TextStyle(color: txtclr,fontFamily: font,fontSize: 16)),
      padding: MaterialStateProperty.all(padding),
      backgroundColor: MaterialStateProperty.all(bgColor),
      foregroundColor: MaterialStateProperty.all(fgColor),
      overlayColor: MaterialStateProperty.all(ovrlayColor),
    ),
      onPressed: 
        onpressed,
      child: Text(text));
}