import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

class Onboarddata {
  final String title;
  final String subtititle;
  final String image;
  final Color color;

   Onboarddata({
    required this.title,
    required this.subtititle,
    required this.image,
    required this.color,
  });
}

class OnboardingItems {
  static List<Onboarddata> loadOnboardingItem() {
    final fi = <Onboarddata>[
       Onboarddata(title: 'Welcome to DecArdiary Store Manager',subtititle: '',image: 'assets/svg/store1.svg', color: AppColors.white),
      Onboarddata(title: 'Track your Products and Models', subtititle: 'you can enable or disable products of your firm on the basis of availability'
      ,image: 'assets/svg/stats1.svg', color: AppColors.white),
      Onboarddata(title: 'Bulk Update Models', subtititle: 'you can update bulkly status of products and models on the basis of stock availability',image: 'assets/svg/bulk1.svg', color:AppColors.white),
      Onboarddata(title: 'View your Collections', subtititle: 'View users details and Coupon details ',image: 'assets/svg/search1.svg', color: AppColors.white),
    ];
    return fi;
  }
}
