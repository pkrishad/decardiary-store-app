// ignore_for_file: non_constant_identifier_names, prefer_const_constructors

import 'package:flutter/material.dart';

Widget CustomElevatedButton(
    { Widget? text,
    void Function()? onpress,
    EdgeInsetsGeometry? padding,
    Size? size,
    Color? txtclr,
    fntsize,font,
    Color? bgColor,
    Color? fgColor,
    Color? ovrlayColor,}) {
 
  return ElevatedButton(
    child: text,
    onPressed: onpress,

    style: ButtonStyle(fixedSize:MaterialStateProperty.all(size) ,
      shape:MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(5))) ,
      textStyle: MaterialStateProperty.all(
          TextStyle(color: txtclr,fontFamily: font,fontSize: 13)),
      padding: MaterialStateProperty.all(padding),
      backgroundColor: MaterialStateProperty.all(bgColor),
      foregroundColor: MaterialStateProperty.all(fgColor),
      overlayColor: MaterialStateProperty.all(ovrlayColor),
    ),
  );
}



