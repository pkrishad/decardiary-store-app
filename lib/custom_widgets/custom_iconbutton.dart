// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

Widget CustomIconButton(
    {required onPressed,
    icon,
    size,btnColor,splashColor,fcscolor}) {
  return IconButton(focusColor: Colors.white,
    splashColor: splashColor,
    color: btnColor,
    highlightColor: fcscolor,
    onPressed: onPressed,
    icon: Icon(icon),
    iconSize: size,
  );
}