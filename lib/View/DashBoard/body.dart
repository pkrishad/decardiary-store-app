// ignore_for_file: prefer_const_constructors

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iconsax/iconsax.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/DashBoard/banner.dart';
import 'package:storeadmin_app/View/DashBoard/providerdash.dart';
import 'package:storeadmin_app/View/Notifications/notification.dart';
import 'package:storeadmin_app/View/Profile/profile.dart';
import 'package:storeadmin_app/View/Profile/providerprofile.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/custom_widgets/dash_container.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  bool isLoading = false;
  int _current = 0;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var userdata = Provider.of<ProviderProfile>(context, listen: false);
    return Consumer<ProviderDash>(builder: (context, provider, child) {
      return Column(children: [
        Container(
          height: size.height * 0.088,
          color: AppColors.white,
          child: Stack(children: [
            Positioned(
              top: 12,
              left: 17,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomContainer(
                    color: Colors.grey.shade50.withOpacity(0.2),
                    padding: EdgeInsets.all(4.0),
                    width: size.width - 162,
                    child: userdata.data != null
                        ? customText(
                            text: userdata.data!.storename,
                            weight: FontWeight.bold,
                            textSize: Sizes.TEXT_SIZE_20)
                        : customText(text: ""),
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return const Msg();
                            },
                          ),
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            color: Colors.grey.shade50,
                            borderRadius: BorderRadius.circular(8.0)),
                        child: Icon(
                          Iconsax.notification,
                          color: AppColors.black,
                          size: 30.0,
                        ),
                      )),
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return const Profile();
                          },
                        ),
                      );
                    },
                    child: CircleAvatar(
                      radius: 26,
                      backgroundColor: Colors.grey.shade100,
                      child: userdata.data != null
                          ? CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 21,
                              backgroundImage: NetworkImage(
                                  userdata.data!.storeimage,
                                  scale: 0.01))
                          : CircleAvatar(
                              radius: 23,
                              backgroundColor: Colors.grey.shade50,
                            ),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                ],
              ),
            ),

            // Positioned(top:13,left:70,child: CustomContainer(padding: EdgeInsets.all(12.0),width:size.width-140,child: Center(child: customText(text: "Alfa",weight: FontWeight.bold)),border: Border.all(color: Colors.grey.shade300),borderRadius: BorderRadius.circular(4.0),))
          ]),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: Container(
            color: Colors.grey.shade50,
            height: 1,
          ),
        ),
        CustomContainer(
            height: size.height * 0.22,
            margin: const EdgeInsets.only(left: 0, right: 0),
            child: provider.dash?.topBanners.length == null
                ? Text("")
                : CarouselSlider.builder(
                    itemBuilder: (BuildContext context, index, realindex) {
                      final top = provider.dash?.topBanners[index];
                      return Card(
                          color: AppColors.white,
                          elevation: 0,
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(
                                  width: 0.8, color: AppColors.white),
                              borderRadius: BorderRadius.circular(10)),
                          child: InkWell(
                            onTap: () {
                              top!.link.isEmpty
                                  ? null
                                  : Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                      return Bannerinner(top: top);
                                    }));
                              // var url = "${top?.link}";
                              // if (await canLaunch(url)) {
                              //   await launch(url);
                              // } else {
                              //   throw 'Could not launch $url';
                              // }
                            },
                            child: CustomContainer(
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10.0),
                                  child: top!.image == null
                                      ? CustomContainer(
                                          color: Colors.grey.shade100,
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        )
                                      : Image.network(
                                          top.image,
                                          fit: BoxFit.fill,
                                        )),
                              color: AppColors.white,
                              width: size.width,
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ));
                    },
                    itemCount: provider.dash?.topBanners.length,
                    options: CarouselOptions(
                      aspectRatio: 3 / 1.3,
                      autoPlay: false,
                      viewportFraction: 1.0,

                      // reverse: true ,
                      autoPlayAnimationDuration: Duration(seconds: 5),
                      onPageChanged: (index, reason) => setState(() {
                        _current = index;
                      }),
                    ),
                  )),

        SizedBox(
          height: size.height * 0.01,
        ),
        provider.dash?.topBanners.length == null
            ? Text("no data", style: TextStyles.white_20_700())
            : AnimatedSmoothIndicator(
                duration: Duration(seconds: 3),
                activeIndex: _current,
                count: provider.dash!.topBanners.length,
                effect: WormEffect(
                    dotColor: Colors.grey.shade100,
                    activeDotColor: AppColors.primaryColor,
                    dotWidth: 8.0,
                    dotHeight: 8.0)),
        SizedBox(
          height: size.height * 0.03,
        ),
        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          SizedBox(
            width: 15,
          ),
          Dashcontainer(
            border:
                Border.all(color: Color(0xFFFF8748).withOpacity(0.8), width: 3),
            color: AppColors.white,
            icon: Icon(
              Iconsax.people,
              color: Color(0xFFFF8748).withOpacity(0.8),
            ),
            text1: provider.dash?.users == null
                ? Shimmer.fromColors(
                    baseColor: Colors.white,
                    highlightColor: Colors.grey.shade100,
                    child: CustomContainer(
                      width: size.width,
                      height: 30.0,
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                    ))
                : Text(
                    "${provider.dash?.users}",
                    style: GoogleFonts.lato(
                        color: AppColors.primaryColor,
                        fontSize: 34.0,
                        fontWeight: FontWeight.w900),
                  ),
            text2: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              customText(
                  text: "Total Users",
                  color: Colors.grey.withOpacity(0.9),
                  textSize: 18.0),
            ]),
            totalsteps: provider.dash != null ? provider.dash?.users : 0,
            currentstep: provider.dash?.users,
          ),
          SizedBox(
            width: 15,
          ),
          Dashcontainer(
              border: Border.all(color: Colors.grey.shade200, width: 2),
              color: Colors.white,
              icon: Icon(
                Iconsax.bag,
                color: Color(0xFFFF8748).withOpacity(0.8),
              ),
              text1: provider.dash?.products.total == null
                  ? Shimmer.fromColors(
                      baseColor: Colors.white,
                      highlightColor: Colors.grey.shade100,
                      child: CustomContainer(
                        width: size.width,
                        height: 30.0,
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                      ))
                  : Text(
                      "${provider.dash?.products.activated}/${provider.dash?.products.total}",
                      style: GoogleFonts.lato(
                          color: AppColors.primaryColor,
                          fontSize: 34.0,
                          fontWeight: FontWeight.w900),
                    ),
              text2:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                customText(
                    text: "Products",
                    color: Colors.grey.withOpacity(0.9),textSize: 18.0
                  ),
              ]),
              totalsteps: provider.dash?.products.total,
              currentstep: provider.dash?.products.activated),
          SizedBox(
            width: 15,
          ),
        ]),

        SizedBox(
          height: size.height * 0.02,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: 15,
            ),
            Dashcontainer(
              border: Border.all(color: Colors.grey.shade200, width: 2),
              color: AppColors.white,
              icon: Icon(
                Iconsax.picture_frame,
                color: Color(0xFFFF8748).withOpacity(0.8),
              ),
              text1: provider.dash?.models.total == null
                  ? Shimmer.fromColors(
                      baseColor: Colors.white,
                      highlightColor: Colors.grey.shade100,
                      child: CustomContainer(
                        width: size.width,
                        height: 30.0,
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                      ))
                  : Text(
                      "${provider.dash?.models.activated}/${provider.dash?.models.total}",
                      style: GoogleFonts.lato(
                          color: AppColors.primaryColor,
                          fontSize: 34.0,
                          fontWeight: FontWeight.w900),
                    ),
              text2:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                customText(
                    text: "Models",
                    color: Colors.grey.withOpacity(0.9),
                   textSize: 18.0),
              ]),
              totalsteps: provider.dash?.models.total,
              currentstep: provider.dash?.models.activated,
            ),
            SizedBox(
              width: 15,
            ),
            Dashcontainer(
              border: Border.all(
                  color: Color(0xFFFF8748).withOpacity(0.8), width: 3),
              color: AppColors.white,
              icon: Icon(
                Icons.confirmation_num_outlined,
                color: Color(0xFFFF8748).withOpacity(0.8),
              ),
              text1: provider.dash?.coupons.total == null
                  ? Shimmer.fromColors(
                      baseColor: Colors.white,
                      highlightColor: Colors.grey.shade100,
                      child: CustomContainer(
                        width: size.width,
                        height: 30.0,
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                      ))
                  : Text(
                      "${provider.dash?.coupons.used}/${provider.dash?.coupons.total}",
                      style: GoogleFonts.lato(
                          color: AppColors.primaryColor,
                          fontSize: 34.0,
                          fontWeight: FontWeight.w900),
                    ),
              text2:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                customText(
                    text: "Vouchers",
                    color: Colors.grey.withOpacity(0.9),
                    textSize: 18.0),
              ]),
              totalsteps: provider.dash?.coupons.total,
              currentstep: provider.dash?.coupons.used,
            ),
            SizedBox(
              width: 15,
            ),
          ],
        )

//  Iconsax.home,
//     Iconsax.bag,
//     Iconsax.picture_frame,
//     Iconsax.people,
        //       SizedBox(
        //           height: size.height * 0.79,
        //           child: Stack(children: <Widget>[
        //             ClipPath(
        //               clipper: MyClipper(),
        //               child: Container(
        //                 padding:
        //                     const EdgeInsets.only(left: 20, top: 30, right: 20),
        //                 height: size.height * 0.34,
        //                 width: double.infinity,
        //                 decoration: BoxDecoration(
        //                   gradient: LinearGradient(
        //                     begin: Alignment.topRight,
        //                     end: Alignment.bottomLeft,
        //                     colors: const [Color(0xFFf5c1ab), Color(0xFFf0a6b7)],
        //                   ),
        //                 ),
        //                 child: Column(
        //                     // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //                     children: <Widget>[]),
        //               ),
        //             ),
        //             Column(children: [
        //               SizedBox(
        //                 height: size.height * 0.02,
        //               ),

        //               SizedBox(
        //                 height: 20,
        //               ),
        //               provider.dash?.topBanners.length == null
        //                   ? Text("no data", style: TextStyles.white_20_700())
        //                   : AnimatedSmoothIndicator(
        //                       duration: Duration(seconds: 2),
        //                       activeIndex: _current,
        //                       count: provider.dash!.topBanners.length,
        //                       effect: WormEffect(
        //                           dotColor: Color(0xFF11249F).withOpacity(0.1),
        //                           activeDotColor: AppColors.white,
        //                           dotWidth: 8.0,
        //                           dotHeight: 8.0)),
        //             ]),
        //             Positioned(
        //               left: 0,
        //               right: 0,
        //               bottom: size.aspectRatio * 55,
        //               child: Padding(
        //                 padding: const EdgeInsets.all(15),
        //                 child: Column(
        //                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //                   children: [
        //                     Row(
        //                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //                       children: [
        //                         Dashcontainer(
        //                           color: AppColors.white,
        //                           icon: Icon(
        //                             Icons.supervisor_account,
        //                             color: Color(0xFFFF8748).withOpacity(0.8),
        //                           ),
        //                           text1:provider.dash?.users == null ? Shimmer.fromColors(
        //   baseColor: Colors.white,
        //   highlightColor: Colors.grey.shade100,
        //   child:CustomContainer(width: size.width,height: 30.0,color: Colors.white,borderRadius: BorderRadius.circular(8),)
        // ):
        //                           customText(text: "${provider.dash?.users}",color:Color(0xFFFF8748),textSize:  40.0,weight: FontWeight.bold ),

        //                           text2: Row(
        //                               mainAxisAlignment: MainAxisAlignment.center,
        //                               children: [
        //                                 customText(
        //                                     text: "Total Users",
        //                                     color: Colors.grey.withOpacity(0.9),
        //                                     weight: FontWeight.bold),
        //                               ]),
        //                         ),
        //                         Dashcontainer(
        //                           color: Colors.white,
        //                           icon: Icon(
        //                             Icons.shopping_bag,
        //                             color: Color(0XFFB31217).withOpacity(0.4),
        //                           ),
        //                           text1:provider.dash?.products.total == null ? Shimmer.fromColors(
        //   baseColor: Colors.white,
        //   highlightColor: Colors.grey.shade100,
        //   child:CustomContainer(width: size.width,height: 30.0,color: Colors.white,borderRadius: BorderRadius.circular(8),)
        // ):
        //                              customText (text: "${provider.dash?.products.activated}/${provider.dash?.products.total}",color: Color(0XFFB31217),textSize: 28.0,weight: FontWeight.bold),

        //                           text2: Row(
        //                               mainAxisAlignment: MainAxisAlignment.center,
        //                               children: [
        //                                 customText(
        //                                     text: "Products",
        //                                     color: Colors.grey.withOpacity(0.9),
        //                                     weight: FontWeight.bold),
        //                               ]),
        //                         ),
        //                       ],
        //                     ),
        //                     SizedBox(
        //                       height: size.height * 0.02,
        //                     ),
        //                     Row(
        //                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //                       children: [
        //                         Dashcontainer(
        //                           color: AppColors.white,
        //                           icon: Icon(
        //                             Icons.view_module,
        //                             color: Color(0XFF16A085),
        //                           ),
        //                           text1:
        //                           provider.dash?.models.total == null ? Shimmer.fromColors(
        //   baseColor: Colors.white,
        //   highlightColor: Colors.grey.shade100,
        //   child:CustomContainer(width: size.width,height: 30.0,color: Colors.white,borderRadius: BorderRadius.circular(8),)
        // ):
        //                               customText(text: "${provider.dash?.models.activated}/${provider.dash?.models.total}",color: Color(0XFF16A085) ,textSize:28.0,weight: FontWeight.bold),

        //                           text2: Row(
        //                               mainAxisAlignment: MainAxisAlignment.center,
        //                               children: [
        //                                 customText(
        //                                     text: "Models",
        //                                     color: Colors.grey.withOpacity(0.9),
        //                                     weight: FontWeight.bold),
        //                               ]),
        //                         ),
        //                         Dashcontainer(
        //                           color: AppColors.white,
        //                           icon: Icon(
        //                             Icons.confirmation_num,
        //                             color: Color(0xFF4E4376).withOpacity(0.4),
        //                           ),
        //                           text1:
        //                           provider.dash?.coupons.total == null ? Shimmer.fromColors(
        //   baseColor: Colors.white,
        //   highlightColor: Colors.grey.shade100,
        //   child:CustomContainer(width: size.width,height: 30.0,color: Colors.white,borderRadius: BorderRadius.circular(8),)
        // ):
        //                               customText(text: "${provider.dash?.coupons.used}/${provider.dash?.coupons.total}",color: Color(0xFF4E4376),textSize:30.0,weight: FontWeight.bold ),

        //                           text2: Row(
        //                               mainAxisAlignment: MainAxisAlignment.center,
        //                               children: [
        //                                 customText(
        //                                     text: "Vouchers",
        //                                     color: Colors.grey.withOpacity(0.9),
        //                                     weight: FontWeight.bold),
        //                               ]),
        //                         ),
        //                       ],
        //                     )
        //                   ],
        //                 ),
        //               ),
        //             ),
        //           ])),
        // ]),
      ]);
    });
  }
}
