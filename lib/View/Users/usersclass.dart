// To parse this JSON data, do
//
//     final users = usersFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

UsersData usersFromJson(String str) => UsersData.fromJson(json.decode(str));

String usersToJson(UsersData data) => json.encode(data.toJson());

class UsersData {
    UsersData({
        required this.status,
        required this.data,
        required this.message,
    });

    final String status;
    final Data data;
    final String message;

    factory UsersData.fromJson(Map<String, dynamic> json) => UsersData(
        status: json["status"],
        data: Data.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
        "message": message,
    };
}

class Data {
    Data({
        required this.currentPage,
        required this.data,
        // required this.from,
        required this.lastPage,
        // required this.nextPageUrl,
        // required this.path,
        // required this.perPage,
        // required this.prevPageUrl,
        // required this.to,
        // required this.total,
    });

    final int currentPage;
    final List<Datum> data;
    // final int from;
    final int lastPage;
    // final String nextPageUrl;
    // final String path;
    // final String perPage;
    // final dynamic prevPageUrl;
    // final int to;
    // final int total;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        currentPage: json["current_page"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        // from: json["from"],
        lastPage: json["last_page"],
        // nextPageUrl: json["next_page_url"],
        // path: json["path"],
        // perPage: json["per_page"],
        // prevPageUrl: json["prev_page_url"],
        // to: json["to"],
        // total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        // "from": from,
        "last_page": lastPage,
        // "next_page_url": nextPageUrl,
        // "path": path,
        // "per_page": perPage,
        // "prev_page_url": prevPageUrl,
        // "to": to,
        // "total": total,
    };
}

class Datum {
    Datum({
        required this.membershipstatus,
        required this.createdAt,
        required this.expiryDate,
        required this.username,
        required this.expiresin,
    });

    final int membershipstatus;
    final DateTime createdAt;
    final DateTime expiryDate;
    final String username;
    final int expiresin;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        membershipstatus: json["membershipstatus"],
        createdAt: DateTime.parse(json["created_at"]),
        expiryDate: DateTime.parse(json["expiry_date"]),
        username: json["username"],
        expiresin: json["expiresin"],
    );

    Map<String, dynamic> toJson() => {
        "membershipstatus": membershipstatus,
        "created_at": createdAt.toIso8601String(),
        "expiry_date": expiryDate.toIso8601String(),
        "username": username,
        "expiresin": expiresin,
    };
}
