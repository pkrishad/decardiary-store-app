class GetCProductsReq {
  int page;
  String searchtext;
  String dataorder;
  String status;
  String isdisabled;
  int perpage;

  GetCProductsReq(this.page, this.searchtext,this.dataorder,this.isdisabled,this.perpage,this.status);

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["page"] = page;
    map["perpage"] = perpage;
    map["dataorder"] = dataorder;
    map["status"] = status;
    map["isdisabled"] = isdisabled;
    map["search"] = searchtext;

    // page=$current_page&perpage=$perpage&dataorder=$sort&status=$status&isdisabled=$action&search=
    return map;
  }
}

class CProductClass {
  CProductClass({
    required this.status,
    required this.data,
    required this.message,
  });

  String status;
  Data data;
  String message;

  factory CProductClass.fromJson(Map<String, dynamic> json) => CProductClass(
        status: json["status"],
        data: Data.fromJson(json["data"]),
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
        "message": message,
      };
}

class Data {
  Data({
    required this.currentPage,
    required this.data,
    // required this.from,
    required this.lastPage,
    // required this.nextPageUrl,
    // required this.path,
    // required this.perPage,
    // required this.prevPageUrl,
    // required this.to,
    // required this.total,
  });

  int currentPage;
  List<Datum> data;
  // int from;
  int lastPage;
  // dynamic nextPageUrl;
  // String path;
  // int perPage;
  // dynamic prevPageUrl;
  // int to;
  // int total;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        currentPage: json["current_page"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        // from: json["from"],
        lastPage: json["last_page"],
        // nextPageUrl: json["next_page_url"],
        // path: json["path"],
        // perPage: json["per_page"],
        // prevPageUrl: json["prev_page_url"],
        // to: json["to"],
        // total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        // "from": from,
        "last_page": lastPage,
        // "next_page_url": nextPageUrl,
        // "path": path,
        // "per_page": perPage,
        // "prev_page_url": prevPageUrl,
        // "to": to,
        // "total": total,
      };
}

class Datum {
  Datum({
    required this.id,
    required this.name,
    required this.image,
    required this.globalStatus,
    required this.type,
    required this.viewAs,
    required this.status,
  });

  int id;
  String name;
  String image;
  int globalStatus;
  int type;
  int viewAs;
  int status;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        image: json["image"],
        globalStatus: json["global_status"],
        type: json["type"],
        viewAs: json["view_as"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image": image,
        "global_status": globalStatus,
        "type": type,
        "view_as": viewAs,
        "status": status,
      };
}
