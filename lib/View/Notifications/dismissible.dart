import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';

class DismissibleWidget<T>extends StatelessWidget {
  final Widget child;
  final T item;
  final DismissDirectionCallback onDismissed;
  const DismissibleWidget({Key? key, required this.child, required this.item, required this.onDismissed 
    
  }) : super(key: key);

  @override
  Widget build(BuildContext context) =>Dismissible(
      direction: DismissDirection.horizontal,
      
      key: ObjectKey(item),
  background: RightSwipe(),
      secondaryBackground: LeftSwipe(),
      onDismissed: onDismissed,
  child: child);
}
Widget LeftSwipe()=>
  Container(alignment: Alignment.centerRight,padding: const EdgeInsets.symmetric(horizontal: 20),color: AppColors.dark,
  child: const Icon(Icons.delete_forever,color: AppColors.white,size: 32.0,),);
Widget RightSwipe()=>
    Container(alignment: Alignment.centerLeft,padding: const EdgeInsets.symmetric(horizontal: 20),color: AppColors.dark,
      child: const Icon(Icons.delete_forever,color: AppColors.white,size: 32.0,),);
