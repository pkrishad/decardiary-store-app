// import 'package:firebase_messaging/firebase_messaging.dart';
// ignore_for_file: non_constant_identifier_names

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';
import 'package:storeadmin_app/Logout/providerlogout.dart';
import 'package:storeadmin_app/Navigation/nav.dart';
import 'package:storeadmin_app/Version%20Check/update_app.dart';
import 'package:storeadmin_app/View/Coupons/All%20Coupons/p_couponlist.dart';
import 'package:storeadmin_app/View/Coupons/History/p_history.dart';
import 'package:storeadmin_app/View/DashBoard/providerdash.dart';
import 'package:storeadmin_app/View/Login%20Screen/login_int.dart';
import 'package:storeadmin_app/View/Login%20Screen/loginscreen.dart';
import 'package:storeadmin_app/View/Models/MProvider/model_list.dart';
import 'package:storeadmin_app/View/Models/MProvider/updatecmod.dart';
import 'package:storeadmin_app/View/Models/MProvider/updatemlist.dart';
import 'package:storeadmin_app/View/Notifications/providernot.dart';
import 'package:storeadmin_app/View/OnBoarding/splash.dart';
import 'package:storeadmin_app/View/Profile/providerprofile.dart';
import 'package:storeadmin_app/View/UpdateProfile/p_update.dart';
import 'package:storeadmin_app/View/UpdateProfile/p_updatepwd.dart';
import 'package:storeadmin_app/View/Users/p_users.dart';
import 'package:storeadmin_app/View/products/ProProvider/pcategry.dart';
import 'package:storeadmin_app/View/products/ProProvider/proprovider.dart';
import 'package:storeadmin_app/View/products/ProProvider/updatecproducts.dart';
import 'package:storeadmin_app/View/products/ProProvider/updateplist.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/notification_service.dart';
import 'Version Check/providerversion.dart';
import 'View/Models/MProvider/categoryprovider.dart';
import 'locator.dart' as ltr;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await FirebaseMessaging.instance.subscribeToTopic('sub');
  await ltr.init();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => ltr.loc<LoginUser>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<ProviderVersion>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<ProviderDash>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<Logout>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<ProviderProfile>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<ProviderCoupon>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<ProviderModel>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<ProviderHistory>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<ProviderUser>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<UpdateProfile>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<UpdatePwd>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<ProviderModellist>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<ProviderCProduct>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<ProviderProduct>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<UpdateCProduct>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<UpdateProductlist>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<UpdateCModel>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<UpdateModellist>()),
      ChangeNotifierProvider(create: (_) => ltr.loc<ProviderNot>()),
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key, this.dioClient}) : super(key: key);
  final DioClient? dioClient;

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
   DateTime pre_backpress = DateTime.now();
  @override
  void initState() {
    final firebaseMessaging = FCM();
    firebaseMessaging.setNotifications();
    super.initState();
    
  }

  @override
  Widget build(BuildContext context) {
    return OverlaySupport.global(
      child: MaterialApp(
        builder: (context, widget) => Navigator(
          onGenerateRoute: (context) => MaterialPageRoute(
            builder: (context) => UpdateApp(
              child: widget,
            ),
          ),
        ),
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        navigatorKey: ltr.loc<NavigationService>().navigatorKey,
        debugShowCheckedModeBanner: false,
        home: const Splash(),
      ),
    );
  }
}


