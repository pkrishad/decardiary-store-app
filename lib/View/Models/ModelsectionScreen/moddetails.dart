import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Models/MProvider/model_list.dart';
import 'package:storeadmin_app/View/Models/ModelClass/listclass.dart';
import 'package:storeadmin_app/custom_widgets/activebar.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_button.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';

class ModelDetails extends StatefulWidget {
  final Datum data;
  const ModelDetails({Key? key, required this.data}) : super(key: key);

  @override
  State<ModelDetails> createState() => _ModelDetailsState();
}

class _ModelDetailsState extends State<ModelDetails> {
    final RefreshController refreshController =
      RefreshController(initialRefresh: true);
          bool togglevalue = false;
  @override
  Widget build(BuildContext context) {
    late int dat;
    Size size = MediaQuery.of(context).size;
    checkConnection(context);
    return Consumer<ProviderModellist>(builder: (context, provider, child) {
      return Scaffold(
          backgroundColor: AppColors.white,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: AppColors.white,
            leading: IconButton(
              onPressed: () {
                 provider.getModellist(isRefresh: true,);
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back_ios_outlined,
                color: AppColors.black,
                size: 20.0,
              ),
            ),
            title: customText(
                text: widget.data.name,
                color: AppColors.black,
                weight: FontWeight.bold),
          ),
          body: SmartRefresher(
              controller: refreshController,
            
              enablePullDown: true,
             
              onRefresh: () async {
                final result =
                    await provider.getModellist(isRefresh: true);
                if (result) {
                  refreshController.refreshCompleted();
                } else {
                  refreshController.refreshFailed();
                }
              },
              onLoading: () async {
                final result = await provider.getModellist();
                if (result) {
                  refreshController.loadComplete();
                } else {
                  refreshController.loadFailed();
                }
              },
              child:Padding(
              padding: const EdgeInsets.all(20),
              child: Column(children: [
                CustomContainer(
                  color: AppColors.primaryLightColor.withOpacity(0.3),
                  height: size.height * 0.45,
                  borderRadius: BorderRadius.circular(20),
                  child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: Image.network(
            widget.data.image,
            fit: BoxFit.cover,
          ),
                  ),
                ),
                SizedBox(
                  height: size.height * 0.01,
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: Column(
          children: [
            SizedBox(
              height: size.height * 0.02,
            ),
            CustomContainer(
              color: AppColors.white,
              height: size.height * 0.07,
              borderRadius: BorderRadius.circular(10),
              shadow: [
                BoxShadow(
                    offset: const Offset(0, 1),
                    blurRadius: 5,
                    color: AppColors.black.withOpacity(0.2))
              ],
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    customText(text: "Status", textSize: 25.0),
                    ActiveBar(
                      bgcolor: widget.data.status == 0
                          ? Colors.green.shade100
                          : Colors.red.shade700,
                      text: customText(
                          text: widget.data.status == 0
                              ? "Active"
                              : "Inactive"),
                      onTap: () {},
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            CustomContainer(
              color: AppColors.white,
              height: size.height * 0.07,
              borderRadius: BorderRadius.circular(10),
              shadow: [
                BoxShadow(
                    offset: const Offset(0, 1),
                    blurRadius: 5,
                    color: AppColors.black.withOpacity(0.2))
              ],
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      customText(text: "Category", textSize: 25.0),
                      ActiveBar(
                        bgcolor: widget.data.globalStatus == 0
                            ? Colors.green.shade100
                            : Colors.red.shade700,
                        text: customText(
                            text: widget.data.categoryId == 45
                                ? "Basin"
                                : widget.data.categoryId == 43?"Kitchen":widget.data.categoryId == 42?"Wardrobe":widget.data.categoryId == 41?"Doors":"null"),
                        onTap: () {},
                      ),
                    ]),
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            CustomContainer(
              color: AppColors.white,
              height: size.height * 0.07,
              borderRadius: BorderRadius.circular(10),
              shadow: [
                BoxShadow(
                    offset: const Offset(0, 1),
                    blurRadius: 5,
                    color: AppColors.black.withOpacity(0.2))
              ],
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      customText(text: "Actions", textSize: 25.0),
                      CustomElevatedButton(
                        fgColor: AppColors.black,
                        bgColor: widget.data.status == 0
                            ? Colors.green.shade100
                            : Colors.red.shade700,
                        text: customText(
                            text: widget.data.status == 0
                                ? "Enabled"
                                : "Disabled"),
                        onpress: () {
                            togglevalue = !togglevalue;
                          // print(togglevalue);
                          if (togglevalue) {
                            dat = 1;
                          } else if (!togglevalue) {
                            dat = 0;
                          }
                          provider.updateStatus(
                              modelid: widget.data.id, status: dat);
                          Future.delayed(
                              Duration(milliseconds: 200), () {
     refreshController.isRefresh;
                                            refreshController.requestRefresh(
                                                needCallback: true);
                                            refreshController
                                                .refreshCompleted();
                              });
                        
                          // provider.getModellist(isRefresh: true);
                        },
                      ),
                    ]),
              ),
            )
          ],
                  ),
                ),
              ]))));
    });
  }
}
