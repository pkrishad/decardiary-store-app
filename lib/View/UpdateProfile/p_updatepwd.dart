





import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class UpdatePwd with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;
  UpdatePwd({
    required this.repo,
    required this.dioClient,
  });

  Future<ApiResponse?> updatepwd(
      {required BuildContext context,
      required TextEditingController currentpwd,
      required TextEditingController newpassword,
      required TextEditingController cnfmpassword,
      }) async {
    final Map<String, dynamic> updateData = {
      "current_password": currentpwd.text,
      "new_password": newpassword.text,
      "confirm_password" : cnfmpassword.text
    };

    ApiResponse res = await repo.postData(StringConst.UPDATE_PASSWORD,
        data: jsonEncode(updateData));
    if (kDebugMode) {
      print(res.response!.statusCode);
    }
    final resdata = (res.response?.data);
    if (resdata["status"] == "success" && res.response!.statusCode == 200) {
      Alerts.showSuccess("Password changed successfully");
      notifyListeners();
    } else if (res.error != "401") {
      if (kDebugMode) {
        print(res.error);
      }
      Alerts.showError("new password must be different");
    }
  }
}