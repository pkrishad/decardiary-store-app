import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

AppBar customAppBar({fntcolor,title,textSize,leading,bottom}) {
  return AppBar(
     backgroundColor: Colors.transparent,
    elevation: 0,
    title: title,
    bottom: PreferredSize(
            preferredSize: const Size(double.infinity,25.0),
            child: Expanded(
              flex: 20,
              child: bottom),),
    titleTextStyle: GoogleFonts.roboto(
      fontSize: textSize,
      color: fntcolor,
      fontStyle: FontStyle.normal,
    ),
    leading: leading,
  );
}
