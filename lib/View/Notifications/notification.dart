// ignore_for_file: unused_import

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/View/Notifications/providernot.dart';
import 'package:storeadmin_app/View/Notifications/smsclass.dart';
import 'package:storeadmin_app/View/Notifications/smspage.dart';
import 'package:storeadmin_app/custom_widgets/activebar.dart';
import 'package:storeadmin_app/custom_widgets/connectivity.dart';
import 'package:storeadmin_app/custom_widgets/custom_container.dart';
import 'package:storeadmin_app/custom_widgets/custom_iconbutton.dart';
import 'package:storeadmin_app/custom_widgets/custom_text.dart';
import 'package:storeadmin_app/View/Notifications/dismissible.dart';
import 'package:storeadmin_app/notification_service.dart';

class Msg extends StatefulWidget {
  const Msg({Key? key}) : super(key: key);

  @override
  _MsgState createState() => _MsgState();
}

class _MsgState extends State<Msg> {
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);
  bool isLoading = false;
  late List<Datum> notification = [];
 
  @override
  Widget build(BuildContext context) {
    checkConnection(context);
    return Consumer<ProviderNot>(builder: (context, provider, child) {
      var product =
          provider.notification.map((e) => e.details.map((e) => e.product));
      print(product);
      return SafeArea(
          child: Scaffold(
              appBar: AppBar(
                elevation: 0,
                leading: CustomIconButton(
                    icon: Icons.arrow_back_ios_rounded,
                    size: 25.0,
                    btnColor: AppColors.dark,
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                toolbarHeight: 80,
                backgroundColor: AppColors.white,
                centerTitle: true,
                title: Padding(
                    padding: const EdgeInsets.all(30),
                    child: Container(
                        width: 200,
                        height: 50,
                        decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: [
                              BoxShadow(
                                  offset: const Offset(0, 1),
                                  blurRadius: 5,
                                  color: AppColors.black.withOpacity(0.23))
                            ]),
                        child: Center(
                            child: customText(
                                text: "Notifications",
                                textSize: 20.0,
                                color: AppColors.dark)))),
              ),
              backgroundColor: AppColors.white,
              body: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(children: [
                    Expanded(
                        child: SmartRefresher(
                            controller: refreshController,
                            enablePullUp: true,
                            enablePullDown: true,
                            footer: CustomFooter(
                              builder:
                                  (BuildContext context, LoadStatus? mode) {
                                Widget child;
                                if (mode == LoadStatus.idle) {
                                  child = Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        customText(
                                            text: "Swipe up to load more",
                                            color: AppColors.hint2)
                                      ]);
                                } else if (mode == LoadStatus.loading) {
                                  child = Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        const CupertinoActivityIndicator(),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        customText(
                                            text: "Loading..",
                                            color: AppColors.hint2)
                                      ]);
                                } else if (mode == LoadStatus.failed) {
                                  child = Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        const Icon(
                                          Icons.error_outlined,
                                          color: AppColors.hint2,
                                        ),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        customText(
                                            text: "No more Data",
                                            color: AppColors.hint2)
                                      ]);
                                } else if (mode == LoadStatus.canLoading) {
                                  child = Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        customText(
                                            text: "Swipe up to load more",
                                            color: AppColors.hint2)
                                      ]);
                                } else {
                                  child = Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        customText(
                                            text: "No more Data",
                                            color: AppColors.hint2)
                                      ]);
                                }
                                return child;
                              },
                            ),
                            onRefresh: () async {
                              final result = await provider.getNotification(
                                isRefresh: true,
                              );
                              if (result) {
                                refreshController.refreshCompleted();
                              } else {
                                refreshController.refreshFailed();
                              }
                            },
                            onLoading: () async {
                              final result = await provider.getNotification();
                              if (result) {
                                refreshController.loadComplete();
                              } else {
                                refreshController.loadFailed();
                              }
                            },
                            child: isLoading
                                ? const Center(
                                    child: CircularProgressIndicator())
                                : provider.notification.isNotEmpty
                                    ? ListView.separated(
                                        separatorBuilder:
                                            (BuildContext context, int index) {
                                          return const SizedBox(height: 10);
                                        },
                                        itemCount: provider.notification.length,
                                        itemBuilder: (context, index) {
                                          final sms =
                                              provider.notification[index];

                                          return DismissibleWidget(
                                              item:
                                                  provider.notification[index],
                                              onDismissed: (direction) =>
                                                  dismissItem(
                                                    context,
                                                    index,
                                                    direction,
                                                  ),
                                              child: Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 1,
                                                      vertical: 6),
                                                  child: InkWell(
                                                    onTap: () async {
                                                      await Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder:
                                                                  (context) =>
                                                                      SmsInner(
                                                                        data:
                                                                            sms,
                                                                      )));
                                                    },
                                                    child: Card(
                                                      elevation: 2,
                                                      child: ListTile(
                                                        isThreeLine: true,
                                                        contentPadding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal: 10,
                                                                vertical: 15),
                                                        tileColor:
                                                            AppColors.white,
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10)),
                                                        minLeadingWidth: 10.0,
                                                        leading: const CircleAvatar(
                                                          backgroundColor:
                                                              AppColors.white,
                                                          child: Icon(
                                                            Icons
                                                                .notifications_active,
                                                            size: 30.0,
                                                            color:
                                                                AppColors.blue,
                                                          ),
                                                        ),
                                                        title: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            customText(
                                                                text:
                                                                    "${sms.subject.toString().substring(8)}",
                                                                textSize: 15.0,
                                                                weight:
                                                                    FontWeight
                                                                        .bold),
                                                            customText(
                                                                text:
                                                                    "${sms.createdAt.toString().substring(0, 10)}",
                                                                textSize: 10.0,
                                                                color: AppColors
                                                                    .hint),
                                                          ],
                                                        ),
                                                        subtitle: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            const SizedBox(
                                                              height: 7.0,
                                                            ),
                                                            customText(
                                                                text:
                                                                    "${sms.description.toString().substring(12).toLowerCase()}",
                                                                textSize: 15.0,
                                                                color: AppColors
                                                                    .hint),
                                                            const SizedBox(
                                                              height: 5.0,
                                                            ),
                                                            CustomContainer(
                                                              child: customText(
                                                                  text:
                                                                      "${sms.model}"),
                                                              color: AppColors
                                                                  .blue,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10),
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(10),
                                                            ),
                                                            const SizedBox(
                                                              height: 3.0,
                                                            ),
                                                            CustomContainer(
                                                              child: customText(
                                                                  text: sms.details
                                                                              .map((e) => e.product)
                                                                              .join(" , ") ==
                                                                          null
                                                                      ? ""
                                                                      : '${sms.details.map((e) => e.product).join(" , ")}'),
                                                              color: AppColors
                                                                  .white,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10),
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(10),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  )));
                                        },
                                      )
                                    : Center(
                                        child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          const Icon(
                                            Icons.message_outlined,
                                            color: AppColors.hint,
                                            size: 40.0,
                                          ),
                                          customText(text: "No Notifications")
                                        ],
                                      ))))
                  ]))));
    });
  }

  void dismissItem(
      BuildContext context, int index, DismissDirection direction) {
    ProviderNot smslist = Provider.of<ProviderNot>(context, listen: false);
    setState(() {
      smslist.deletesms(smslist.notification[index].notificationId);
      smslist.notification.removeAt(index);
    });
    switch (direction) {
      case DismissDirection.startToEnd:
        break;
      case DismissDirection.endToStart:
        break;
      case DismissDirection.vertical:
        // TODO: Handle this case.
        break;
      case DismissDirection.horizontal:
        smslist.deletesms(smslist.notification[index].notificationId);
        smslist.notification.removeAt(index);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Row(children: const [
              Icon(
                Icons.delete_forever_rounded,
                color: AppColors.dark,
              ),
              SizedBox(
                width: 20,
              ),
              Text(
                "1 Notification Archived",
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: AppColors.dark),
              ),
            ]),
            behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.white));
        // smslist.notification.removeWhere((index) => false);

        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text(
              "1 Notification Archived",
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: AppColors.darkblue),
            ),
            behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.white));
        break;
      case DismissDirection.up:
        // TODO: Handle this case.
        break;
      case DismissDirection.down:
        // TODO: Handle this case.
        break;
      case DismissDirection.none:
        // TODO: Handle this case.
        break;
    }
  }
}
