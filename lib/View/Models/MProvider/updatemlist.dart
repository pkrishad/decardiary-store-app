




import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:storeadmin_app/Values/values.dart';
import 'package:storeadmin_app/dio/api_response.dart';
import 'package:storeadmin_app/dio/dio_client.dart';
import 'package:storeadmin_app/repo.dart';

class UpdateModellist with ChangeNotifier {
  final Repo repo;
  final DioClient dioClient;

  UpdateModellist({required this.repo, required this.dioClient});
// "product_categories"
  Future<ApiResponse?> updatemodlist(
      {required BuildContext context,
      required int modelid,
      required int status,
      }) async {
    // Dialogs.showLoading(context: context);
    var mapData = {
      "models": [
        json.encode({"model_id":modelid,"status":status})
      ].toString()
    };
  
    ApiResponse? res = await repo.postData(
        StringConst.UPDATE_MODEL_LIST,
        data: mapData,);
    final resdata = (res.response?.data);
    if (kDebugMode) {
      print(mapData);
    }
    if (res.response != null && res.response?.statusCode == 200) {
    } else if (res.error != "401") {
      if (kDebugMode) {
        print(res.error);
      }
      // Alerts.showError("Enter a valid Username and Password");
    }
  }
}